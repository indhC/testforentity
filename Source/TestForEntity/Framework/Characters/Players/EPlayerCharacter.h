﻿// © TestForEntity 2023.07.03:20:20 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GameFramework/Character.h"
#include "TestForEntity/Components/Attributes/Interfaces/EAttributesActorInterface.h"
#include "TestForEntity/Components/Combats/Interfaces/EBoneTransformTrackerInterface.h"
#include "TestForEntity/Components/Combats/Interfaces/ECombatScreenConfigInterface.h"
#include "TestForEntity/Components/Traces/Interfaces/EPeriodicTraceConditionInterface.h"
#include "TestForEntity/Interfaces/EInputActionControlInterface.h"
#include "TestForEntity/Interfaces/EMeshActorInterface.h"
#include "TestForEntity/Interfaces/EPickableItemConsumerInterface.h"

#include "EPlayerCharacter.generated.h"


enum class EEInventoryOperationStatus : uint8;

UCLASS(Blueprintable, BlueprintType)
class TESTFORENTITY_API AEPlayerCharacter : public ACharacter,
                                            public IEMeshActorInterface, public IECombatScreenConfigInterface, public IEAttributesActorInterface,
                                            public IEPickableItemConsumerInterface, public IEBoneTransformTrackerInterface, public IEInputActionControlInterface,
                                            public IEPeriodicTraceConditionInterface
{
    GENERATED_BODY()


    /** Camera boom positioning the camera behind the character */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Camera", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class USpringArmComponent> SpringArmComponent;

    /** Follow camera */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Camera", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UCameraComponent> CameraComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Pick Up", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USceneComponent> PickUpTargetComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USceneComponent> LeftHandTransformTrackingComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Generators", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UELootGeneratorComponent> LootGeneratorComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Attributes", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UEAttributesComponent> AttributesComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Inventories", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UEInventoryComponent> InventoryComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UECombatComponent> CombatComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Components|Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UELineTraceComponent> TraceComponent;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> CombatScreenConfigTable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FGameplayTag CombatScreenConfigRowNameTag;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FName PelvisBoneName;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FName LeftHandBoneName;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FName RightHandBoneName;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UTexture> CrosshairDefaultTexture;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FVector2D CrosshairTextureSize;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FVector2D CrosshairTextureHalfSize;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FVector2D CrosshairScreenOffset;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    FVector2D CrosshairScreenLocation;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    float TraceDistance;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character|Configuration|Combat", meta = (AllowPrivateAccess = "true"))
    float TraceDelay;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Movement", meta = (AllowPrivateAccess = "true"))
    double CapsuleAdjusterZOffset;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Movement", meta = (AllowPrivateAccess = "true"))
    float CapsuleAdjusterRadius;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Movement", meta = (AllowPrivateAccess = "true"))
    float CapsuleAdjusterHalfHeight;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Movement", meta = (AllowPrivateAccess = "true"))
    float CapsuleAdjusterRadiusWithoutCollision;


    /** Input Mapping Context */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Input", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UInputMappingContext> InputMappingContext;

    /** All Input Action Handlers that need to be registered */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Character|Configuration|Input", meta = (AllowPrivateAccess = "true"))
    TArray<FGameplayTag> InputActionHandlerNameTags;


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Character", meta = (AllowPrivateAccess = "true"))
    TMap<FGameplayTag, TObjectPtr<UEInputActionHandler>> InputActionHandlersMap;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character|Configuration|Combat")
    void UpdateCombatScreenParameters(const FGameplayTag& InConfigRowNameTag);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character|Configuration|Combat")
    void InitializeCombatComponent() const;


    //
    // Delegate Event Handlers
    //

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character")
    void OnItemPickedUpEvent(AEPickableItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character")
    void OnItemDroppedEvent(AEPickableItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character")
    void OnItemPickedUpByInventoryEvent(const EEInventoryOperationStatus InStatus, AEPickableItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character")
    void OnItemsDroppedByInventoryEvent(const EEInventoryOperationStatus InStatus, const TArray<AEPickableItem*>& InDroppingItems);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Player Character")
    void OnOldClipRemovedEvent(const class AEWeapon* InCurrentWeapon, AEAmmo* InOldAmmo);

public:

    // Sets default values for this character's properties
    AEPlayerCharacter();

    virtual void PostInitProperties() override;

    virtual void Landed(const FHitResult& Hit) override;


    //
    // Delegates
    //

    UPROPERTY(BlueprintAssignable, Category = "Combat|Configuration")
    FOnCombatScreenConfigChaned OnCombatScreenConfigChangedDelegate;


    // Called every frame
    virtual void Tick(const float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;


    /////////////////////////////////////////////////////////////////
    /// IEMeshActorInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual USkeletalMeshComponent* GetMeshComponent_Implementation() const override;


    /////////////////////////////////////////////////////////////////
    /// IECombatScreenConfigInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual UTexture* GetCrosshairDefaultTexture_Implementation() const override;

    virtual FVector2D GetCrosshairTextureSize_Implementation() const override;

    virtual FVector2D GetCrosshairTextureHalfSize_Implementation() const override;

    virtual FVector2D GetCrosshairScreenOffset_Implementation() const override;

    virtual FVector2D GetCrosshairScreenLocation_Implementation() const override;

    virtual float GetTraceDistance_Implementation() const override;

    virtual FVector GetTargetPickUpLocation_Implementation() override;

    virtual FRotator GetCameraRotation_Implementation() const override;

    virtual float GetTraceDelay_Implementation() override;


    /////////////////////////////////////////////////////////////////
    /// IEAttributesActorInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual UEAttributesComponent* GetAttributesComponent_Implementation() const override;

    virtual void UpdateParentAttributesComponent_Implementation(const AActor* InParent) override;


    /////////////////////////////////////////////////////////////////
    /// IEPickableItemConsumerInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual void SubscribePickableItemDelegates_Implementation(AEPickableItem* InItem) override;


    /////////////////////////////////////////////////////////////////
    /// IEBoneTransformTrackerInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual FTransform GetTransform_Implementation() const override;

    virtual void UpdateTransform_Implementation(const FTransform& InUpdatedTransform) override;


    /////////////////////////////////////////////////////////////////
    /// IEPeriodicTraceConditionInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual bool IsCurrentlyTraceable_Implementation() override;


    /////////////////////////////////////////////////////////////////
    /// IEInputActionControlInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual void SubscribeCapsuleOverlapEvents_Implementation(UEInputActionHandler* InHandler, const FName& InBeginOverlapMethodName, const FName& InEndOverlapMethodName) override;

    virtual void SubscribeTraceHitEvent_Implementation(UEInputActionHandler* InHandler, const FName& InTraceHitHandlerMethodName) override;

    virtual void UpdateTraceOwner_Implementation(UEInputActionHandler* InTraceOwner) override;

    virtual bool IsTraceItemInventoryAddable_Implementation(AActor* InFoundItem) const override;

    virtual void SubscribeWeaponAmmoOutEvent_Implementation(UEInputActionHandler* InHandler, const FName& InWeaponAmmoOutHandlerMethodName) override;

    virtual bool IsReloadPossible_Implementation() const override;

    virtual AEAmmo* GetCurrentWeaponAmmo_Implementation() const override;

    virtual void ReloadCurrentWeapon_Implementation(AEAmmo* InNewAmmo) override;

    virtual bool IsAttackPossible_Implementation() const override;

    virtual void Attack_Implementation(const FVector& InWorldPosition, const FVector& InWorldRotation) override;


    //
    // Getters And Setters
    //

    FORCEINLINE bool IsAiming() const;

    FORCEINLINE bool IsReloading() const;

    FORCEINLINE bool IsLeftHandTransformTrackingEnabled() const;

    FORCEINLINE FTransform GetStockSocketTransform() const;

};
