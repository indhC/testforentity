﻿// © TestForEntity 2023.07.23:12:22 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EDummy.h"

#include "Components/BoxComponent.h"
#include "TestForEntity/Components/Attributes/EAttributesComponent.h"
#include "TestForEntity/Components/Generators/ELootGeneratorComponent.h"
#include "TestForEntity/Utilities/ECombatFunctionLibrary.h"


// Sets default values
AEDummy::AEDummy(): HealthAttributeNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)}
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
    SetRootComponent(SkeletalMeshComponent);

    CollisionBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBoxComponent"));
    CollisionBoxComponent->SetupAttachment(SkeletalMeshComponent);

    LootGeneratorComponent = CreateDefaultSubobject<UELootGeneratorComponent>(TEXT("LootGeneratorComponent"));
    LootGeneratorComponent->SetupAttachment(SkeletalMeshComponent);

    AttributesComponent = CreateDefaultSubobject<UEAttributesComponent>(TEXT("AttributesComponent"));
}

// Called when the game starts or when spawned
void AEDummy::BeginPlay()
{
    Super::BeginPlay();

    AttributesComponent->OnAttributeChangeDelegate.AddDynamic(this, &AEDummy::OnAttributeChangedEvent);
}

void AEDummy::OnAttributeChangedEvent_Implementation(const AActor* InInstigator, const UEAttributesComponent* InInstigatorAttributesComponent, const FGameplayTag& InAttributeTag,
                                                     const float InNewValue, const float InValue, const float InValueDelta)
{
    FEAttributeDescriptor Attribute;
    AttributesComponent->GetAttribute(InAttributeTag, Attribute);
    if (Attribute.IsValueMinimum())
    {
        LootGeneratorComponent->SpawnLoot();
        AttributesComponent->ResetAttribute(this, HealthAttributeNameTag);
    }
}


/////////////////////////////////////////////////////////////////
/// IEAttributesActorInterface Implementation
/////////////////////////////////////////////////////////////////

UEAttributesComponent* AEDummy::GetAttributesComponent_Implementation() const
{
    return AttributesComponent;
}

void AEDummy::UpdateParentAttributesComponent_Implementation(const AActor* InParent)
{
    UECombatFunctionLibrary::UpdateParentAttributesComponent(InParent, AttributesComponent);
}
