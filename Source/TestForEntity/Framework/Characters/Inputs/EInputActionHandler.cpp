﻿// © TestForEntity 2023.07.21:03:31 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EInputActionHandler.h"

#include "GameFramework/Character.h"
#include "TestForEntity/Components/Combats/Interfaces/ECombatScreenConfigInterface.h"
#include "TestForEntity/Interfaces/EInputActionControlInterface.h"


void UEInputActionHandler::Initialize_Implementation(const FEInputActionHandlerConfigRow& InConfigRow, ACharacter* InOwner)
{
    ensureAlways(InOwner && InOwner->Implements<UEInputActionControlInterface>() && InOwner->Implements<UECombatScreenConfigInterface>());

    CurrentCharacter = InOwner;

    HandleTriggerEvent = InConfigRow.HandleTriggerEvent;
    InputAction = InConfigRow.InputAction;
}

void UEInputActionHandler::Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent)
{
    InEnhancedInputComponent->BindAction(InputAction, HandleTriggerEvent, this, &UEInputActionHandler::Handle);
}

bool UEInputActionHandler::MayBeExecuted_Implementation() const
{
    return true;
}

void UEInputActionHandler::Handle_Implementation(const FInputActionValue& InValue)
{
}
