﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "EAttributesActorInterface.generated.h"


// This class does not need to be modified.
UINTERFACE()
class UEAttributesActorInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IEAttributesActorInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attributes Actor")
    class UEAttributesComponent* GetAttributesComponent() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attributes Actor")
    void UpdateParentAttributesComponent(const AActor* InParent);

};
