﻿// © TestForEntity 2023.07.03:19:42 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"
#include "Interfaces/EReloadableItemInterface.h"
#include "ECombatComponent.generated.h"


USTRUCT(BlueprintType)
struct FECombatConfigTableRow : public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    FGameplayTag DefaultWeaponNameTag{FGameplayTag::RequestGameplayTag(NAME_Name, false)};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    FGameplayTag DefaultAmmoNameTag{FGameplayTag::RequestGameplayTag(NAME_Name, false)};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    FName OwnerWeaponSocketName{NAME_None};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    TObjectPtr<UAnimMontage> HipFireAnimMontage;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    FName HipFireAnimMontageSectionName{NAME_None};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    TObjectPtr<UAnimMontage> HipReloadAnimMontage;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Configuration")
    FName HipReloadAnimMontageSectionName{NAME_None};
};


UENUM(BlueprintType)
enum class EECombatState: uint8
{
    ECCS_Unoccupied UMETA(DisplayName = "State.Unoccupied"),
    ECCS_Firing UMETA(DisplayName = "State.Firing"),
    ECCS_Reloading UMETA(DisplayName = "State.Reloading"),
};


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TESTFORENTITY_API UECombatComponent : public UActorComponent, public IEReloadableItemInterface
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> CombatConfigDataTable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat|Configuration", meta = (AllowPrivateAccess = "true"))
    FGameplayTag CombatConfigRowNameTag;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration", meta = (AllowPrivateAccess = "true"))
    FGameplayTag DefaultWeaponNameTag;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration", meta = (AllowPrivateAccess = "true"))
    FGameplayTag DefaultAmmoNameTag;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration", meta = (AllowPrivateAccess = "true"))
    FName OwnerWeaponSocketName;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Firing", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UAnimMontage> HipFireAnimMontage;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Firing", meta = (AllowPrivateAccess = "true"))
    FName HipFireAnimMontageSectionName;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Reloading", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UAnimMontage> HipReloadAnimMontage;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Reloading", meta = (AllowPrivateAccess = "true"))
    FName HipReloadAnimMontageSectionName;


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<AActor> CurrentOwner;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    EECombatState State;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class AEWeapon> CurrentWeapon;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<AEAmmo> ReplacedAmmo;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    bool bCanShoot;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    bool bAiming;

protected:

    // Called when the game starts
    virtual void BeginPlay() override;


    template<class TTimerEventHandler>
    void StartEventTimer(TTimerEventHandler InTimerHandler, const float InRate) const
    {
        FTimerHandle ActionTimerHandler;
        FTimerDelegate TimerDelegate;
        TimerDelegate.BindLambda(InTimerHandler);
        CurrentOwner->GetWorldTimerManager().SetTimer(ActionTimerHandler, TimerDelegate, InRate, false);
    }


    UFUNCTION(BlueprintCallable, Category = "Combat|Weapon")
    void LoadTableConfig();

public:

    // Sets default values for this component's properties
    UECombatComponent();


    //
    // Delegates
    //

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoRanOut, const AEWeapon*, InCurrentWeapon);
    UPROPERTY(BlueprintAssignable, Category = "Combat|Weapon")
    FOnAmmoRanOut OnAmmoRanOutDelegate;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnOldClipRemoved, const AEWeapon*, InCurrentWeapon, class AEAmmo*, InOldAmmo);
    UPROPERTY(BlueprintAssignable, Category = "Combat|Weapon|Reloading")
    FOnOldClipRemoved OnOldClipRemovedDelegate;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloaded, const AEWeapon*, InCurrentWeapon, class AEAmmo*, InNewAmmo);
    UPROPERTY(BlueprintAssignable, Category = "Combat|Weapon|Reloading")
    FOnWeaponReloaded OnWeaponReloadedDelegate;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat|Weapon")
    void EquipWeapon(AEWeapon* InWeapon);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat|Weapon")
    bool CanAttack() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat|Weapon")
    void Attack(const FVector& InStartLocation, const FVector& InDirection);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat|Weapon")
    bool IsUnoccupied() const;


    /////////////////////////////////////////////////////////////////
    /// IEReloadableItemInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual bool MayBeReloaded_Implementation() const override;

    virtual bool Reload_Implementation(AEAmmo* InNewAmmo) override;

    virtual void GrabClip_Implementation() override;

    virtual void DropClip_Implementation() override;

    virtual void ReplaceClip_Implementation() override;


    //
    // Getters And Setters
    //

    FORCEINLINE const FGameplayTag& GetDefaultWeaponNameTag() const
    {
        return DefaultWeaponNameTag;
    }

    FORCEINLINE const FGameplayTag& GetDefaultAmmoNameTag() const
    {
        return DefaultAmmoNameTag;
    }

    FORCEINLINE bool IsAiming() const
    {
        return bAiming;
    }

    FORCEINLINE bool IsReloading() const
    {
        return EECombatState::ECCS_Reloading == State;
    }

    FORCEINLINE const USkeletalMeshSocket* GetStockSocket() const;

    FORCEINLINE FTransform GetStockSocketTransform() const;

};
