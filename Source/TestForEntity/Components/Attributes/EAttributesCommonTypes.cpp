﻿// © TestForEntity 2023.07.07:18:37 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EAttributesCommonTypes.h"

#include "EAttributesComponent.h"


namespace EAttributesComponent
{
    constexpr float GMultiplierPositive{1.0f};
    constexpr float GMultiplierNegative{-1.0f};
}


void FEAttributeDescriptor::Initialize(const UEAttributesComponent* InAttributesOwner, const FEAttributeConfigurationDataTableRow* InAttributeConfigurationRow)
{
    if (!InAttributesOwner || !InAttributeConfigurationRow)
    {
        return;
    }

    bInvalid = false;

    bActive = InAttributeConfigurationRow->bActive;
    if (bActive)
    {
        Affecting = InAttributeConfigurationRow->Affecting;
    }

    bPeriodic = InAttributeConfigurationRow->bPeriodic;
    Period = InAttributeConfigurationRow->Period;
    Duration = InAttributeConfigurationRow->Duration;

    AffectedBy = InAttributeConfigurationRow->AffectedBy;

    MinimumValue = InAttributeConfigurationRow->MinimumValue;
    MaximumValue = InAttributeConfigurationRow->MaximumValue;
    MaximumValueAttributeTag = InAttributeConfigurationRow->MaximumValueAttributeTag;
    const float CalculatedMaximum{GetCorrectValue(InAttributesOwner, MaximumValueAttributeTag, MaximumValue)};

    if (!MaximumValueAttributeTag.IsValid())
    {
        const float Maximum{FMath::Max(MinimumValue, MaximumValue)};
        MinimumValue = FMath::Min(MinimumValue, MaximumValue);
        MaximumValue = Maximum;
    }

    DefaultValueAttributeTag = InAttributeConfigurationRow->DefaultValueAttributeTag;
    DefaultValue = FMath::Clamp(GetCorrectValue(InAttributesOwner, DefaultValueAttributeTag, InAttributeConfigurationRow->DefaultValue), MinimumValue, CalculatedMaximum);

    Value = DefaultValue;
}

float FEAttributeDescriptor::GetAdditionOperationDelta(const float InDelta, const bool bInNegative, const float InMaximum) const
{
    const float Multiplier{(bInNegative) ? (EAttributesComponent::GMultiplierNegative) : (EAttributesComponent::GMultiplierPositive)};
    const float ValueDelta{(bInNegative) ? (Value - MinimumValue) : (InMaximum - Value)};
    return Multiplier * FMath::Min(ValueDelta, FMath::Abs(InDelta));
}

float FEAttributeDescriptor::Update(const UEAttributesComponent* InAttributesOwner, const float InDelta)
{
    if (bInvalid)
    {
        return InDelta;
    }

    const bool bNegative{InDelta < 0.0f};
    const float Maximum{GetCorrectValue(InAttributesOwner, MaximumValueAttributeTag, MaximumValue)};
    const bool bAdditionOperation{EEEffectOperation::EEO_Addition == EffectOperation};
    if (bAdditionOperation && ((bNegative && (Value <= MinimumValue)) || (!bNegative && (Value >= Maximum))))
    {
        return InDelta;
    }

    const float Result{(bAdditionOperation) ? (GetAdditionOperationDelta(InDelta, bNegative, Maximum)) : (InDelta)};
    Value = (bAdditionOperation) ? (Value + Result) : (Value * Result);
    Value = FMath::Clamp(Value, MinimumValue, Maximum);
    return Result;
}

float FEAttributeDescriptor::GetCorrectValue(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeTag, const float InDefaultValue)
{
    float Result{InDefaultValue};
    if (InAttributeTag.IsValid() && InAttributesOwner)
    {
        FEAttributeDescriptor Attribute;
        InAttributesOwner->GetAttribute(InAttributeTag, Attribute);
        Result = (Attribute.IsInvalid()) ? (Result) : (Attribute.GetValue());
    }
    return Result;
}
