﻿// © TestForEntity 2023.07.21:19:21 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "EInputActionControlInterface.generated.h"


// This class does not need to be modified.
UINTERFACE()
class UEInputActionControlInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IEInputActionControlInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    //
    // Trace And Interaction Control
    //

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Trace And Interaction")
    void SubscribeCapsuleOverlapEvents(class UEInputActionHandler* InHandler, const FName& InBeginOverlapMethodName, const FName& InEndOverlapMethodName);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Trace And Interaction")
    void SubscribeTraceHitEvent(UEInputActionHandler* InHandler, const FName& InTraceHitHandlerMethodName);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Trace And Interaction")
    void UpdateTraceOwner(UEInputActionHandler* InTraceOwner);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Trace And Interaction")
    bool IsTraceItemInventoryAddable(AActor* InFoundItem) const;


    //
    // Attack And Reload Control
    //

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Attack And Reload")
    void SubscribeWeaponAmmoOutEvent(UEInputActionHandler* InHandler, const FName& InWeaponAmmoOutHandlerMethodName);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Attack And Reload")
    bool IsReloadPossible() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Attack And Reload")
    class AEAmmo* GetCurrentWeaponAmmo() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Attack And Reload")
    void ReloadCurrentWeapon(AEAmmo* InNewAmmo);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Attack And Reload")
    bool IsAttackPossible() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Control Interface|Attack And Reload")
    void Attack(const FVector& InWorldPosition, const FVector& InWorldRotation);

};
