﻿// © TestForEntity 2023.07.23:02:48 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"

#include "EJumpInputActionHandler.generated.h"


/**
 * 
 */
UCLASS()
class TESTFORENTITY_API UEJumpInputActionHandler : public UEInputActionHandler
{
    GENERATED_BODY()

public:

    virtual void Handle_Implementation(const FInputActionValue& InValue) override;

};
