﻿// © TestForEntity 2023.07.03:22:57 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"
#include "EInventory.generated.h"


USTRUCT(BlueprintType)
struct FESlotByItemTypeConfig
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot By Type Config")
    TSubclassOf<class AEPickableItem> ItemType;

    /**
     * Whether the number of Slots for the current Item type should be fixed. Primary Sorting Attribute: all Items with a fixed number of Slots (true value) MUST BE closer to
     * the top of the Inventory (index 0) than Items with a non-fixed number of Slots (false value)!
     */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot By Type Config")
    bool bFixedItemTypeSlotsCount{false};

    /**
     * Sort Order Priority: The lower the value, the closer the Slot is to the first (Slot index 0) Inventory Slot. This value is not declarative, but just an advice of how
     * close to the start of the Inventory the Slot corresponding to the SortOrder value should be in its group. Depending on the value of the main sorting attribute
     * bFixedItemTypeSlotsCount, the SortOrder can be adjusted accordingly to place all fixed slot Items closer to the start of the Inventory (index 0) than non-fixed Slot
     * Items.
     */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot By Type Config")
    int32 SortOrder{0};

    /** Should Inventory Slots for this Items be additionally sorted by Item type name */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot By Type Config")
    bool bSortableByTypeName{false};

    /** Should empty Slots of this Item type also be sorted along with non-empty Slots */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot By Type Config")
    bool bEmptySlotSortable{false};

    /** Limiting the maximum number of Slots in the Inventory for an Item type */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot By Type Config")
    int32 ItemTypeSlotsCount{0};
};


USTRUCT(BlueprintType)
struct FEInventoryConfigTableRow : public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory Configuration")
    int32 MaximumInventorySlotsCount{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory Configuration")
    int32 RowsCount{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory Configuration")
    int32 ColumnsCount{0};

    /** The original Slots Configuration may differ from the actual Inventory Slots Configuration.  */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory Configuration")
    TArray<FESlotByItemTypeConfig> SupportedItemTypeSlotConfigs;
};


USTRUCT(BlueprintType)
struct FEInventorySlot
{
    GENERATED_BODY()


    FEInventorySlot(): ActualItemType(nullptr)
    {
        UniqueId = FGuid::NewGuid();
    }

    explicit FEInventorySlot(const FESlotByItemTypeConfig& InConfiguration): FEInventorySlot()
    {
        Configuration = InConfiguration;
    }


    FORCEINLINE const FGuid& GetUniqueId() const
    {
        return UniqueId;
    }

    FORCEINLINE const FESlotByItemTypeConfig& GetConfiguration() const
    {
        return Configuration;
    }

    FORCEINLINE bool IsEmpty() const;

    FORCEINLINE int32 GetSortOrder() const
    {
        return Configuration.SortOrder;
    }

    FORCEINLINE bool IsSortableByTypeName() const
    {
        return Configuration.bSortableByTypeName;
    }

    FORCEINLINE const TSubclassOf<AEPickableItem>& GetItemType() const
    {
        return Configuration.ItemType;
    }

    FORCEINLINE const TSubclassOf<AEPickableItem>& GetActualItemType() const
    {
        return ActualItemType;
    }

    FORCEINLINE int32 GetItemsCount() const
    {
        return Items.Num();
    }

    FORCEINLINE bool ContainsItem(const AEPickableItem* InItem) const;

    FORCEINLINE const TArray<TObjectPtr<AEPickableItem>>& GetItems() const
    {
        return Items;
    }

    void AddItem(AEPickableItem* InItem);

    void RemoveItem(AEPickableItem* InItem);

    void RemoveItemAt(const int32 InIndex);

    DECLARE_DELEGATE_RetVal_TwoParams(bool, FOnComparingNeighbourItems, const AEPickableItem&, const AEPickableItem&);
    FORCEINLINE void SortItems(const FOnComparingNeighbourItems& InPredicate);

    FORCEINLINE bool operator==(const FEInventorySlot& InAnotherSlot) const
    {
        return UniqueId == InAnotherSlot.UniqueId;
    }

    FORCEINLINE AEPickableItem* operator[](const int32 InIndex) const
    {
        return (Items.IsValidIndex(InIndex)) ? (Items[InIndex].Get()) : (nullptr);
    }

private:

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot|Unique ID", meta = (AllowPrivateAccess = "true"))
    FGuid UniqueId;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot|Configuration", meta = (AllowPrivateAccess = "true"))
    FESlotByItemTypeConfig Configuration;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<AEPickableItem> ActualItemType;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot", meta = (AllowPrivateAccess = "true"))
    TArray<TObjectPtr<AEPickableItem>> Items;

};


USTRUCT(BlueprintType)
struct FEInventorySlotCachedConfig
{
    GENERATED_BODY()


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot Cached Config")
    FESlotByItemTypeConfig Config;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot Cached Config")
    int32 StartIndex{0};

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory Slot Cached Config")
    int32 EndIndex{0};
};


/**
 * Default abstract implementation of the Inventory. As generic as possible
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class TESTFORENTITY_API UEInventory : public UObject
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    FGameplayTag InventoryConfigRowNameTag;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> InventoryConfigTable;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 MaximumInventorySlotsCount;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 RowsCount;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 ColumnsCount;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
    TArray<FEInventorySlot> Slots;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
    TMap<TSubclassOf<AEPickableItem>, FEInventorySlotCachedConfig> BaseItemTypeSlotCachedItemTypeConfigMap;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
    int32 FreeSlotIndex;

protected:

    enum class EESearchConditionState: uint8
    {
        ESCS_NotFound,
        ESCS_Found,
        ESCS_FirstFound,
        ESCS_BetterMatchFound
    };

    DECLARE_DELEGATE_RetVal_TwoParams(EESearchConditionState, FSlotSearchConditionSignature, const TSubclassOf<AEPickableItem>&, const FEInventorySlot&);
    int32 FindSlotIndex(const TSubclassOf<AEPickableItem>& InNewItemType, const FSlotSearchConditionSignature& InSearchConditionDelegate, const int32 InMaximumIndex) const;

    DECLARE_DELEGATE_OneParam(FItemsProcessingCallbackSignature, const FEInventorySlot*);
    void ProcessSlotItemsByCondition(const TSubclassOf<AEPickableItem>& InBaseItemType, const FItemsProcessingCallbackSignature& InCallbackDelegate, const bool bInIncludeNotFilled) const;


    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TSubclassOf<AEPickableItem> FindBaseItemType(const TSubclassOf<AEPickableItem>& InItemType) const;

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    int32 FindSlotIndexByItemType(const TSubclassOf<AEPickableItem>& InItemType, const bool bInIncludeEmptySlots = true) const;

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    int32 FindFreeSlot(const TSubclassOf<AEPickableItem>& InItemType) const;

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    bool IsItemValid(const AEPickableItem* InNewItem) const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    bool CanAddItemByAdditionalConditions(AEPickableItem* InNewItem, TSubclassOf<AEPickableItem> InBaseType) const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void AddItemToSlot(AEPickableItem* InNewItem, const int32 InSlotIndex);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    bool CompareSlotConfigs(const FESlotByItemTypeConfig& InLeft, const FESlotByItemTypeConfig& InRight) const;

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    void SortSlots();

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<AEPickableItem*> RemoveItemsFromSlot(FEInventorySlot& InSlot, const int32 InCount = 1);


    //
    // Getters And Setters
    //

    TArray<FEInventorySlot>& GetSlotsRef()
    {
        return Slots;
    }

    const TArray<FEInventorySlot>& GetSlotsConst() const
    {
        return Slots;
    }

public:

    UEInventory();


    bool IsSlotConfigInvalid(const FESlotByItemTypeConfig& SlotConfig, const int32 InConfigIndex) const;

    virtual void PostInitProperties() override;


    //
    // Delegates
    //

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemPickedUp, AEPickableItem*, InDroppedItem);

    UPROPERTY(BlueprintAssignable)
    FOnItemPickedUp OnItemPickedUpDelegate;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemDropped, AEPickableItem*, InDroppedItem);

    UPROPERTY(BlueprintAssignable)
    FOnItemDropped OnItemDroppedDelegate;


    UFUNCTION(BlueprintCallable, Category = "Inventory")
    bool CanAddItem(AEPickableItem* InNewItem) const;

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    int32 Add(AEPickableItem* InNewItem, const bool bInSkipAddValidation = false);

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    AEPickableItem* Peek(const TSubclassOf<AEPickableItem>& InItemType);

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    AEPickableItem* PeekBySlotNumber(const int32 InSlotNumber);

    /**
     * Attempts to remove the specified Item from an Inventory
     * @param InItem const AEPickableItem* instance of the specified Item to remove from the Inventory
     * @return AEPickableItem* instance which determines whether the specified InItem has been removed from the Inventory (InItem instance) or not (nullptr value)
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    AEPickableItem* Remove(const AEPickableItem* InItem);

    /**
     * Removes the specified InCount number of Items from an Inventory Slot which corresponds to the specified InItemType
     * @param InItemType const TSubclassOf<AEPickableItem>& InItemType instance of the specified Item type to remove from the Inventory
     * @param InCount const int32 value (default value: 1) which determines count of the Items of InItemType which should be removed from the Inventory
     * @return TArray<AEPickableItem*> array no larger than InCount which contains the Items removed from the Slot corresponding to InSlotNumber number. An empty array can be 
     * returned in the following cases:
     * 1. Slot with the InSlotNumber number is not found;
     * 2. Slot with the InSlotNumber number does not contain Items (Slot is empty);
     * 3. InCount <= 0
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<AEPickableItem*> RemoveOfType(const TSubclassOf<AEPickableItem>& InItemType, const int32 InCount = 1);

    /**
     * Removes the specified count of InCount Items from the Inventory Slot with the specified InSlotNumber number
     * @param InSlotNumber const int32 value of the specified Slot number
     * @param InCount const int32 value of the Items count to be removed from the Inventory Slot with the InSlotNumber number
     * @return TArray<AEPickableItem*> array no larger than InCount which contains the Items removed from the Slot corresponding to InSlotNumber number. An empty array can be 
     * returned in the following cases:
     * 1. Slot with the InSlotNumber number is not found;
     * 2. Slot with the InSlotNumber number does not contain Items (Slot is empty);
     * 3. InCount <= 0
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<AEPickableItem*> RemoveFromSlot(const int32 InSlotNumber, const int32 InCount = 1);

    /**
     * Determines what should be done with the used Item (already used and replaced with a more filled Item, etc.) for the current implementation of the Inventory
     * @param InOldItem AEPickableItem* instance of an used Item (Item was used and replaced with a more filled Item, etc.)
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void ProcessUsedItem(AEPickableItem* InOldItem);

    /**
     * Gets the index of the first Slot and the index of the end Slot for the specified Item type. Determines a group of Slots (with a range of its indices) by the Item's base
     * type. Returns {-1, -1} if for InItemType it is not possible to find the corresponding group of Inventory Slots by base Item type
     * @param InItemType const TSubclassOf<AEPickableItem>& instance of the Item type
     * @param OutStartIndex int32& value which returns the starting index of the Slots group
     * @param OutEndIndex int32& value which returns the ending index of the Slots group
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    void GetItemTypesSlotIndexesRange(const TSubclassOf<AEPickableItem>& InItemType, int32& OutStartIndex, int32& OutEndIndex) const;

    /**
     * Determines whether there is a Slot for Items which are of the specified InBaseItemType or one of the InBaseItemType descendant types
     * @param InBaseItemType const TSubclassOf<AEPickableItem>& instance of the base Item type
     * @return bool value which specifies whether the Slot for Items of the specified InBaseItemType or one of its descendants type is found
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    bool ContainsItemTypeSlot(const TSubclassOf<AEPickableItem>& InBaseItemType) const;

    /**
     * Gets the Item type of the first Slot for Items which are of the specified InBaseItemTypes or one of InBaseItemTypes' descendant types
     * @param InBaseItemTypes const TArray<TSubclassOf<AEPickableItem>>& array of the base Item types
     * @return TSubclassOf<AEPickableItem> instance which specifies the Item type of the first found Slot for Items of the specified InBaseItemTypes or one of its descendants
     * type
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TSubclassOf<AEPickableItem> ContainsItemTypesSlot(const TArray<TSubclassOf<AEPickableItem>>& InBaseItemTypes) const;

    /**
     * Counts the number of Slots for Items which are of the specified InBaseItemType or one of InBaseItemType's descendant types
     * @param InBaseItemType const TSubclassOf<AEPickableItem>& instance of the base Item type
     * @param bInIncludeNotFilled const bool value (default value is true) which specifies whether only fully filled Slots should be counted
     * @return int32 value which specifies the number of all found Slots for Items of the specified InBaseItemType or one of its descendants type
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    int32 GetItemTypeSlotsCount(const TSubclassOf<AEPickableItem>& InBaseItemType, const bool bInIncludeNotFilled = true) const;

    /**
     * Gets the Slot index of an Item in the Inventory
     * @param InItem const AEPickableItem* instance of the Item
     * @return int32 value which specifies the Slot index of the Item in the Inventory
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    int32 GetItemSlotIndex(const AEPickableItem* InItem) const;

    /**
     * Looks for Items which are of the specified InBaseItemType or one of the InBaseItemType descendant types
     * @param InBaseItemType const TSubclassOf<AEPickableItem>& instance of the base Item type
     * @return TArray<AEPickableItem*> array which contains all found Items of the specified InBaseItemType or one of its descendants type
     */
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<AEPickableItem*> FindItemsByType(const TSubclassOf<AEPickableItem>& InBaseItemType) const;

};
