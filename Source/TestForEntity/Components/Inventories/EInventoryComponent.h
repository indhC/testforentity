﻿// © TestForEntity 2023.07.07:18:39 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EInventoryComponent.generated.h"


UENUM(BlueprintType)
enum class EEInventoryOperationStatus: uint8
{
    SSuccess UMETA(DisplayName = "Status.SSuccess"),

    SNoDroppingItems UMETA(DisplayName = "Status.SNoDroppingItems"),
    SDroppingUsedItem UMETA(DisplayName = "Status.SDroppingUsedItem"),
    SPickingUpUsedItem UMETA(DisplayName = "Status.SPickingUpUsedItem"),
    SItemDroppingStarted UMETA(DisplayName = "Status.SItemDroppingStarted"),
    SItemDroppingEnded UMETA(DisplayName = "Status.SItemDroppingEnded"),

    FFreeInventorySlotsAreOut UMETA(DisplayName = "Status.FFreeInventorySlotsAreOut"),
    FWeaponSlotsAreOut UMETA(DisplayName = "Status.FWeaponSlotsAreOut"),

    FInvalidWeaponIndex UMETA(DisplayName = "Status.FInvalidWeaponIndex"),
    FOutOfAmmo UMETA(DisplayName = "Status.FOutOfAmmo"),
    FSupportedAmmoTypeNotFound UMETA(DisplayName = "Status.FSupportedAmmoTypeNotFound"),
    FInvalidAmmoType UMETA(DisplayName = "Status.FInvalidAmmoType"),
};


/**
 * A Component which is used to manage the Inventory and the Items stored in the Inventory
 * @note This implementation is far from the best, but it is quite simple. We currently only have 3 main Item types: AEAmmo, AEWeapon, and Potion-Like Items. Potion-Like
 * Items are used (or ignored) immediately when picking up, drop, etc./attempting to pick up, drop, etc. such Item. And at the moment, only AEAmmo and AEWeapon are collected
 * into the Inventory. Selecting logic to pick up, drop, etc. an Item based on its type using 'if()' will need to be replaced with Strategies (or something similar) to decouple
 * the core logic of the UEInventoryComponent from the logic to pick up, drop, etc. an Item based on its type. For now, we will need to insert an 'else if()' and a
 * corresponding pick up, drop, etc. method into the UEInventoryComponent for each new Item type which should be placed into the Inventory
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TESTFORENTITY_API UEInventoryComponent : public UActorComponent
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<class UEInventory> InventoryType;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UEInventory> Inventory;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
    int32 CurrentWeaponIndex;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<class AEAmmo> CurrentAmmoType;

protected:

    // Called when the game starts
    virtual void BeginPlay() override;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    int32 FindNewExistingWeaponIndex(class AEWeapon* InWeapon);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void PickUpItemWithStatus(class AEPickableItem* InItem, const EEInventoryOperationStatus InPickUpStatus = EEInventoryOperationStatus::SSuccess);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void PickUpAmmo(AEAmmo* InAmmo, const EEInventoryOperationStatus InPickUpStatus);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void PickUpWeapon(AEWeapon* InWeapon, const EEInventoryOperationStatus InPickUpStatus);

    /**
     * Attempts to change the CurrentWeaponIndex to the specified InNewIndex. The index determines the current active Weapon
     * @param InNewIndex const int32 value which defines the new index of the current Weapon
     * @param bInValidateIndex const bool value which determines whether the specified InWeaponIndex has been checked and is valid (true value)
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void SwapCurrentWeaponIndex(const int32 InNewIndex, const bool bInValidateIndex = true);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    TSubclassOf<AEAmmo> UpdateCurrentAmmoType(const AEWeapon* InWeapon);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void DropRemovedItems(const TArray<class AEPickableItem*>& InRemovedItems, const bool bInCurrentWeaponExists);

    /** Drops the specified Weapon */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void DropWeapon(AEWeapon* InWeapon, const bool bInCurrentWeaponExists);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void DropSingleItem(AEPickableItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void OnItemPickedUpByInventoryEvent(AEPickableItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void OnItemDroppedFromInventoryEvent(AEPickableItem* InItem);

public:

    // Sets default values for this component's properties
    UEInventoryComponent();


    // Called every frame
    virtual void TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


    //
    // Delegates
    //

    /**
     * Related EEInventoryOperationStatus statuses:
     * 1. SSuccess;
     * 2. FInvalidWeaponIndex
     */
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnWeaponSwapped, const EEInventoryOperationStatus, InStatus, const AEWeapon*, InNewWeapon,
                                                  const int32, InPreviousWeaponIndex, const int32, InNewWeaponIndex);

    UPROPERTY(BlueprintAssignable)
    FOnWeaponSwapped OnWeaponSwappedDelegate;

    /**
     * Related EEInventoryOperationStatus statuses:
     * 1. SSuccess;
     * 2. SAmmoTypeUpdated;
     * 3. FOutOfAmmo;
     * 4. FInvalidAmmoType
     */
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnWeaponAmmoRetrivied, const EEInventoryOperationStatus, InStatus, const AEWeapon*, InCurrentWeapon,
                                                  TSubclassOf<AEAmmo>, InCurrentAmmoType, const int32, InCurrentAmmoCount);

    UPROPERTY(BlueprintAssignable)
    FOnWeaponAmmoRetrivied OnWeaponReloadedDelegate;

    /**
     * Related EEInventoryOperationStatus statuses:
     * 1. SSuccess;
     * 2. FFreeInventorySlotsAreOut;
     * 3. FWeaponSlotsAreOut;
     */
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemPickedUp, const EEInventoryOperationStatus, InStatus, const AEPickableItem*, InNewItem);

    UPROPERTY(BlueprintAssignable)
    FOnItemPickedUp OnItemPickedUpDelegate;

    /**
     * Related EEInventoryOperationStatus statuses:
     * 1. SSuccess;
     * 2. FSupportedAmmoTypeNotFound
     */
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAmmoTypeSelected, const EEInventoryOperationStatus, InStatus, TSubclassOf<AEPickableItem>, InPreviousAmmoType,
                                                   TSubclassOf<AEPickableItem>, InNewAmmoType);

    UPROPERTY(BlueprintAssignable)
    FOnAmmoTypeSelected OnAmmoTypeSelectedDelegate;

    /**
     * Related EEInventoryOperationStatus statuses:
     * 1. SSuccess;
     * 2. SNoDroppingItems;
     * 3. SItemDroppingStarted;
     * 4. SItemDroppingEnded
     */
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemDropped, const EEInventoryOperationStatus, InStatus, const TArray<AEPickableItem*>&, InDroppingItems);

    UPROPERTY(BlueprintAssignable)
    FOnItemDropped OnItemDroppedDelegate;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    bool CanAddItem(AActor* InItem);

    /**
      * Attempts to change the CurrentWeaponIndex to the specified InNewIndex. The index determines the current active Weapon
      * @param InNewIndex const int32 value which defines the new index of the current Weapon
      */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void SwapWeapon(const int32 InNewIndex);

    /**
     * Performs additional actions for an Item which has already been used and which is being replaced with a new Item
     * @param InOldItem AEPickableItem* instance of an Item which has already been used and needs to be replaced with a new Item
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void ProcessUsedItem(AEPickableItem* InOldItem);

    /**
     * Returns the currently selected Ammo type for the current Weapon
     * @return TSubclassOf<AEAmmo> instance which specifies the current Ammo type selected for the current Weapon
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    TSubclassOf<AEAmmo> GetCurrentWeaponAmmoType() const;

    /**
     * Attempts to return the Ammo for the current Weapon located at the CurrentWeaponIndex in the Inventory Slots. Ammo may not be returned if there is no Ammo for the current
     * Weapon type in the Inventory
     * @return AEAmmo* instance of the new Weapon Ammo
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    AEAmmo* GetCurrentWeaponAmmo();

    /**
     * Selects the current Ammo type for the Weapon. The selected Weapon Ammo will be used to Reload the Weapon as long as there is at least one Ammo of the specified type in
     * the Inventory. The first available Ammo of a different type will be used for the current Weapon in case the selected type of Ammo for the Weapon runs out
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void SelectWeaponAmmoType(const AEAmmo* InAmmo);

    /**
     * Attempts to pick up an Item. An Item may not be picked up if there is no free Slot in the Inventory and the Item has a new Item type, or if there are no empty Slots and
     * the current Item type has reached the Inventory Slot limit
     * @param InItem AEPickableItem* instance which describes the Item which should be stored into the Inventory
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void PickUpItem(AEPickableItem* InItem);

    /**
     * Attempts to drop the specified Item from the Inventory
     * @param InItem const AEPickableItem* instance which specifies the Item to drop from the Inventory
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void DropItem(AEPickableItem* InItem);

    /**
     * Attempts to drop the specified number of the specified Item
     * @param InSlotNumber const int32 value which specifies the Items Slot number from which Items should be dropped
     * @param InCount const int32 value which specifies the desired count of InItem which should be dropped from the Inventory
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void DropItemsFromSlot(const int32 InSlotNumber, const int32 InCount);


    //
    // Getters And Setters
    //

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    FORCEINLINE AEWeapon* GetCurrentWeapon() const;

};
