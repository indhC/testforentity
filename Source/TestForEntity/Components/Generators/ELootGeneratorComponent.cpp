﻿// © TestForEntity 2023.07.04:01:29 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ELootGeneratorComponent.h"

#include "TestForEntity/Utilities/ECombatFunctionLibrary.h"
#include "TestForEntity/World/EPickableItem.h"
#include "TestForEntity/World/Ammo/EAmmo.h"
#include "TestForEntity/World/Weapons/EWeapon.h"


namespace ELootGeneratorComponent
{
    constexpr float GRandomMinimum{0.0f};
    constexpr float GRandomMaximum{1.0f};

    const TSubclassOf<AEPickableItem> GTypeAmmo{AEAmmo::StaticClass()};
    const TSubclassOf<AEPickableItem> GTypeWeapon{AEWeapon::StaticClass()};
}


// Sets default values for this component's properties
UELootGeneratorComponent::UELootGeneratorComponent(): DefaultLootGenerationRulesConfigNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)}
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UELootGeneratorComponent::BeginPlay()
{
    Super::BeginPlay();

    if (LootGenerationRulesConfigsDataTable)
    {
        ensureAlways(DefaultLootGenerationRulesConfigNameTag.IsValid());
    }

    ensureAlways(AmmoConfigsDataTable);
    ensureAlways(WeaponConfigsDataTable);
}

AEAmmo* UELootGeneratorComponent::SpawnAmmo_Implementation(const FGameplayTag& InAmmoNameTag)
{
    if (!AmmoConfigsDataTable || !InAmmoNameTag.IsValid())
    {
        return nullptr;
    }

    const FEAmmoConfigTableRow* AmmoRow{AmmoConfigsDataTable->FindRow<FEAmmoConfigTableRow>(InAmmoNameTag.GetTagName(), FString{}, false)};
    return Cast<AEAmmo>(UECombatFunctionLibrary::SpawnItem(GetWorld(), AmmoRow, ELootGeneratorComponent::GTypeAmmo, GetComponentLocation(), GetComponentRotation()));
}

AEWeapon* UELootGeneratorComponent::SpawnWeapon_Implementation(const FGameplayTag& InAmmoNameTag)
{
    if (!WeaponConfigsDataTable || !InAmmoNameTag.IsValid())
    {
        return nullptr;
    }

    const FEWeaponConfigTableRow* WeaponRow{WeaponConfigsDataTable->FindRow<FEWeaponConfigTableRow>(InAmmoNameTag.GetTagName(), FString{}, false)};
    return Cast<AEWeapon>(UECombatFunctionLibrary::SpawnItem(GetWorld(), WeaponRow, ELootGeneratorComponent::GTypeWeapon, GetComponentLocation(), GetComponentRotation()));
}

void UELootGeneratorComponent::SpawnLoot_Implementation()
{
    if (!LootGenerationRulesConfigsDataTable || !DefaultLootGenerationRulesConfigNameTag.IsValid())
    {
        return;
    }

    FELootGenerationRulesTableRow* Row{
        LootGenerationRulesConfigsDataTable->FindRow<FELootGenerationRulesTableRow>(DefaultLootGenerationRulesConfigNameTag.GetTagName(), FString{})
    };
    if (!ensureAlways(Row))
    {
        return;
    }

    for (const TTuple<FGameplayTag, FELootRulesConfig>& ItemTypeLootRulesMap : Row->ItemTypeLootRulesMap)
    {
        const FELootRulesConfig& LootRulesConfig{ItemTypeLootRulesMap.Value};
        if (!LootRulesConfig.ActualItemType)
        {
            continue;
        }

        for (int32 i = 0; i < LootRulesConfig.AttemptsCount; i++)
        {
            if (const float Chance{FMath::FRandRange(ELootGeneratorComponent::GRandomMinimum, ELootGeneratorComponent::GRandomMaximum)}; Chance > LootRulesConfig.SpawnChanceRange)
            {
                continue;
            }

            AEPickableItem* Item{nullptr};
            // The solution isn't perfect, but it's straightforward. This class may struggle when new AEPickableItem implementations are added
            if (LootRulesConfig.ActualItemType->IsChildOf(ELootGeneratorComponent::GTypeAmmo))
            {
                Item = SpawnAmmo(ItemTypeLootRulesMap.Key);
            }
            else if (LootRulesConfig.ActualItemType->IsChildOf(ELootGeneratorComponent::GTypeWeapon))
            {
                Item = SpawnWeapon(ItemTypeLootRulesMap.Key);
            }
            (Item) ? (Item->Drop()) : (false);
        }
    }
}
