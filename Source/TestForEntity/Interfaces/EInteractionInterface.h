﻿// © TestForEntity 2023.07.23:12:19 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "EInteractionInterface.generated.h"


UENUM(BlueprintType)
enum class EEInteractionType: uint8
{
    EIT_PickUp UMETA(DisplayName = "Type.PickUp"),
    EIT_SelfActing UMETA(DisplayName = "Type.SelfActing"),
};


// This class does not need to be modified.
UINTERFACE()
class UEInteractionInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IEInteractionInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interaction")
    void ShowPickUpWidget(AActor* InInstigator);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interaction")
    void HidePickUpWidget(AActor* InInstigator);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interaction")
    EEInteractionType GetInteractionType();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interaction")
    void Interact(AActor* InInstigator);

};
