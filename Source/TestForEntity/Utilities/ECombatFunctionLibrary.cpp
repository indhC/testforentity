﻿// © TestForEntity 2023.07.04:01:28 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ECombatFunctionLibrary.h"

#include "TestForEntity/Components/Attributes/EAttributesComponent.h"
#include "TestForEntity/Components/Attributes/Interfaces/EAttributesActorInterface.h"
#include "TestForEntity/Interfaces/EMeshActorInterface.h"
#include "TestForEntity/World/EPickableItem.h"


namespace ECombatFunctionLibrary
{
    constexpr float GFailedAnimationPlay{0.0f};
}


AEPickableItem* UECombatFunctionLibrary::SpawnItem(UWorld* InWorld, const FEPickableItemConfigTableRow* InItemConfigRow, const TSubclassOf<AEPickableItem> InExpectedItemType,
                                                   const FVector& InLocation, const FRotator& InRotation)
{
    if (!ensureAlways(InItemConfigRow) || !ensureAlways(InItemConfigRow->ActualType) || !InItemConfigRow->ActualType->IsChildOf(InExpectedItemType))
    {
        return nullptr;
    }

    FActorSpawnParameters SpawnParameters;
    SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
    AEPickableItem* Result{InWorld->SpawnActor<AEPickableItem>(InItemConfigRow->ActualType, InLocation, InRotation, SpawnParameters)};
    if (Result)
    {
        Result->Initialize(InItemConfigRow);
    }
    return Result;
}

void UECombatFunctionLibrary::UpdateParentAttributesComponent(const AActor* InParent, UEAttributesComponent* InCurrentAttributesComponent)
{
    if (InParent && InParent->Implements<UEAttributesActorInterface>())
    {
        InCurrentAttributesComponent->SetParentAttributesComponent(IEAttributesActorInterface::Execute_GetAttributesComponent(InParent));
    }
    else
    {
        InCurrentAttributesComponent->SetParentAttributesComponent(nullptr);
    }
}

float UECombatFunctionLibrary::PlayAnimationMontage(AActor* InOwner, UAnimMontage* InAnimMontage, const FName& InMontageSectionName)
{
    if (!InOwner || InOwner->IsActorBeingDestroyed() || !InOwner->Implements<UEMeshActorInterface>() || !InAnimMontage || !InMontageSectionName.IsValid())
    {
        return ECombatFunctionLibrary::GFailedAnimationPlay;
    }

    const USkeletalMeshComponent* OwnerMeshComponent{IEMeshActorInterface::Execute_GetMeshComponent(InOwner)};
    if (!OwnerMeshComponent)
    {
        return ECombatFunctionLibrary::GFailedAnimationPlay;
    }

    if (UAnimInstance* AnimInstance{OwnerMeshComponent->GetAnimInstance()})
    {
        const float Result{AnimInstance->Montage_Play(InAnimMontage)};
        AnimInstance->Montage_JumpToSection(InMontageSectionName);
        return Result;
    }

    return ECombatFunctionLibrary::GFailedAnimationPlay;
}
