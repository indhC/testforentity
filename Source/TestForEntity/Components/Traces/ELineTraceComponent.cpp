﻿// © TestForEntity 2023.07.23:13:14 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ELineTraceComponent.h"

#include "Interfaces/EPeriodicTraceConditionInterface.h"
#include "Kismet/GameplayStatics.h"
#include "TestForEntity/Components/Combats/Interfaces/ECombatScreenConfigInterface.h"


namespace ELineTraceComponent
{
    constexpr int32 GDefaultIndexPlayerController{0};
}


// Sets default values for this component's properties
UELineTraceComponent::UELineTraceComponent(): bTraceInTimer{false},
                                              bOwnerTraceConditional{false}
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UELineTraceComponent::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void UELineTraceComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    const AActor* CurrentOwner{GetOwner()};
    if (!bTraceInTimer || !bOwnerTraceConditional || !IEPeriodicTraceConditionInterface::Execute_IsCurrentlyTraceable(ComponentOwner))
    {
        return;
    }

    const APlayerController* PlayerController{UGameplayStatics::GetPlayerController(CurrentOwner, ELineTraceComponent::GDefaultIndexPlayerController)};
    if (!PlayerController)
    {
        return;
    }

    const float TraceDistance{IECombatScreenConfigInterface::Execute_GetTraceDistance(CurrentOwner)};
    const FVector2D& CrosshairScreenLocation{IECombatScreenConfigInterface::Execute_GetCrosshairScreenLocation(CurrentOwner)};
    if (FVector WorldPosition, WorldRotation; UGameplayStatics::DeprojectScreenToWorld(PlayerController, CrosshairScreenLocation, WorldPosition, WorldRotation))
    {
        FHitResult HitResult;
        Trace(WorldPosition, WorldRotation, TraceDistance, HitResult);
        OnHitOccurredDelegate.Broadcast(HitResult);
    }
}

bool UELineTraceComponent::Trace_Implementation(const FVector& InStartLocation, const FVector& InDirection, const float InTraceDistance, FHitResult& OutHitResult)
{
    FCollisionQueryParams QueryParameters;
    QueryParameters.AddIgnoredActor(GetOwner());

    const FVector& EndLocation{InStartLocation + InDirection * InTraceDistance};
    return GetWorld()->LineTraceSingleByChannel(OutHitResult, InStartLocation, EndLocation, ECC_Visibility, QueryParameters);
}


//
// Getters And Setters
//

void UELineTraceComponent::SetComponentOwner(UObject* InComponentOwner)
{
    ComponentOwner = InComponentOwner;
    bOwnerTraceConditional = ComponentOwner && ComponentOwner->Implements<UEPeriodicTraceConditionInterface>() && GetOwner()->Implements<UECombatScreenConfigInterface>();
}
