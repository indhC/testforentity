﻿// © TestForEntity 2023.07.24:01:11 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "ERegistriesFunctionLibrary.generated.h"


struct FGameplayTag;
struct FEAttributeDescriptor;

/**
 * 
 */
UCLASS()
class TESTFORENTITY_API UERegistriesFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    static bool GetAttributeDescriptor(UGameInstance* InGameInstance, const class UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeNameTag,
                                       FEAttributeDescriptor& OutDescriptor);

    static class UEInputActionHandler* GetInputActionHandler(UGameInstance* InGameInstance, ACharacter* InOwner, const FGameplayTag& InInputActionHandlerNameTag);

};
