﻿// © TestForEntity 2023.07.21:18:59 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "TestForEntity/Components/Traces/Interfaces/EPeriodicTraceConditionInterface.h"
#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"

#include "EInteractionInputActionHandler.generated.h"


/**
 * 
 */
UCLASS()
class TESTFORENTITY_API UEInteractionInputActionHandler : public UEInputActionHandler, public IEPeriodicTraceConditionInterface
{
    GENERATED_BODY()


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interaction Input Action Handler", meta = (AllowPrivateAccess = "true"))
    int32 OverlappedPickableItemsCount;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interaction Input Action Handler", meta = (AllowPrivateAccess = "true"))
    bool bPickableItemsCheckingPaused;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interaction Input Action Handler", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<AActor> TraceFoundItem;


    UFUNCTION(Category = "Interaction Input Action Handler")
    void OnCapsuleComponentBeginOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, const int32 InOtherBodyIndex,
                                        const bool bInFromSweep, const FHitResult& InSweepResult);

    UFUNCTION(Category = "Interaction Input Action Handler")
    void OnCapsuleComponentEndOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, const int32 InOtherBodyIndex);

    UFUNCTION(Category = "Interaction Input Action Handler")
    void OnHitOccurredEvent(const FHitResult& InHitResult);

public:

    UEInteractionInputActionHandler();


    virtual void Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent) override;

    virtual void Handle_Implementation(const FInputActionValue& InValue) override;


    /////////////////////////////////////////////////////////////////
    /// IEPeriodicTraceConditionInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual bool IsCurrentlyTraceable_Implementation() override;

};
