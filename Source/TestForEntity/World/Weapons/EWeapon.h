﻿// © TestForEntity 2023.07.03:22:22 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "TestForEntity/Components/Combats/Interfaces/EReloadableItemInterface.h"
#include "TestForEntity/World/EPickableItem.h"
#include "EWeapon.generated.h"


USTRUCT(BlueprintType)
struct FEWeaponConfigTableRow : public FEPickableItemConfigTableRow
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TArray<TSubclassOf<AEAmmo>> SupportedAmmoTypes;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    FGameplayTag DefaultAmmoNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TObjectPtr<UDataTable> AmmoConfigDataTable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    FName ClipBoneName{NAME_None};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    FName WeaponStockSocketName{NAME_None};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    bool bAutomatic{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    float DelayBetweenShots{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    float BulletHittingRange{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TObjectPtr<USoundAttenuation> SoundAttenuation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TObjectPtr<USoundBase> FireEmptyAmmoSound;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TObjectPtr<class UNiagaraSystem> ImpactParticles;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TObjectPtr<USoundBase> ImpactBodySound;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Configuration")
    TObjectPtr<USoundBase> ImpactSurfaceSound;
};


UCLASS(Blueprintable, BlueprintType)
class TESTFORENTITY_API AEWeapon : public AEPickableItem, public IEReloadableItemInterface
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UELineTraceComponent> TraceComponent;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<TSubclassOf<AEAmmo>> SupportedAmmoTypes;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration|Mesh", meta = (AllowPrivateAccess = "true"))
    FName ClipBoneName;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat|Configuration|Mesh", meta = (AllowPrivateAccess = "true"))
    FName WeaponStockSocketName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration", meta = (AllowPrivateAccess = "true"))
    bool bAutomatic;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration", meta = (AllowPrivateAccess = "true"))
    float DelayBetweenShots;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration", meta = (AllowPrivateAccess = "true"))
    float BulletHittingRange;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USoundAttenuation> SoundAttenuation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration|Firing", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USoundBase> FireEmptyAmmoSound;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration|Impact", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UNiagaraSystem> ImpactParticles;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration|Impact", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USoundBase> ImpactBodySound;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon|Configuration|Impact", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USoundBase> ImpactSurfaceSound;


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<AEAmmo> CurrentAmmo;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
    int32 ShotsCount;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
    bool bMovingClip;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
    void SpawnImpactParticles(const FHitResult& InHitResult, const AActor* InOtherActor);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
    void PlayImpactSound(const AActor* InOtherActor, const FVector& InImpactPoint);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
    void ApplyWeaponEffects(const AActor* InOtherActor);

public:

    // Sets default values for this actor's properties
    AEWeapon();


    // Called every frame
    virtual void Tick(const float DeltaTime) override;


    virtual void Initialize(const FEPickableItemConfigTableRow* InRow) override;

    virtual void Drop_Implementation() override;

    UFUNCTION(BlueprintCallable, Category = "Weapon")
    void Fire(const FVector& InStartLocation, const FVector& InDirection);

    UFUNCTION(BlueprintCallable, Category = "Weapon")
    void OnShotOverNotificationHandler();

    virtual bool IsEmpty_Implementation() const override;


    /////////////////////////////////////////////////////////////////
    /// IEReloadableItemInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual bool MayBeReloaded_Implementation() const override;

    virtual bool Reload_Implementation(AEAmmo* InNewAmmo) override;

    virtual void GrabClip_Implementation() override;

    virtual void DropClip_Implementation() override;

    virtual void ReplaceClip_Implementation() override;


    //
    // Getters And Setters
    //

    FORCEINLINE const TArray<TSubclassOf<AEAmmo>>& GetSupportedAmmoTypes() const
    {
        return SupportedAmmoTypes;
    }

    FORCEINLINE const FName& GetClipBoneName() const
    {
        return ClipBoneName;
    }

    FORCEINLINE AEAmmo* GetCurrentAmmo() const
    {
        return CurrentAmmo;
    }

    FORCEINLINE int32 GetAmmoCount() const;

    FORCEINLINE bool IsAutomatic() const
    {
        return bAutomatic;
    }

    FORCEINLINE float GetDelayBetweenShots() const
    {
        return DelayBetweenShots;
    }

    FORCEINLINE int32 GetShotsCount() const
    {
        return ShotsCount;
    }

    FORCEINLINE bool IsClipMoving() const
    {
        return bMovingClip;
    }

    FORCEINLINE const USkeletalMeshSocket* GetStockSocket() const;

    FTransform GetStockSocketTransform() const;

};
