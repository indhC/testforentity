﻿// © TestForEntity 2023.07.03:23:02 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "TestForEntity/TestForEntity.h"
#include "TestForEntity/Components/Attributes/Interfaces/EAttributesActorInterface.h"
#include "TestForEntity/Interfaces/EInteractionInterface.h"
#include "TestForEntity/Interfaces/EMeshActorInterface.h"
#include "EPickableItem.generated.h"


USTRUCT(BlueprintType)
struct FEItemDroppingConfig
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Dropping Configuration")
    float DroppingDuration{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Dropping Configuration")
    float DroppingImpulse{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Dropping Configuration")
    float DroppingDefaultDirectionAngle{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Dropping Configuration")
    float DroppingRandomRotationAngleMin{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Dropping Configuration")
    float DroppingRandomRotationAngleMax{0.0f};
    
};


UENUM(BlueprintType)
enum class EEItemState: uint8
{
    EIS_PickingUp UMETA(DisplayName = "State.PickingUp"),
    EIS_EquipInterpolation UMETA(DisplayName = "State.EquipInterpolation", Hidden),
    EIS_Equipped UMETA(DisplayName = "State.Equipped"),
    EIS_PickedUp UMETA(DisplayName = "State.PickedUp"),
    EIS_Falling UMETA(DisplayName = "State.Falling", Hidden),
};


USTRUCT(BlueprintType)
struct FEPickableItemConfigTableRow : public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    TSubclassOf<class AEPickableItem> ActualType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    FText NameText;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    FText ActionText;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    TObjectPtr<UTexture> IconTexture;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    TObjectPtr<USkeletalMesh> SkeletalMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    TSubclassOf<UUserWidget> PickUpWidgetType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    EEItemRarity Rarity{EEItemRarity::EIR_Common};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    EEItemState SpawningState{EEItemState::EIS_PickingUp};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    int32 MaximumCount{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    int32 MaximumCountPerSlot{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    bool bReplaceDroppingConfigToDefaultAfterFirstDropping{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    FEItemDroppingConfig DroppingConfig;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration",
        meta = (EditCondition = "bReplaceDroppingConfigToDefaultAfterFirstDropping", EditConditionHides))
    FEItemDroppingConfig DefaultDroppingConfig;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    float InterpolationSpeed;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    TObjectPtr<UCurveFloat> PickUpZInterpolationCurve;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    int32 DefaultCount{0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    bool bDestroyDropped{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    bool bDestroyDroppedWhenEmpty{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    float DroppedLifetime{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    float DroppedEmptyLifetime{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    bool bSpawnHidden{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item Configuration")
    bool bSimulatePhysicsWhenInPickingUpState{false};
};


USTRUCT(BlueprintType)
struct FEItemStateProperties
{
    GENERATED_BODY()


    // Skeletal Mesh State Properties

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item State Properties")
    bool bMeshVisibility{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item State Properties")
    bool bMeshSimulatePhysics{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item State Properties")
    bool bMeshEnableGravity{false};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item State Properties")
    FName MeshCollisionProfileName{NAME_None};


    // Area Sphere State Properties

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item State Properties")
    TEnumAsByte<ECollisionEnabled::Type> AreaSphereCollisionEnabled{ECollisionEnabled::NoCollision};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item State Properties")
    TEnumAsByte<ECollisionResponse> AreaSphereCollisionResponse{ECR_Ignore};
};


UCLASS(Blueprintable, BlueprintType)
class TESTFORENTITY_API AEPickableItem : public AActor, public IEAttributesActorInterface, public IEMeshActorInterface, public IEInteractionInterface
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USkeletalMeshComponent> SkeletalMeshComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class USphereComponent> AreaSphereComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UWidgetComponent> PickUpWidgetComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Components|Attributes", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UEAttributesComponent> AttributesComponent;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    FText NameText;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    FText ActionText;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UTexture> IconTexture;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<UUserWidget> PickUpWidgetType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    EEItemRarity Rarity;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    EEItemState SpawningState;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 MaximumCount;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 MaximumCountPerSlot;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    bool bReplaceDroppingConfigToDefaultAfterFirstDropping;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    FEItemDroppingConfig DroppingConfig;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    FEItemDroppingConfig DefaultDroppingConfig;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    EEItemState State;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    float InterpolationSpeed;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UCurveFloat> PickUpZInterpolationCurve;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 Count;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    bool bDestroyDropped;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    bool bDestroyDroppedWhenEmpty;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    float DroppedLifetime;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    float DroppedEmptyLifetime;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickable Item|Configuration", meta = (AllowPrivateAccess = "true"))
    bool bSimulatePhysicsWhenInPickingUpState;


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    bool bInteracting;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    bool bFalling;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    bool bInterpolating;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    double InterpolationInitialZValue;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    double InterpolationYawDelta;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<AActor> ParentActor;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    bool bHasBeenDropped;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickable Item", meta = (AllowPrivateAccess = "true"))
    bool bDroppingConfigNotReplaced;


    FTimerHandle InterpolationTimerHandle;


    UFUNCTION(Category = "Pickable Item")
    void EnableLifeSpanTimer();

    UFUNCTION(Category = "Pickable Item")
    void DisableLifeSpanTimer();

    UFUNCTION(Category = "Pickable Item")
    void SetMeshUprightRotation() const;

    void InterpolateToParent(float InDeltaTime);

    UFUNCTION(Category = "Pickable Item")
    void InitializePickUpWidget();

    UFUNCTION(Category = "Pickable Item")
    void UpdateDroppingConfig();

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    UFUNCTION(BlueprintCallable, Category = "Pickable Item")
    void UpdateItemStateAndProperties(const EEItemState InNewState);

    /**
     * Specifies the actual Item state, which should be set after the Item is equipped. For example, a scenario is possible where an Item should be invisible when it is equipped.
     * In this case, the EEItemState::EIS_Equipped state might not be appropriate, as the Item will be visible after transitioning to this state
     * @return EEItemState value which specifies the actual Item state, which should be set after the Item is equipped
     */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickable Item")
    EEItemState GetEquippedState() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickable Item")
    void UpdateInitialLocation();

public:

    // Sets default values for this actor's properties
    AEPickableItem();


    // Called every frame
    virtual void Tick(const float InDeltaTime) override;


    //
    // Delegates
    //

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemInitialized, const AEPickableItem*, InItem);
    UPROPERTY(BlueprintAssignable, Category = "Pickable Item")
    FOnItemInitialized OnItemInitializedDelegate;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnItemCountChanged, const AEPickableItem*, InItem, const int32, InPreviousCount, const int32, InNewCount);
    UPROPERTY(BlueprintAssignable, Category = "Pickable Item")
    FOnItemCountChanged OnItemCountChangedDelegate;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemPickedUp, AEPickableItem*, InItem);
    UPROPERTY(BlueprintAssignable, Category = "Pickable Item")
    FOnItemPickedUp OnItemPickedUpDelegate;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemDropped, AEPickableItem*, InItem);
    UPROPERTY(BlueprintAssignable, Category = "Pickable Item")
    FOnItemDropped OnItemDroppedDelegate;


    virtual void Initialize(const FEPickableItemConfigTableRow* InRow);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickable Item")
    void PickUp(AActor* InInstigator);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickable Item")
    void Use(const int32 InCount);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
    void Equip(AActor* InParent);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickable Item")
    void Drop();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
    bool IsEmpty() const;

    UFUNCTION(BlueprintCallable, Category = "Pickable Item")
    void SetCount(const int32 InNewCount);


    /////////////////////////////////////////////////////////////////
    /// IEAttributesActorInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual UEAttributesComponent* GetAttributesComponent_Implementation() const override;

    virtual void UpdateParentAttributesComponent_Implementation(const AActor* InParent) override;


    /////////////////////////////////////////////////////////////////
    /// IEMeshActorInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual USkeletalMeshComponent* GetMeshComponent_Implementation() const override;


    /////////////////////////////////////////////////////////////////
    /// IEInteractionInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual void ShowPickUpWidget_Implementation(AActor* InInstigator) override;

    virtual void HidePickUpWidget_Implementation(AActor* InInstigator) override;

    virtual EEInteractionType GetInteractionType_Implementation() override;

    virtual void Interact_Implementation(AActor* InInstigator) override;


    //
    // Getters And Setters
    //

    FORCEINLINE const FText& GetNameText() const
    {
        return NameText;
    }

    FORCEINLINE const FText& GetActionText() const
    {
        return ActionText;
    }

    FORCEINLINE UTexture* GetIconTexture() const
    {
        return IconTexture;
    }

    FORCEINLINE EEItemRarity GetRarity() const
    {
        return Rarity;
    }

    FORCEINLINE int32 GetMaximumCount() const
    {
        return MaximumCount;
    }

    FORCEINLINE int32 GetMaximumCountPerSlot() const
    {
        return MaximumCountPerSlot;
    }

    FORCEINLINE int32 GetCount() const
    {
        return Count;
    }

    FORCEINLINE AActor* GetParent() const
    {
        return ParentActor.Get();
    }

};
