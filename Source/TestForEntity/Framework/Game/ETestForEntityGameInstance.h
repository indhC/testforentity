﻿// © TestForEntity 2023.07.23:12:23 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TestForEntity/Components/Attributes/Interfaces/EAttributesStorageInterface.h"
#include "TestForEntity/Framework/Characters/Inputs/Interfaces/EInputActionHandlersStorageInterface.h"
#include "ETestForEntityGameInstance.generated.h"


/**
 * 
 */
UCLASS()
class TESTFORENTITY_API UETestForEntityGameInstance : public UGameInstance, public IEAttributesStorageInterface, public IEInputActionHandlersStorageInterface
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Storage|Concifugration|Attributes", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> AttributesDataTable;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Storage|Concifugration|Attributes", meta = (AllowPrivateAccess = "true"))
    TMap<FGameplayTag, FEAttributeDescriptor> AttributeDescriptors;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Storage|Concifugration|Input Action Handlers", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> InputActionHandlersDataTable;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Storage|Concifugration|Input Action Handlers", meta = (AllowPrivateAccess = "true"))
    TMap<FGameplayTag, TObjectPtr<UEInputActionHandler>> InputActionHandlers;


    UFUNCTION(Category = "Game Instance")
    FEAttributeDescriptor TryCacheAttributeDescriptor(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeNameTag);


    UFUNCTION(Category = "Game Instance")
    UEInputActionHandler* TryCacheInputActionHandler(ACharacter* InOwner, const FGameplayTag& InInputActionHandlerNameTag);

public:

    /////////////////////////////////////////////////////////////////
    /// IEAttributesStorageInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual FEAttributeDescriptor GetCachedAttributeDescriptor_Implementation(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeNameTag) override;


    /////////////////////////////////////////////////////////////////
    /// IEInputActionHandlersStorageInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual UEInputActionHandler* GetCachedInputActionHandler_Implementation(ACharacter* InOwner, const FGameplayTag& InInputActionHandlerNameTag) override;

};
