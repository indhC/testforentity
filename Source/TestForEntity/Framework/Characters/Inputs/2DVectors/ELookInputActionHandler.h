﻿// © TestForEntity 2023.07.03:20:20 Dmitry V. (ind.hC). All rights reserved.mework/Characters/Inputs/EInputActionHandler.h"

#pragma once

#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"

#include "ELookInputActionHandler.generated.h"


/**
 * 
 */
UCLASS()
class TESTFORENTITY_API UELookInputActionHandler : public UEInputActionHandler
{
    GENERATED_BODY()

public:

    virtual void Handle_Implementation(const FInputActionValue& InValue) override;

};
