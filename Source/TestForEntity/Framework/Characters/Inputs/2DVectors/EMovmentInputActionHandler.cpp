﻿// © TestForEntity 2023.07.21:04:03 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EMovmentInputActionHandler.h"

#include "GameFramework/Character.h"


void UEMovementInputActionHandler::Handle_Implementation(const FInputActionValue& InValue)
{
    ACharacter* Character{GetCurrentCharacter()};
    const FRotator YawRotation{0.0f, Character->GetController()->GetControlRotation().Yaw, 0.0f};
    const FRotationMatrix YawRotationMatrix{FRotationMatrix{YawRotation}};
    const FVector2D MovementVector{InValue.Get<FVector2D>()};
    Character->AddMovementInput(YawRotationMatrix.GetUnitAxis(EAxis::X), MovementVector.Y);
    Character->AddMovementInput(YawRotationMatrix.GetUnitAxis(EAxis::Y), MovementVector.X);
}
