﻿// © TestForEntity 2023.07.07:02:34 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EPlayerCharacterAnimInstance.h"

#include "EPlayerCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"


namespace EPlayerCharacterAnimInstance
{
    constexpr double GAngleRight{90};
}


/////////////////////////////////////////////////////////////////
/// FEPlayerCharacterAnimInstanceProxy Definitions
/////////////////////////////////////////////////////////////////

FEPlayerCharacterAnimInstanceProxy::FEPlayerCharacterAnimInstanceProxy(UAnimInstance* InAnimInstance): Super(InAnimInstance)
{
}

void FEPlayerCharacterAnimInstanceProxy::UpdateSpeed(const FVector& Velocity)
{
    FVector CurrentVelocity{Velocity};
    CurrentVelocity.Z = 0.0; // Movement in the vertical plane should not affect the lateral Speed of the Character!
    Speed = CurrentVelocity.Size();
}

void FEPlayerCharacterAnimInstanceProxy::UpdateMovementOffsetYaw(const FVector& InVelocity, const FRotator& InBaseAimRotation)
{
    const FRotator& MovementRotation{UKismetMathLibrary::MakeRotFromX(InVelocity)};
    MovementOffsetYaw = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, InBaseAimRotation).Yaw;
}

void FEPlayerCharacterAnimInstanceProxy::ResetGroundRelatedStates()
{
    bShouldTurnRight = false;
    bShouldNotTurnRight = true;

    bShouldTurnLeft = false;
    bShouldNotTurnLeft = true;

    CharacterForwardRotationYaw = 0.0;
    CharacterForwardRotationNegateYaw = 0.0;

    PreviousPlayerRotationYaw = PlayerCharacter->GetActorRotation().Yaw;
    PreviousRotationCurveValue = 0.0f;
}

void FEPlayerCharacterAnimInstanceProxy::UpdateCharacterForwardRotation()
{
    const double CurrentRotationYaw{PlayerCharacter->GetActorRotation().Yaw};
    const double PlayerRotationDeltaYaw{CurrentRotationYaw - PreviousPlayerRotationYaw};
    PreviousPlayerRotationYaw = CurrentRotationYaw;
    CharacterForwardRotationYaw = FRotator::NormalizeAxis(CharacterForwardRotationYaw - PlayerRotationDeltaYaw);
    CharacterForwardRotationNegateYaw = -CharacterForwardRotationYaw;
}

void FEPlayerCharacterAnimInstanceProxy::UpdateCharacterForwardRotationPerTurningCurve(const UEPlayerCharacterAnimInstance* InAnimInstance)
{
    const float CurrentRotationValue{InAnimInstance->GetCurveValue(InAnimInstance->RotationCurveName)};
    const float CurveRotationValueDelta{CurrentRotationValue - PreviousRotationCurveValue};
    PreviousRotationCurveValue = CurrentRotationValue;
    (CharacterForwardRotationYaw > 0.0) ? (CharacterForwardRotationYaw -= CurveRotationValueDelta) : (CharacterForwardRotationYaw += CurveRotationValueDelta);
    CharacterForwardRotationYaw = FMath::Clamp(CharacterForwardRotationYaw, -EPlayerCharacterAnimInstance::GAngleRight, EPlayerCharacterAnimInstance::GAngleRight);
    CharacterForwardRotationNegateYaw = -CharacterForwardRotationYaw;
}

void FEPlayerCharacterAnimInstanceProxy::UpdateTurnRightLeftFlags()
{
    bShouldTurnRight = CharacterForwardRotationYaw <= -EPlayerCharacterAnimInstance::GAngleRight;
    bShouldNotTurnRight = CharacterForwardRotationYaw > 0.0;

    bShouldTurnLeft = CharacterForwardRotationYaw >= EPlayerCharacterAnimInstance::GAngleRight;
    bShouldNotTurnLeft = CharacterForwardRotationYaw < 0.0;
}

void FEPlayerCharacterAnimInstanceProxy::PreUpdate(UAnimInstance* InAnimInstance, const float InDeltaSeconds)
{
    Super::PreUpdate(InAnimInstance, InDeltaSeconds);

    const UEPlayerCharacterAnimInstance* AnimInstance{Cast<UEPlayerCharacterAnimInstance>(InAnimInstance)};
    if (!AnimInstance)
    {
        return;
    }

    if ((!PlayerCharacter) ? (PlayerCharacter = Cast<AEPlayerCharacter>(AnimInstance->TryGetPawnOwner())) : (false); !PlayerCharacter)
    {
        return;
    }

    bAccelerating = PlayerCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.0f;

    const FVector& Velocity{PlayerCharacter->GetVelocity()};
    UpdateSpeed(Velocity);

    bShouldStartJogging = bAccelerating && (Speed > 0.0);
    bShouldStopJogging = Speed <= 0.0;

    bInAir = PlayerCharacter->GetMovementComponent()->IsFalling();

    const FRotator& BaseAimRotation{PlayerCharacter->GetBaseAimRotation()};
    UpdateMovementOffsetYaw(Velocity, BaseAimRotation);

    bAiming = PlayerCharacter->IsAiming();
    bReloading = PlayerCharacter->IsReloading();
    bLeftHandTransformTrackingAlpha = static_cast<double>(PlayerCharacter->IsLeftHandTransformTrackingEnabled());
    StockSocketTransform = PlayerCharacter->GetStockSocketTransform();

    CharacterForwardRotationPitch = BaseAimRotation.Pitch;

    if (bInAir || (Speed > 0.0))
    {
        ResetGroundRelatedStates();
        return;
    }

    UpdateCharacterForwardRotation();
    UpdateTurnRightLeftFlags();

    if (!AnimInstance->RotationMetadataCurveName.IsValid() || !AnimInstance->RotationCurveName.IsValid())
    {
        return;
    }
    if (const float TurningMetadataValue{AnimInstance->GetCurveValue(AnimInstance->RotationMetadataCurveName)}; TurningMetadataValue >= 1.0f)
    {
        UpdateCharacterForwardRotationPerTurningCurve(AnimInstance);
        UpdateTurnRightLeftFlags();
    }
}


/////////////////////////////////////////////////////////////////
/// UEPlayerCharacterAnimInstance Definitions
/////////////////////////////////////////////////////////////////

UEPlayerCharacterAnimInstance::UEPlayerCharacterAnimInstance(): RotationMetadataCurveName{NAME_None},
                                                                RotationCurveName{NAME_None}
{
}

void UEPlayerCharacterAnimInstance::NativeInitializeAnimation()
{
    Super::NativeInitializeAnimation();

    ensureAlways(RotationMetadataCurveName.IsValid());
    ensureAlways(RotationCurveName.IsValid());
}
