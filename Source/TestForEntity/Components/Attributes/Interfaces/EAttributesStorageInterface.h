﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TestForEntity/Components/Attributes/EAttributesCommonTypes.h"
#include "UObject/Interface.h"
#include "EAttributesStorageInterface.generated.h"


// This class does not need to be modified.
UINTERFACE()
class UEAttributesStorageInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IEAttributesStorageInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Attributes Storage")
    FEAttributeDescriptor GetCachedAttributeDescriptor(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeTag);

};
