﻿// © TestForEntity 2023.07.21:03:24 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "Engine/DataTable.h"

#include "EInputActionhandler.generated.h"


class UEInputActionHandler;

USTRUCT(BlueprintType, Blueprintable)
struct FEInputActionHandlerConfigRow: public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Action Handler Config Row")
    TSubclassOf<UEInputActionHandler> HandlerType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Action Handler Config Row")
    ETriggerEvent HandleTriggerEvent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Action Handler Config Row")
    TObjectPtr<UInputAction> InputAction;

};


UCLASS(Abstract, Blueprintable, BlueprintType)
class TESTFORENTITY_API UEInputActionHandler : public UObject
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Action Handler|Configuration", meta = (AllowPrivateAccess = "true"))
    ETriggerEvent HandleTriggerEvent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Action Handler|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UInputAction> InputAction;


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Input Action Handler|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<ACharacter> CurrentCharacter;

protected:

    //
    // Getters And Setters
    //

    ACharacter* GetCurrentCharacter() const
    {
        return CurrentCharacter;
    }

public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Handler")
    void Initialize(const FEInputActionHandlerConfigRow& InConfigRow, ACharacter* InOwner);


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Handler")
    void Assign(UEnhancedInputComponent* InEnhancedInputComponent);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Handler")
    bool MayBeExecuted() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Input Action Handler")
    void Handle(const FInputActionValue& InValue);

};
