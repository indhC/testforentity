﻿// © TestForEntity 2023.07.23:01:12 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"

#include "EReloadInputActionHandler.generated.h"


/**
 * 
 */
UCLASS()
class TESTFORENTITY_API UEReloadInputActionHandler : public UEInputActionHandler
{
    GENERATED_BODY()


    UFUNCTION(Category = "Reload Input Action Handler")
    void Reload() const;


    UFUNCTION(Category = "Reload Input Action Handler")
    void OnWeaponAmmoRanOutEvent(const class AEWeapon* InCurrentWeapon);

public:

    virtual void Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent) override;

    virtual bool MayBeExecuted_Implementation() const override;

    virtual void Handle_Implementation(const FInputActionValue& InValue) override;

};
