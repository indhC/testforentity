﻿// © TestForEntity 2023.07.03:23:02 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EPickableItem.h"

#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "TestForEntity/Components/Attributes/EAttributesComponent.h"
#include "TestForEntity/Components/Combats/Interfaces/ECombatScreenConfigInterface.h"
#include "TestForEntity/Interfaces/EPickableItemConsumerInterface.h"
#include "TestForEntity/Utilities/ECombatFunctionLibrary.h"
#include "UI/EPickUpWidget.h"


namespace EPickableItem
{
    constexpr float GTimeResettingLifeSpanTimer{0.0f};

    constexpr int32 GDefaultCount{0};
    constexpr int32 GDefaultMaximumCount{0};

    constexpr float GDefaultDroppingDuration{0.7f};
    constexpr float GDefaultDroppingImpulse{20'000.0f};
    constexpr float GDefaultDroppingDirectionAngle{-20.0f};
    constexpr float GDefaultDroppingRandomRotationAngleMin{-10.0f};
    constexpr float GDefaultDroppingRandomRotationAngleMax{30.0f};

    const FVector2D GDefaultPickUpWidgetDrawSize{400.0, 110.0};

    constexpr float GDefaultInterpolationSpeed{1.0f};


    const FName GCollisionProfileNoCollision{TEXT("NoCollision")};
    const FName GCollisionProfilePickableItem{TEXT("PickableItem")};


    const TMap<EEItemState, FEItemStateProperties> GItemStatePropertiesMap
    {
        {
            EEItemState::EIS_PickingUp, FEItemStateProperties
            {
                true,
                false,
                false,
                GCollisionProfilePickableItem,

                ECollisionEnabled::QueryOnly,
                ECR_Overlap
            }
        },

        {
            EEItemState::EIS_EquipInterpolation, FEItemStateProperties
            {
                true,
                false,
                false,
                GCollisionProfileNoCollision,

                ECollisionEnabled::NoCollision,
                ECR_Ignore
            }
        },

        {
            EEItemState::EIS_Equipped, FEItemStateProperties
            {
                true,
                false,
                false,
                GCollisionProfileNoCollision,

                ECollisionEnabled::NoCollision,
                ECR_Ignore
            }
        },

        {
            EEItemState::EIS_PickedUp, FEItemStateProperties
            {
                false,
                false,
                false,
                GCollisionProfileNoCollision,

                ECollisionEnabled::NoCollision,
                ECR_Ignore
            }
        },

        {
            EEItemState::EIS_Falling, FEItemStateProperties
            {
                true,
                true,
                true,
                GCollisionProfilePickableItem,

                ECollisionEnabled::NoCollision,
                ECR_Ignore
            }
        }
    };
}


// Sets default values
AEPickableItem::AEPickableItem(): Rarity{EEItemRarity::EIR_Common},
                                  MaximumCount{EPickableItem::GDefaultMaximumCount},
                                  bReplaceDroppingConfigToDefaultAfterFirstDropping{false},
                                  DroppingConfig
                                  {
                                      EPickableItem::GDefaultDroppingDuration, EPickableItem::GDefaultDroppingImpulse, EPickableItem::GDefaultDroppingDirectionAngle,
                                      EPickableItem::GDefaultDroppingDirectionAngle, EPickableItem::GDefaultDroppingRandomRotationAngleMax
                                  },
                                  DefaultDroppingConfig
                                  {
                                      EPickableItem::GDefaultDroppingDuration, EPickableItem::GDefaultDroppingImpulse, EPickableItem::GDefaultDroppingDirectionAngle,
                                      EPickableItem::GDefaultDroppingDirectionAngle, EPickableItem::GDefaultDroppingRandomRotationAngleMax
                                  },
                                  State{EEItemState::EIS_PickingUp},
                                  InterpolationSpeed{EPickableItem::GDefaultInterpolationSpeed},
                                  Count{EPickableItem::GDefaultCount},
                                  bDestroyDropped{false},
                                  bDestroyDroppedWhenEmpty{false},
                                  DroppedLifetime{EPickableItem::GTimeResettingLifeSpanTimer},
                                  DroppedEmptyLifetime{EPickableItem::GTimeResettingLifeSpanTimer},
                                  bInteracting{false},
                                  bFalling{false},
                                  bInterpolating{false},
                                  InterpolationInitialZValue{0.0},
                                  InterpolationYawDelta{0.0},
                                  bHasBeenDropped{false},
                                  bDroppingConfigNotReplaced{true}
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
    SkeletalMeshComponent->SetCollisionProfileName(EPickableItem::GCollisionProfilePickableItem);
    SkeletalMeshComponent->SetAnimationMode(EAnimationMode::AnimationBlueprint);
    SetRootComponent(SkeletalMeshComponent);

    AreaSphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("AreaSphereComponent"));
    AreaSphereComponent->SetupAttachment(SkeletalMeshComponent);

    PickUpWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickUpWidgetComponent"));
    PickUpWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
    PickUpWidgetComponent->SetDrawSize(EPickableItem::GDefaultPickUpWidgetDrawSize);
    PickUpWidgetComponent->SetupAttachment(SkeletalMeshComponent);

    AttributesComponent = CreateDefaultSubobject<UEAttributesComponent>(TEXT("AttributesComponent"));
}

void AEPickableItem::EnableLifeSpanTimer()
{
    if (const bool bUseDroppedEmptyLifetime{bDestroyDroppedWhenEmpty && (DroppedEmptyLifetime > 0.0f) && (Count <= 0)};
        bUseDroppedEmptyLifetime || (bDestroyDropped && (DroppedLifetime > 0.0f)))
    {
        DisableLifeSpanTimer();
        SetLifeSpan((bUseDroppedEmptyLifetime) ? (DroppedEmptyLifetime) : (DroppedLifetime));
    }
}

void AEPickableItem::DisableLifeSpanTimer()
{
    SetLifeSpan(EPickableItem::GTimeResettingLifeSpanTimer);
}

void AEPickableItem::SetMeshUprightRotation() const
{
    const FRotator& MeshRotation{0.0f, SkeletalMeshComponent->GetComponentRotation().Yaw, 0.0f};
    SkeletalMeshComponent->SetWorldRotation(MeshRotation, false, nullptr, ETeleportType::TeleportPhysics);
}

void AEPickableItem::InterpolateToParent(const float InDeltaTime)
{
    const FVector CurrentItemLocation{GetActorLocation()};
    const FVector& TargetInterpolationLocation{IECombatScreenConfigInterface::Execute_GetTargetPickUpLocation(ParentActor)};
    FVector CurrentLocation{FMath::VInterpTo(CurrentItemLocation, TargetInterpolationLocation, InDeltaTime, InterpolationSpeed)};
    CurrentLocation.Z = InterpolationInitialZValue;
    const float TimerElapsed{GetWorldTimerManager().GetTimerElapsed(InterpolationTimerHandle)};
    if (const double ZInterpolationDistance{TargetInterpolationLocation.Z - InterpolationInitialZValue}; !FMath::IsNearlyZero(ZInterpolationDistance))
    {
        CurrentLocation.Z += FMath::Abs(ZInterpolationDistance) * PickUpZInterpolationCurve->GetFloatValue(TimerElapsed);
    }

    const FRotator& CameraRotator{IECombatScreenConfigInterface::Execute_GetCameraRotation(ParentActor)};
    const FRotator CurrentRotation{0.0, (CameraRotator.Yaw + InterpolationYawDelta), 0.0};

    SetActorLocationAndRotation(CurrentLocation, CurrentRotation, true, nullptr, ETeleportType::TeleportPhysics);
}

void AEPickableItem::InitializePickUpWidget()
{
    if (PickUpWidgetType)
    {
        PickUpWidgetComponent->SetWidgetClass(PickUpWidgetType);
        Cast<UEPickUpWidget>(PickUpWidgetComponent->GetWidget())->SetOwningItem(this);
        OnItemInitializedDelegate.Broadcast(this);
    }
}

void AEPickableItem::UpdateDroppingConfig()
{
    if (bReplaceDroppingConfigToDefaultAfterFirstDropping && bHasBeenDropped && bDroppingConfigNotReplaced)
    {
        bDroppingConfigNotReplaced = false;
        DroppingConfig = DefaultDroppingConfig;
    }
    bHasBeenDropped = true;
}

// Called when the game starts or when spawned
void AEPickableItem::BeginPlay()
{
    Super::BeginPlay();

    InitializePickUpWidget();

    PickUpWidgetComponent->SetVisibility(false);
}

void AEPickableItem::UpdateItemStateAndProperties(const EEItemState InNewState)
{
    if (InNewState == State)
    {
        return;
    }
    State = (EEItemState::EIS_Equipped == InNewState) ? (GetEquippedState()) : (InNewState);

    Execute_HidePickUpWidget(this, nullptr);

    const FEItemStateProperties& Properties{EPickableItem::GItemStatePropertiesMap[State]};
    SkeletalMeshComponent->SetVisibility(Properties.bMeshVisibility);

    const bool bPickingUpStateSwitched{EEItemState::EIS_PickingUp == State};
    if (bPickingUpStateSwitched && bSimulatePhysicsWhenInPickingUpState)
    {
        SkeletalMeshComponent->SetSimulatePhysics(true);
        SkeletalMeshComponent->SetEnableGravity(true);
        SkeletalMeshComponent->SetCollisionProfileName(EPickableItem::GCollisionProfilePickableItem);
    }
    else
    {
        SkeletalMeshComponent->SetSimulatePhysics(Properties.bMeshSimulatePhysics);
        SkeletalMeshComponent->SetEnableGravity(Properties.bMeshEnableGravity);
        SkeletalMeshComponent->SetCollisionProfileName(Properties.MeshCollisionProfileName);
    }

    AreaSphereComponent->SetCollisionEnabled(Properties.AreaSphereCollisionEnabled);
    AreaSphereComponent->SetCollisionResponseToAllChannels(Properties.AreaSphereCollisionResponse);

    (bPickingUpStateSwitched) ? (EnableLifeSpanTimer()) : (DisableLifeSpanTimer());
}

EEItemState AEPickableItem::GetEquippedState_Implementation() const
{
    return EEItemState::EIS_Equipped;
}

void AEPickableItem::UpdateInitialLocation_Implementation()
{
    if (ParentActor)
    {
        SetActorLocation(ParentActor->GetActorLocation(), false, nullptr, ETeleportType::TeleportPhysics);
    }
}

// Called every frame
void AEPickableItem::Tick(const float InDeltaTime)
{
    Super::Tick(InDeltaTime);

    if (bFalling)
    {
        SetMeshUprightRotation();
    }

    if (bInterpolating)
    {
        InterpolateToParent(InDeltaTime);
    }
}

void AEPickableItem::Initialize(const FEPickableItemConfigTableRow* InRow)
{
    if (!InRow)
    {
        return;
    }

    NameText = InRow->NameText;
    ActionText = InRow->ActionText;
    IconTexture = InRow->IconTexture;

    SkeletalMeshComponent->SetSkeletalMeshAsset(InRow->SkeletalMesh);

    PickUpWidgetType = InRow->PickUpWidgetType;

    Rarity = InRow->Rarity;

    MaximumCount = InRow->MaximumCount;
    MaximumCountPerSlot = InRow->MaximumCountPerSlot;

    bReplaceDroppingConfigToDefaultAfterFirstDropping = InRow->bReplaceDroppingConfigToDefaultAfterFirstDropping;
    DroppingConfig = InRow->DroppingConfig;
    if(bReplaceDroppingConfigToDefaultAfterFirstDropping)
    {
        DefaultDroppingConfig = InRow->DefaultDroppingConfig;
    }

    PickUpZInterpolationCurve = InRow->PickUpZInterpolationCurve;

    Count = InRow->DefaultCount;

    bDestroyDropped = InRow->bDestroyDropped;
    bDestroyDroppedWhenEmpty = InRow->bDestroyDroppedWhenEmpty;
    DroppedLifetime = InRow->DroppedLifetime;
    DroppedEmptyLifetime = InRow->DroppedEmptyLifetime;
    bSimulatePhysicsWhenInPickingUpState = InRow->bSimulatePhysicsWhenInPickingUpState;

    InitializePickUpWidget();

    UpdateItemStateAndProperties(State);
    if (InRow->bSpawnHidden)
    {
        SkeletalMeshComponent->SetVisibility(false);
    }
}

void AEPickableItem::PickUp_Implementation(AActor* InInstigator)
{
    ParentActor = InInstigator;

    Execute_HidePickUpWidget(this, ParentActor);
    UpdateItemStateAndProperties(EEItemState::EIS_EquipInterpolation);

    if (!PickUpZInterpolationCurve || !ParentActor || !ParentActor->Implements<UECombatScreenConfigInterface>())
    {
        UpdateItemStateAndProperties(EEItemState::EIS_PickedUp);
        OnItemPickedUpDelegate.Broadcast(this);
        return;
    }

    InterpolationInitialZValue = GetActorLocation().Z;
    InterpolationYawDelta = GetActorRotation().Yaw - IECombatScreenConfigInterface::Execute_GetCameraRotation(ParentActor).Yaw;

    float CurveTime;
    PickUpZInterpolationCurve->GetTimeRange(CurveTime, CurveTime);

    FTimerDelegate TimerDelegate;
    TimerDelegate.BindLambda([this]()
    {
        bInterpolating = false;
        bInteracting = false;

        UpdateItemStateAndProperties(EEItemState::EIS_PickedUp);

        OnItemPickedUpDelegate.Broadcast(this);
    });
    GetWorldTimerManager().SetTimer(InterpolationTimerHandle, TimerDelegate, CurveTime, false);

    bInterpolating = true;
}

void AEPickableItem::Use_Implementation(const int32 InCount)
{
}

void AEPickableItem::Equip_Implementation(AActor* InParent)
{
    // TODO DOV: Add an Animation for replacing Weapons in the Weapon implementation!
    ParentActor = InParent;

    Execute_UpdateParentAttributesComponent(this, InParent);
    UpdateItemStateAndProperties(EEItemState::EIS_Equipped);
}

void AEPickableItem::Drop_Implementation()
{
    bFalling = true;

    UpdateDroppingConfig();
    UpdateItemStateAndProperties(EEItemState::EIS_Falling);
    SetMeshUprightRotation();
    UpdateInitialLocation();

    const FVector& MeshForward{SkeletalMeshComponent->GetForwardVector()};
    const FVector& MeshRight{SkeletalMeshComponent->GetRightVector()};
    FVector Impulse{MeshRight.RotateAngleAxis(DroppingConfig.DroppingDefaultDirectionAngle, MeshForward)};

    const float RandomAngle{FMath::FRandRange(DroppingConfig.DroppingRandomRotationAngleMin, DroppingConfig.DroppingRandomRotationAngleMax)};
    Impulse = DroppingConfig.DroppingImpulse * Impulse.RotateAngleAxis(RandomAngle, FVector::UpVector);
    SkeletalMeshComponent->AddImpulse(Impulse);

    FTimerHandle DroppingTimerHandle;
    FTimerDelegate TimerDelegate;
    TimerDelegate.BindLambda([this]()
    {
        bFalling = false;
        bInteracting = false;

        ParentActor = nullptr;

        Execute_UpdateParentAttributesComponent(this, nullptr);
        UpdateItemStateAndProperties(EEItemState::EIS_PickingUp);

        OnItemDroppedDelegate.Broadcast(this);
    });
    GetWorldTimerManager().SetTimer(DroppingTimerHandle, TimerDelegate, DroppingConfig.DroppingDuration, false);
}

bool AEPickableItem::IsEmpty_Implementation() const
{
    return Count <= 0;
}

void AEPickableItem::SetCount(const int32 InNewCount)
{
    const int32 PreviousCount{Count};
    Count = InNewCount;

    if (PreviousCount != Count)
    {
        OnItemCountChangedDelegate.Broadcast(this, PreviousCount, Count);
    }
}


/////////////////////////////////////////////////////////////////
/// IEAttributesActorInterface Implementation
/////////////////////////////////////////////////////////////////

UEAttributesComponent* AEPickableItem::GetAttributesComponent_Implementation() const
{
    return AttributesComponent;
}

void AEPickableItem::UpdateParentAttributesComponent_Implementation(const AActor* InParent)
{
    UECombatFunctionLibrary::UpdateParentAttributesComponent(InParent, AttributesComponent);
}


/////////////////////////////////////////////////////////////////
/// IEMeshActorInterface Implementation
/////////////////////////////////////////////////////////////////

USkeletalMeshComponent* AEPickableItem::GetMeshComponent_Implementation() const
{
    return SkeletalMeshComponent;
}


/////////////////////////////////////////////////////////////////
/// IEInteractionInterface Implementation
/////////////////////////////////////////////////////////////////

void AEPickableItem::ShowPickUpWidget_Implementation(AActor* InInstigator)
{
    if (!bInteracting && !bFalling && !bInterpolating)
    {
        PickUpWidgetComponent->SetVisibility(true);
    }
}

void AEPickableItem::HidePickUpWidget_Implementation(AActor* InInstigator)
{
    PickUpWidgetComponent->SetVisibility(false);
}

EEInteractionType AEPickableItem::GetInteractionType_Implementation()
{
    return EEInteractionType::EIT_PickUp;
}

void AEPickableItem::Interact_Implementation(AActor* InInstigator)
{
    if (bInteracting)
    {
        return;
    }
    bInteracting = true;

    if (InInstigator && InInstigator->Implements<UEPickableItemConsumerInterface>())
    {
        IEPickableItemConsumerInterface::Execute_SubscribePickableItemDelegates(InInstigator, this);
    }

    PickUp(InInstigator);
}
