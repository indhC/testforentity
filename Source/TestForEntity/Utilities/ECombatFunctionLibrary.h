﻿// © TestForEntity 2023.07.04:01:22 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ECombatFunctionLibrary.generated.h"


struct FEPickableItemConfigTableRow;

/**
 * 
 */
UCLASS(Abstract, BlueprintType)
class TESTFORENTITY_API UECombatFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:

    static class AEPickableItem* SpawnItem(UWorld* InWorld, const FEPickableItemConfigTableRow* InItemConfigRow, const TSubclassOf<AEPickableItem> InExpectedItemType, const FVector& InLocation, const FRotator& InRotation);

    UFUNCTION(BlueprintCallable, Category = "Combat|Attributes")
    static void UpdateParentAttributesComponent(const AActor* InParent, class UEAttributesComponent* InCurrentAttributesComponent);

    UFUNCTION(BlueprintCallable, Category = "Combat|Animations")
    static float PlayAnimationMontage(AActor* InOwner, UAnimMontage* InAnimMontage, const FName& InMontageSectionName);

};
