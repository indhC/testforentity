﻿// © TestForEntity 2023.07.03:21:43 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EWeaponAnimInstance.h"

#include "EWeapon.h"
#include "TestForEntity/Framework/Characters/Players/EPlayerCharacter.h"


FEWeaponAnimInstanceProxy::FEWeaponAnimInstanceProxy(UAnimInstance* InInstance): Super(InInstance)
{
}

void FEWeaponAnimInstanceProxy::PreUpdate(UAnimInstance* InAnimInstance, const float InDeltaSeconds)
{
    Super::PreUpdate(InAnimInstance, InDeltaSeconds);

    if (!InAnimInstance)
    {
        return;
    }
    if ((!OwningWeapon) ? (OwningWeapon = Cast<AEWeapon>(InAnimInstance->GetOwningActor())) : (false); !OwningWeapon)
    {
        return;
    }

    bFiring = OwningWeapon->GetShotsCount() > 0;
    bMovingClip = OwningWeapon->IsClipMoving();

    if (const AActor* Parent{OwningWeapon->GetParent()}; Parent && Parent->Implements<UEBoneTransformTrackerInterface>())
    {
        const FTransform& CurrentLeftHandTransform{IEBoneTransformTrackerInterface::Execute_GetTransform(Parent)};
        CurrentLeftHandLocation = CurrentLeftHandTransform.GetLocation();
        CurrentLeftHandRotation = CurrentLeftHandTransform.Rotator();
    }
}

void UEWeaponAnimInstance::NativeBeginPlay()
{
    Super::NativeBeginPlay();

    
}
