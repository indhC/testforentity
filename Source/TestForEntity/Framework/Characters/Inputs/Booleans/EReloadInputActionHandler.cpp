﻿// © TestForEntity 2023.07.23:01:13 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EReloadInputActionHandler.h"

#include "GameFramework/Character.h"
#include "TestForEntity/Components/Combats/Interfaces/EReloadableItemInterface.h"
#include "TestForEntity/Interfaces/EInputActionControlInterface.h"


namespace ReloadInputActionHandler
{
    const FName GNameWeaponAmmoOutHandler{TEXT("OnWeaponAmmoRanOutEvent")};
}


void UEReloadInputActionHandler::Reload() const
{
    ACharacter* Character{GetCurrentCharacter()};
    if (!IEInputActionControlInterface::Execute_IsReloadPossible(Character))
    {
        return;
    }
    if (AEAmmo* NewAmmo{IEInputActionControlInterface::Execute_GetCurrentWeaponAmmo(Character)})
    {
        IEInputActionControlInterface::Execute_ReloadCurrentWeapon(Character, NewAmmo);
    }
}

// ReSharper disable once CppMemberFunctionMayBeConst
void UEReloadInputActionHandler::OnWeaponAmmoRanOutEvent(const AEWeapon* InCurrentWeapon)
{
    Reload();
}

void UEReloadInputActionHandler::Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent)
{
    Super::Assign_Implementation(InEnhancedInputComponent);

    IEInputActionControlInterface::Execute_SubscribeWeaponAmmoOutEvent(GetCurrentCharacter(), this, ReloadInputActionHandler::GNameWeaponAmmoOutHandler);
}

bool UEReloadInputActionHandler::MayBeExecuted_Implementation() const
{
    return IEInputActionControlInterface::Execute_IsReloadPossible(GetCurrentCharacter());
}

void UEReloadInputActionHandler::Handle_Implementation(const FInputActionValue& InValue)
{
    Reload();
}
