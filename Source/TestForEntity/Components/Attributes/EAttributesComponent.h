﻿// © TestForEntity 2023.07.23:12:26 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EAttributesCommonTypes.h"
#include "EAttributesComponent.generated.h"


USTRUCT(BlueprintType)
struct FPeriodAttributeDescriptor
{
    GENERATED_BODY()


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Periodic Attribute Descriptor")
    FEAttributeDescriptor PeriodicAttribute;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Periodic Attribute Descriptor")
    float CurrentTime{0.0f};

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Periodic Attribute Descriptor")
    float ApplicationDuration{0.0f};

};


/**
 * A Component which encapsulates the logic for processing the Attributes of the Actors, as well as the logic for the interaction of the Actors through the available active
 * Attributes and the Attributes which are affected by the active Attributes.
 * It is currently possible to configure the maximum Attribute value and default value using other Attributes (see
 * FEAttributeConfigurationDataTableRow::MaximumValueAttributeTag and FEAttributeConfigurationDataTableRow::DefaultValueAttributeTag): if these tags have the value NAME_None,
 * then the corresponding values of FEAttributeConfigurationDataTableRow::MaximumValue and FEAttributeConfigurationDataTableRow::DefaultValue will be used.
 * If any of the Attributes depends on at least one other Attribute (maximum value or default value), then the configuration of the Actor Attributes available in the Component
 * must contain the dependent Attribute tag BELOW the Attribute tag(s) on which the Attribute depends.
 * The default Attribute configuration remains the same throughout the game session. The list of all Attributes loaded from the configured UDataTable is stored in the current
 * UGameInstance, which MUST implement the IEAttributesStorageInterface interface
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TESTFORENTITY_API UEAttributesComponent : public UActorComponent
{
    GENERATED_BODY()


    /** NOTE: If the MAXIMUM and/or DEFAULT value of an Attribute is dependent on other Attributes, then they MUST BE placed before this Attribute in the list!!! */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attributes", meta = (AllowPrivateAccess = "true"))
    TArray<FGameplayTag> RelatedAttributeTags;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", BlueprintGetter = "GetActiveAttributeTags", meta = (AllowPrivateAccess = "true"))
    FGameplayTagContainer ActiveAttributeTags;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", meta = (AllowPrivateAccess = "true"))
    TMap<FGameplayTag, FEAttributeDescriptor> AttributeDescriptors;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", meta = (AllowPrivateAccess = "true"))
    TMap<FGameplayTag, FPeriodAttributeDescriptor> AffectingPeriodicAttributes;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attributes", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UEAttributesComponent> ParentAttributesComponent;

protected:

    // Called when the game starts
    virtual void BeginPlay() override;


    UFUNCTION(BlueprintCallable, Category = "Attributes")
    void ApplyAttributeEffect(const AActor* Instigator, const FEAttributeDescriptor& InOtherActiveAttribute);

    UFUNCTION(BlueprintCallable, Category = "Attributes")
    void ApplyEffects(const TArray<FEAttributeDescriptor>& InAttributes);

public:

    // Sets default values for this component's properties
    UEAttributesComponent();


    virtual void TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


    //
    // Delegates
    //

    /** This Delegate is intended to handle state changes of all available Attributes. Should be used to handle changes in Attribute values which have representation in the UI */
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FOnAttributeChanged, const AActor*, InInstigator, const UEAttributesComponent*, InInstigatorAttributesComponent, const
                                                 FGameplayTag&, InAttributeTag, const float, InNewValue, const float, InValue, const float, InValueDelta);

    UPROPERTY(BlueprintAssignable, Category = "Attributes")
    FOnAttributeChanged OnAttributeChangeDelegate;


    UFUNCTION(BlueprintCallable, Category = "Attributes")
    void GetAttribute(const FGameplayTag& InAttributeTag, FEAttributeDescriptor& OutAttribute) const;

    UFUNCTION(BlueprintCallable, Category = "Attributes")
    bool ResetAttribute(const AActor* InInstigator, const FGameplayTag& InAttributeNameTag);

    UFUNCTION(BlueprintCallable, Category = "Attributes")
    bool ApplyEffect(AActor* InEffectActor);


    //
    // Getters And Setters
    //

    UFUNCTION(BlueprintCallable, Category = "Attributes")
    const FGameplayTagContainer& GetActiveAttributeTags() const
    {
        return ActiveAttributeTags;
    }

    FORCEINLINE UEAttributesComponent* GetParentAttributesComponent() const
    {
        return ParentAttributesComponent.Get();
    }

    FORCEINLINE void SetParentAttributesComponent(UEAttributesComponent* InParentAttributesComponent)
    {
        this->ParentAttributesComponent = InParentAttributesComponent;
    }

};
