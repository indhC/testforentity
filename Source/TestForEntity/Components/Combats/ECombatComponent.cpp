﻿// © TestForEntity 2023.07.03:19:42 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ECombatComponent.h"

#include "Engine/SkeletalMeshSocket.h"
#include "Interfaces/EBoneTransformTrackerInterface.h"
#include "TestForEntity/Interfaces/EMeshActorInterface.h"
#include "TestForEntity/Utilities/ECombatFunctionLibrary.h"
#include "TestForEntity/World/Ammo/EAmmo.h"
#include "TestForEntity/World/Weapons/EWeapon.h"


namespace ECombatComponent
{
    constexpr float GDefaultTimeMinimum{0.04f};
}


// Sets default values for this component's properties
UECombatComponent::UECombatComponent(): CombatConfigRowNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)},
                                        DefaultWeaponNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)},
                                        DefaultAmmoNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)},
                                        OwnerWeaponSocketName{NAME_None},
                                        HipFireAnimMontageSectionName{NAME_None},
                                        HipReloadAnimMontageSectionName{NAME_None},
                                        State{EECombatState::ECCS_Unoccupied},
                                        bCanShoot{true},
                                        bAiming{false}
{
    // Set this component to be initialized when the game starts, and to be ticked every frame. You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UECombatComponent::BeginPlay()
{
    Super::BeginPlay();

    LoadTableConfig();

    ensureAlways(DefaultWeaponNameTag.IsValid());

    ensureAlways(OwnerWeaponSocketName.IsValid());

    CurrentOwner = GetOwner();
    ensureAlways(CurrentOwner);
    ensureAlways(CurrentOwner->Implements<UEMeshActorInterface>());

    if (ensureAlways(HipFireAnimMontage))
    {
        ensureAlways(HipFireAnimMontageSectionName.IsValid());
    }

    if (ensureAlways(HipReloadAnimMontage))
    {
        ensureAlways(HipReloadAnimMontageSectionName.IsValid());
    }
}

void UECombatComponent::LoadTableConfig()
{
    if (!ensureAlwaysMsgf(CombatConfigDataTable, TEXT("The Combat Configuration Data Table is not specified!"))
        || !ensureAlwaysMsgf(CombatConfigRowNameTag.IsValid(), TEXT("The Combat Configuration Row Name Tag is not specified")))
    {
        return;
    }

    const FECombatConfigTableRow* Row{CombatConfigDataTable->FindRow<FECombatConfigTableRow>(CombatConfigRowNameTag.GetTagName(), FString{})};
    if (!ensureAlwaysMsgf(Row, TEXT("Combat Configuration Row with with Name='%s' is not found in specified Data Table!"), *CombatConfigRowNameTag.GetTagName().ToString()))
    {
        return;
    }

    DefaultWeaponNameTag = Row->DefaultWeaponNameTag;
    DefaultAmmoNameTag = Row->DefaultAmmoNameTag;
    OwnerWeaponSocketName = Row->OwnerWeaponSocketName;
    HipFireAnimMontage = Row->HipFireAnimMontage;
    HipFireAnimMontageSectionName = Row->HipFireAnimMontageSectionName;
    HipReloadAnimMontage = Row->HipReloadAnimMontage;
    HipReloadAnimMontageSectionName = Row->HipReloadAnimMontageSectionName;
}

void UECombatComponent::EquipWeapon_Implementation(AEWeapon* InWeapon)
{
    if (!IsUnoccupied() || !InWeapon)
    {
        return;
    }

    USkeletalMeshComponent* OwnerMeshComponent{IEMeshActorInterface::Execute_GetMeshComponent(CurrentOwner)};
    if (!OwnerMeshComponent)
    {
        return;
    }

    if (const USkeletalMeshSocket* OwnerMeshWeaponSocket{OwnerMeshComponent->GetSocketByName(OwnerWeaponSocketName)})
    {
        CurrentWeapon = InWeapon;
        CurrentWeapon->Equip(CurrentOwner);
        OwnerMeshWeaponSocket->AttachActor(InWeapon, OwnerMeshComponent);
    }
}

bool UECombatComponent::CanAttack_Implementation() const
{
    return CurrentWeapon && !CurrentWeapon->IsEmpty();
}

void UECombatComponent::Attack_Implementation(const FVector& InStartLocation, const FVector& InDirection)
{
    if (!IsUnoccupied() || !CurrentWeapon || !bCanShoot)
    {
        return;
    }
    bCanShoot = false;

    State = EECombatState::ECCS_Firing;

    const bool bCurrentAmmoEmpty{CurrentWeapon->IsEmpty()};
    if (!bCurrentAmmoEmpty)
    {
        UECombatFunctionLibrary::PlayAnimationMontage(CurrentOwner, HipFireAnimMontage, HipFireAnimMontageSectionName);
    }

    CurrentWeapon->Fire(InStartLocation, InDirection);

    if (bCurrentAmmoEmpty)
    {
        OnAmmoRanOutDelegate.Broadcast(CurrentWeapon);
    }

    StartEventTimer([this]()
    {
        bCanShoot = true;
        State = EECombatState::ECCS_Unoccupied;
    }, CurrentWeapon->GetDelayBetweenShots());
}

bool UECombatComponent::IsUnoccupied_Implementation() const
{
    return EECombatState::ECCS_Unoccupied == State;
}


/////////////////////////////////////////////////////////////////
/// IEReloadableItemInterface Implementation
/////////////////////////////////////////////////////////////////

bool UECombatComponent::MayBeReloaded_Implementation() const
{
    return CurrentWeapon && IsUnoccupied() && Execute_MayBeReloaded(CurrentWeapon);
}

bool UECombatComponent::Reload_Implementation(AEAmmo* InNewAmmo)
{
    if ((!IsUnoccupied() && (EECombatState::ECCS_Firing != State)) || !InNewAmmo || !CurrentWeapon)
    {
        return false;
    }

    State = EECombatState::ECCS_Reloading;

    const float TimerRate{UECombatFunctionLibrary::PlayAnimationMontage(CurrentOwner, HipReloadAnimMontage, HipReloadAnimMontageSectionName)};
    StartEventTimer([this, InNewAmmo]()
    {
        (Execute_Reload(CurrentWeapon, InNewAmmo)) ? (OnWeaponReloadedDelegate.Broadcast(CurrentWeapon, InNewAmmo)) : (false);
        State = EECombatState::ECCS_Unoccupied;
    }, FMath::Max(TimerRate, ECombatComponent::GDefaultTimeMinimum));
    return true;
}

void UECombatComponent::GrabClip_Implementation()
{
    if (CurrentWeapon && CurrentOwner && CurrentOwner->Implements<UEBoneTransformTrackerInterface>())
    {
        const USkeletalMeshComponent* WeaponMeshComponent{IEMeshActorInterface::Execute_GetMeshComponent(CurrentWeapon)};
        const int32 ClipBoneIndex{WeaponMeshComponent->GetBoneIndex(CurrentWeapon->GetClipBoneName())};
        const FTransform& ClipBoneTransform{WeaponMeshComponent->GetBoneTransform(ClipBoneIndex)};
        IEBoneTransformTrackerInterface::Execute_UpdateTransform(CurrentOwner, ClipBoneTransform);

        ReplacedAmmo = CurrentWeapon->GetCurrentAmmo();
        Execute_GrabClip(CurrentWeapon);
    }
}

void UECombatComponent::DropClip_Implementation()
{
    if (ReplacedAmmo)
    {
        OnOldClipRemovedDelegate.Broadcast(CurrentWeapon, ReplacedAmmo);
        ReplacedAmmo = nullptr;

        (CurrentWeapon) ? (Execute_DropClip(CurrentWeapon)) : (false);
    }
}

void UECombatComponent::ReplaceClip_Implementation()
{
    if (CurrentWeapon)
    {
        Execute_ReplaceClip(CurrentWeapon);
    }
}


//
// Getters And Setters
//

const USkeletalMeshSocket* UECombatComponent::GetStockSocket() const
{
    return (CurrentWeapon) ? (CurrentWeapon->GetStockSocket()) : (nullptr);
}

FTransform UECombatComponent::GetStockSocketTransform() const
{
    return (CurrentWeapon) ? (CurrentWeapon->GetStockSocketTransform()) : (FTransform{});
}
