﻿// © TestForEntity 2023.07.04:01:28 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"
#include "TestForEntity/World/EPickableItem.h"
#include "ELootGeneratorComponent.generated.h"


USTRUCT(BlueprintType)
struct FELootRulesConfig
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Item Rules")
    TSubclassOf<AEPickableItem> ActualItemType;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Item Rules", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
    float SpawnChanceRange{0.0f};

    /** Determines the Maximum Number of Items of this type which may be spawned. I. e., the actual count can be LESS than or EQUAL to AttemptsCount */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Item Rules")
    int32 AttemptsCount{0};
};


USTRUCT(BlueprintType)
struct FELootGenerationRulesTableRow : public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Generation Rules")
    TMap<FGameplayTag, FELootRulesConfig> ItemTypeLootRulesMap;
};


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TESTFORENTITY_API UELootGeneratorComponent : public USceneComponent
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Generator|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> LootGenerationRulesConfigsDataTable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Generator|Configuration", meta = (AllowPrivateAccess = "true"))
    FGameplayTag DefaultLootGenerationRulesConfigNameTag;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Generator|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> AmmoConfigsDataTable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot Generator|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> WeaponConfigsDataTable;

protected:

    // Called when the game starts
    virtual void BeginPlay() override;

public:

    // Sets default values for this component's properties
    UELootGeneratorComponent();


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Loot Generator")
    class AEAmmo* SpawnAmmo(const FGameplayTag& InAmmoNameTag);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Loot Generator")
    class AEWeapon* SpawnWeapon(const FGameplayTag& InAmmoNameTag);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Loot Generator")
    void SpawnLoot();

};
