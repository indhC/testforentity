﻿// © TestForEntity 2023.07.23:13:13 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ELineTraceComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TESTFORENTITY_API UELineTraceComponent : public UActorComponent
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Line Trace Component", meta = (AllowPrivateAccess = "true"))
    bool bTraceInTimer;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Line Trace Component", meta = (AllowPrivateAccess = "true"))
    bool bOwnerTraceConditional;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Line Trace Component", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UObject> ComponentOwner;

protected:

    // Called when the game starts
    virtual void BeginPlay() override;

public:

    // Sets default values for this component's properties
    UELineTraceComponent();


    // Called every frame
    virtual void TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


    //
    // Delegates
    //

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHitOccurred, const FHitResult&, InHitResult);

    UPROPERTY(BlueprintAssignable, Category = "Line Trace Component")
    FOnHitOccurred OnHitOccurredDelegate;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Line Trace Component")
    bool Trace(const FVector& InStartLocation, const FVector& InDirection, const float InTraceDistance, FHitResult& OutHitResult);


    //
    // Getters And Setters
    //

    FORCEINLINE void SetComponentOwner(UObject* InComponentOwner);

    FORCEINLINE void SetTraceInTimer(const bool bInTraceInTimer)
    {
        this->bTraceInTimer = bInTraceInTimer;
    }

};
