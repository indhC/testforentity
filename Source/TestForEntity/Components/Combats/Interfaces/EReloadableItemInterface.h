﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "EReloadableItemInterface.generated.h"


// This class does not need to be modified.
UINTERFACE()
class UEReloadableItemInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IEReloadableItemInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Reloadable Item")
    bool MayBeReloaded() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Reloadable Item")
    bool Reload(class AEAmmo* InNewAmmo);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Reloadable Item")
    void GrabClip();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintNativeEvent, Category = "Reloadable Item")
    void DropClip();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Reloadable Item")
    void ReplaceClip();

};
