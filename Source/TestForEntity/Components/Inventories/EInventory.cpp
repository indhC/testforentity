﻿// © TestForEntity 2023.07.03:22:58 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EInventory.h"

#include "TestForEntity/World/EPickableItem.h"


namespace EInventory
{
    constexpr int32 GDefaultSlotGroupsDelta{0};

    constexpr int32 GDefaultMaximumInventorySlotsCount{12};
    constexpr int32 GDefaultInventoryRowsCount{3};
    constexpr int32 GDefaultInventoryColumnsCount{4};

    constexpr int32 GIndexNotFound{-1};
    constexpr int32 GIndexSlotFirst{0};
}


/////////////////////////////////////////////////////////////////
/// FEInventorySlot Definitions
/////////////////////////////////////////////////////////////////

bool FEInventorySlot::IsEmpty() const
{
    return Items.IsEmpty();
}

bool FEInventorySlot::ContainsItem(const AEPickableItem* InItem) const
{
    return Items.Contains(InItem);
}

void FEInventorySlot::AddItem(AEPickableItem* InItem)
{
    if (Items.IsEmpty())
    {
        ActualItemType = InItem->GetClass();
    }
    Items.AddUnique(InItem);
}

void FEInventorySlot::RemoveItem(AEPickableItem* InItem)
{
    Items.Remove(InItem);
    if (Items.IsEmpty())
    {
        ActualItemType = nullptr;
    }
}

void FEInventorySlot::RemoveItemAt(const int32 InIndex)
{
    if (!Items.IsValidIndex(InIndex))
    {
        return;
    }
    Items.RemoveAt(InIndex);
    if (Items.IsEmpty())
    {
        ActualItemType = nullptr;
    }
}

void FEInventorySlot::SortItems(const FOnComparingNeighbourItems& InPredicate)
{
    if (InPredicate.IsBound())
    {
        Items.StableSort([&InPredicate](const AEPickableItem& InLeftItem, const AEPickableItem& InRightItem)
        {
            return InPredicate.Execute(InLeftItem, InRightItem);
        });
    }
}


/////////////////////////////////////////////////////////////////
/// UEInventory Definitions
/////////////////////////////////////////////////////////////////

UEInventory::UEInventory(): InventoryConfigRowNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)},
                            MaximumInventorySlotsCount{EInventory::GDefaultMaximumInventorySlotsCount},
                            RowsCount{EInventory::GDefaultInventoryRowsCount},
                            ColumnsCount{EInventory::GDefaultInventoryColumnsCount},
                            FreeSlotIndex{EInventory::GIndexSlotFirst}
{
}

bool UEInventory::IsSlotConfigInvalid(const FESlotByItemTypeConfig& SlotConfig, const int32 InConfigIndex) const
{
    if (!ensureAlwaysMsgf(SlotConfig.ItemType, TEXT("Slot config with index = %d for Item type does not contain Item type value!"), InConfigIndex))
    {
        return true;
    }
    if (!ensureAlwaysMsgf(!SlotConfig.bFixedItemTypeSlotsCount || (SlotConfig.bFixedItemTypeSlotsCount && (SlotConfig.ItemTypeSlotsCount > 0)),
                          TEXT("The Slot config for an Item type = '%s' with a fixed number of Slots has an ItemTypeSlotsCount <= 0 (equal to %d)!"),
                          *SlotConfig.ItemType->GetName(), SlotConfig.ItemTypeSlotsCount))
    {
        return true;
    }
    if (!ensureAlwaysMsgf(!BaseItemTypeSlotCachedItemTypeConfigMap.Contains(SlotConfig.ItemType),
                          TEXT("Another Slot config for already configured Item type = '%s'"),
                          *SlotConfig.ItemType->GetName()))
    {
        return true;
    }
    return false;
}

void UEInventory::PostInitProperties()
{
    UObject::PostInitProperties();

    if (!ensureAlwaysMsgf(InventoryConfigTable, TEXT("Inventory Configuration Data Table is not specified!"))
        || !ensureAlwaysMsgf(InventoryConfigRowNameTag.IsValid(), TEXT("The Tag with the Inventory Configuration Row Name is not specified!")))
    {
        return;
    }

    const FEInventoryConfigTableRow* ConfigRow{InventoryConfigTable->FindRow<FEInventoryConfigTableRow>(InventoryConfigRowNameTag.GetTagName(), FString{})};
    if (!ensureAlwaysMsgf(ConfigRow, TEXT("Inventory Configuration Row with Name = '%s' is not found!"), *InventoryConfigRowNameTag.GetTagName().ToString()))
    {
        return;
    }

    MaximumInventorySlotsCount = ConfigRow->MaximumInventorySlotsCount;
    RowsCount = ConfigRow->RowsCount;
    ColumnsCount = ConfigRow->ColumnsCount;

    TArray ItemTypeSlotConfigs{ConfigRow->SupportedItemTypeSlotConfigs};
    ItemTypeSlotConfigs.Sort([this](const FESlotByItemTypeConfig& InLeft, const FESlotByItemTypeConfig& InRight)
    {
        if (!InLeft.bFixedItemTypeSlotsCount)
        {
            return false;
        }
        if (InLeft.bFixedItemTypeSlotsCount && !InRight.bFixedItemTypeSlotsCount)
        {
            return true;
        }
        return CompareSlotConfigs(InLeft, InRight);
    });

    int32 ActualSortOrder{0};
    int32 PreviousSortOrder{0};
    for (int32 i = 0; (i < ItemTypeSlotConfigs.Num()) && (Slots.Num() < MaximumInventorySlotsCount); i++)
    {
        FESlotByItemTypeConfig& SlotConfig{ItemTypeSlotConfigs[i]};
        if (IsSlotConfigInvalid(SlotConfig, i))
        {
            continue;
        }

        if (PreviousSortOrder != SlotConfig.SortOrder)
        {
            PreviousSortOrder = SlotConfig.SortOrder;
            SlotConfig.SortOrder = ++ActualSortOrder;
        }

        const int32 MaximumCount{MaximumInventorySlotsCount - Slots.Num()};
        const int32 Count{(SlotConfig.bFixedItemTypeSlotsCount) ? (FMath::Clamp(SlotConfig.ItemTypeSlotsCount, 0, MaximumCount)) : (MaximumCount)};
        SlotConfig.ItemTypeSlotsCount = Count;
        BaseItemTypeSlotCachedItemTypeConfigMap.Add(SlotConfig.ItemType, FEInventorySlotCachedConfig{SlotConfig, Slots.Num(), (Slots.Num() + Count - 1)});
        for (int32 j = 0; j < Count; j++)
        {
            Slots.Add(FEInventorySlot{SlotConfig});
        }
    }

    ensureAlwaysMsgf((BaseItemTypeSlotCachedItemTypeConfigMap.Num() == ItemTypeSlotConfigs.Num()),
                     TEXT(
                         "Some Slot configs for Item types are missing! Make sure:\n1. Each ItemType is set;\n2. Each config with a fixed number of Slots has "
                         "ItemTypeSlotsCount > 0;\n3. there is only one config with non-fixed Slots number (the most basic ItemType which can be put in Inventory)!\nConfigs "
                         "count = %d, used configs count = %d"
                     ),
                     BaseItemTypeSlotCachedItemTypeConfigMap.Num(), ItemTypeSlotConfigs.Num());
}

int32 UEInventory::FindSlotIndex(const TSubclassOf<AEPickableItem>& InNewItemType, const FSlotSearchConditionSignature& InSearchConditionDelegate, const int32 InMaximumIndex) const
{
    if (!InNewItemType || (InMaximumIndex < 0) || (InMaximumIndex > Slots.Num()) || !InSearchConditionDelegate.IsBound())
    {
        return EInventory::GIndexNotFound;
    }

    int32 Result{EInventory::GIndexNotFound};
    bool bSlotFound{false};
    TSubclassOf<AEPickableItem> PreviouslyFoundItemType{nullptr};
    for (int32 i = 0; !bSlotFound && (i < InMaximumIndex);)
    {
        const FEInventorySlot* Slot{&Slots[i]};
        const TSubclassOf<AEPickableItem>& SlotItemType{Slot->GetItemType()};
        if (!InNewItemType->IsChildOf(SlotItemType) || (PreviouslyFoundItemType && PreviouslyFoundItemType->IsChildOf(SlotItemType)))
        {
            i += Slot->GetConfiguration().ItemTypeSlotsCount;
            continue;
        }

        const FEInventorySlotCachedConfig& CachedConfig{BaseItemTypeSlotCachedItemTypeConfigMap[Slot->GetItemType()]};
        const int32 ActualEndIndex{FMath::Min((InMaximumIndex - 1), CachedConfig.EndIndex)};
        for (EESearchConditionState SlotsBlockState{EESearchConditionState::ESCS_NotFound};
             !bSlotFound && (EESearchConditionState::ESCS_FirstFound != SlotsBlockState) && (i <= ActualEndIndex); i++)
        {
            Slot = &Slots[i];
            SlotsBlockState = InSearchConditionDelegate.Execute(InNewItemType, *Slot);
            bSlotFound = EESearchConditionState::ESCS_Found == SlotsBlockState;
            if (EESearchConditionState::ESCS_NotFound != SlotsBlockState)
            {
                Result = i;
                PreviouslyFoundItemType = SlotItemType;
            }
        }
        i = ActualEndIndex + 1;
    }
    return Result;
}

void UEInventory::ProcessSlotItemsByCondition(const TSubclassOf<AEPickableItem>& InBaseItemType, const FItemsProcessingCallbackSignature& InCallbackDelegate,
                                              const bool bInIncludeNotFilled) const
{
    if (!InBaseItemType || !InCallbackDelegate.IsBound())
    {
        return;
    }

    for (int32 i = 0; i < FreeSlotIndex;)
    {
        const FEInventorySlot* Slot{&Slots[i]};
        if (const TSubclassOf<AEPickableItem>& ActualType{Slot->GetActualItemType()}; !ActualType || !ActualType->IsChildOf(InBaseItemType))
        {
            i += Slot->GetConfiguration().ItemTypeSlotsCount;
            continue;
        }

        i++;
        if (bInIncludeNotFilled || !Slot->IsEmpty())
        {
            InCallbackDelegate.Execute(Slot);
        }
    }
}

TSubclassOf<AEPickableItem> UEInventory::FindBaseItemType(const TSubclassOf<AEPickableItem>& InItemType) const
{
    TSubclassOf<AEPickableItem> Result;
    for (const TTuple<TSubclassOf<AEPickableItem>, FEInventorySlotCachedConfig>& ConfigMap : BaseItemTypeSlotCachedItemTypeConfigMap)
    {
        if (InItemType->IsChildOf(ConfigMap.Key) && (!Result || ConfigMap.Key->IsChildOf(Result)))
        {
            Result = ConfigMap.Key;
        }
    }
    return Result;
}

int32 UEInventory::FindSlotIndexByItemType(const TSubclassOf<AEPickableItem>& InItemType, const bool bInIncludeEmptySlots) const
{
    FSlotSearchConditionSignature Delegate;
    int32 MinElementsCount{TNumericLimits<int32>::Max()};
    Delegate.BindLambda([bInIncludeEmptySlots, &MinElementsCount](const TSubclassOf<AEPickableItem>& InBaseItemType, const FEInventorySlot& InSlot)
    {
        if (InSlot.IsEmpty())
        {
            return (bInIncludeEmptySlots) ? (EESearchConditionState::ESCS_FirstFound) : (EESearchConditionState::ESCS_NotFound);
        }
        if ((InSlot.GetActualItemType() != InBaseItemType) || (InSlot.GetItemsCount() >= MinElementsCount))
        {
            return EESearchConditionState::ESCS_NotFound;
        }
        MinElementsCount = InSlot.GetItemsCount();
        return EESearchConditionState::ESCS_BetterMatchFound;
    });
    return FindSlotIndex(InItemType, Delegate, FreeSlotIndex);
}

int32 UEInventory::FindFreeSlot(const TSubclassOf<AEPickableItem>& InItemType) const
{
    FSlotSearchConditionSignature Delegate;
    Delegate.BindLambda([](const TSubclassOf<AEPickableItem>& InSearchableItemType, const FEInventorySlot& InSlot)
    {
        if (!InSlot.IsEmpty())
        {
            return EESearchConditionState::ESCS_NotFound;
        }
        return (InSlot.GetActualItemType() == InSearchableItemType) ? (EESearchConditionState::ESCS_Found) : (EESearchConditionState::ESCS_FirstFound);
    });
    return FindSlotIndex(InItemType, Delegate, Slots.Num());
}

bool UEInventory::IsItemValid(const AEPickableItem* InNewItem) const
{
    return InNewItem && !InNewItem->IsActorBeingDestroyed();
}

bool UEInventory::CanAddItemByAdditionalConditions_Implementation(AEPickableItem* InNewItem, TSubclassOf<AEPickableItem> InBaseType) const
{
    if (InNewItem->IsEmpty())
    {
        return false;
    }
    if (const FEInventorySlotCachedConfig& SlotConfig{BaseItemTypeSlotCachedItemTypeConfigMap[InBaseType]}; SlotConfig.Config.bFixedItemTypeSlotsCount)
    {
        return GetItemTypeSlotsCount(InBaseType, false) < SlotConfig.Config.ItemTypeSlotsCount;
    }
    if (FreeSlotIndex < MaximumInventorySlotsCount)
    {
        return true;
    }
    const int32 SlotIndex{FindSlotIndexByItemType(InNewItem->GetClass())};
    if (SlotIndex < EInventory::GIndexSlotFirst)
    {
        return false;
    }
    const FEInventorySlot* Slot{&Slots[SlotIndex]};
    return Slot->GetItemType() && (Slot->GetItemsCount() < InNewItem->GetMaximumCountPerSlot());
}

void UEInventory::AddItemToSlot_Implementation(AEPickableItem* InNewItem, const int32 InSlotIndex)
{
    FEInventorySlot* Slot{&Slots[InSlotIndex]};
    Slot->AddItem(InNewItem);
    // May be a configurable or extensible option...
    FEInventorySlot::FOnComparingNeighbourItems Event;
    Event.BindLambda([](const AEPickableItem& InLeftItem, const AEPickableItem& InRightItem)
    {
        return InLeftItem.GetCount() > InRightItem.GetCount();
    });
    Slot->SortItems(Event);
}

bool UEInventory::CompareSlotConfigs_Implementation(const FESlotByItemTypeConfig& InLeft, const FESlotByItemTypeConfig& InRight) const
{
    return InLeft.SortOrder == InRight.SortOrder;
}

void UEInventory::SortSlots()
{
    Slots.StableSort([this](const FEInventorySlot& InLeftSlot, const FEInventorySlot& InRightSlot)
    {
        const FESlotByItemTypeConfig& LeftConfig{InLeftSlot.GetConfiguration()};
        const FESlotByItemTypeConfig& RightConfig{InRightSlot.GetConfiguration()};
        if (!CompareSlotConfigs(LeftConfig, RightConfig))
        {
            return false;
        }
        if (LeftConfig.bSortableByTypeName && InLeftSlot.GetActualItemType() && InRightSlot.GetActualItemType()
            && (InLeftSlot.GetActualItemType()->GetName() > InRightSlot.GetActualItemType()->GetName()))
        {
            return false;
        }
        if (!LeftConfig.bEmptySlotSortable && InLeftSlot.IsEmpty())
        {
            return false;
        }
        return InLeftSlot.GetItemsCount() > InRightSlot.GetItemsCount();
    });
}

TArray<AEPickableItem*> UEInventory::RemoveItemsFromSlot(FEInventorySlot& InSlot, const int32 InCount)
{
    const int32 Count{FMath::Clamp(InCount, EInventory::GIndexSlotFirst, InSlot.GetItemsCount())};
    TArray<AEPickableItem*> Result;
    for (int32 i = 0; i < Count; i++)
    {
        Result.Insert(InSlot[InSlot.GetItemsCount() - 1], EInventory::GIndexSlotFirst);
        InSlot.RemoveItemAt(InSlot.GetItemsCount() - 1);
    }

    SortSlots();
    while ((FreeSlotIndex > 0) && Slots[FreeSlotIndex - 1].IsEmpty())
    {
        FreeSlotIndex--;
    }
    return Result;
}

bool UEInventory::CanAddItem(AEPickableItem* InNewItem) const
{
    if (!IsItemValid(InNewItem))
    {
        return false;
    }

    if (const int32 Index{GetItemSlotIndex(InNewItem)}; Index > EInventory::GIndexNotFound)
    {
        return false;
    }

    const TSubclassOf<AEPickableItem>& BaseType{FindBaseItemType(InNewItem->GetClass())};
    return BaseType && CanAddItemByAdditionalConditions(InNewItem, BaseType);
}

int32 UEInventory::Add(AEPickableItem* InNewItem, const bool bInSkipAddValidation)
{
    if (!bInSkipAddValidation && !CanAddItem(InNewItem))
    {
        return EInventory::GIndexNotFound;
    }

    if (const int32 Index{GetItemSlotIndex(InNewItem)}; Index > EInventory::GIndexNotFound)
    {
        return Index;
    }

    const TSubclassOf<AEPickableItem>& ItemType{InNewItem->GetClass()};
    int32 Result{FindSlotIndexByItemType(ItemType)};
    if ((Result < EInventory::GIndexSlotFirst) || (Slots[Result].GetItemsCount() >= InNewItem->GetMaximumCountPerSlot()))
    {
        Result = FindFreeSlot(ItemType);
        (Result >= FreeSlotIndex) ? (FreeSlotIndex = Result + 1) : (false);
    }
    if (Result > EInventory::GIndexNotFound)
    {
        AddItemToSlot(InNewItem, Result);
        SortSlots();
    }

    return Result;
}

AEPickableItem* UEInventory::Peek(const TSubclassOf<AEPickableItem>& InItemType)
{
    const int32 SlotIndex{FindSlotIndexByItemType(InItemType, false)};
    if (SlotIndex < EInventory::GIndexSlotFirst)
    {
        return nullptr;
    }

    if (const FEInventorySlot* Slot{&Slots[SlotIndex]}; !Slot->IsEmpty())
    {
        return Slot->GetItems().Last();
    }
    return nullptr;
}

AEPickableItem* UEInventory::PeekBySlotNumber(const int32 InSlotNumber)
{
    if (!Slots.IsValidIndex(InSlotNumber))
    {
        return nullptr;
    }
    const FEInventorySlot* Slot{&Slots[InSlotNumber]};
    return (Slot->IsEmpty()) ? (nullptr) : (Slot->GetItems().Last());
}

AEPickableItem* UEInventory::Remove(const AEPickableItem* InItem)
{
    const int32 SlotIndex{GetItemSlotIndex(InItem)};
    if (SlotIndex < EInventory::GIndexSlotFirst)
    {
        return nullptr;
    }
    const TArray<AEPickableItem*>& RemovedItems{RemoveItemsFromSlot(Slots[SlotIndex])};
    return (RemovedItems.IsEmpty()) ? (nullptr) : (RemovedItems.Last());
}

TArray<AEPickableItem*> UEInventory::RemoveOfType(const TSubclassOf<AEPickableItem>& InItemType, const int32 InCount)
{
    const int32 SlotIndex{FindSlotIndexByItemType(InItemType, false)};
    if (SlotIndex < EInventory::GIndexSlotFirst)
    {
        return TArray<AEPickableItem*>{};
    }

    return RemoveItemsFromSlot(Slots[SlotIndex], InCount);
}

TArray<AEPickableItem*> UEInventory::RemoveFromSlot(const int32 InSlotNumber, const int32 InCount)
{
    if (!Slots.IsValidIndex(InSlotNumber))
    {
        return TArray<AEPickableItem*>{};
    }
    return RemoveItemsFromSlot(Slots[InSlotNumber], InCount);
}

void UEInventory::ProcessUsedItem_Implementation(AEPickableItem* InOldItem)
{
    // The default implementation of the Inventory does not move the remaining used Items into the Inventory, nor does it populate another Items in the Inventory of the same
    // type with the used Item.
    // For example, after reloading a Weapon, the used Ammo (magazine) MUST BE dropped into the World and MUST BE replaced with new Ammo (magazine) removed from the Inventory
    if (InOldItem)
    {
        OnItemDroppedDelegate.Broadcast(InOldItem);
    }
}

void UEInventory::GetItemTypesSlotIndexesRange(const TSubclassOf<AEPickableItem>& InItemType, int32& OutStartIndex, int32& OutEndIndex) const
{
    const TSubclassOf<AEPickableItem>& BaseItemType{FindBaseItemType(InItemType)};
    OutStartIndex = EInventory::GIndexNotFound;
    OutEndIndex = EInventory::GIndexNotFound;
    if (!BaseItemType)
    {
        return;
    }

    if (const FEInventorySlotCachedConfig* CachedConfig{BaseItemTypeSlotCachedItemTypeConfigMap.Find(BaseItemType)})
    {
        OutStartIndex = CachedConfig->StartIndex;
        OutEndIndex = CachedConfig->EndIndex;
    }
}

bool UEInventory::ContainsItemTypeSlot(const TSubclassOf<AEPickableItem>& InBaseItemType) const
{
    FSlotSearchConditionSignature Delegate;
    Delegate.BindLambda([](const TSubclassOf<AEPickableItem>& InItemType, const FEInventorySlot& InSlot)
    {
        const bool bFound{(InSlot.GetActualItemType() == InItemType) && !InSlot.IsEmpty()};
        return (bFound) ? (EESearchConditionState::ESCS_Found) : (EESearchConditionState::ESCS_NotFound);
    });
    return FindSlotIndex(InBaseItemType, Delegate, FreeSlotIndex) > EInventory::GIndexNotFound;
}

TSubclassOf<AEPickableItem> UEInventory::ContainsItemTypesSlot(const TArray<TSubclassOf<AEPickableItem>>& InBaseItemTypes) const
{
    for (int32 i = 0; i < FreeSlotIndex; i++)
    {
        for (const TSubclassOf<AEPickableItem>& BaseItemType : InBaseItemTypes)
        {
            if (const FEInventorySlot* Slot{&Slots[i]}; !Slot->IsEmpty() && Slot->GetActualItemType()->IsChildOf(BaseItemType))
            {
                return BaseItemType;
            }
        }
    }
    return nullptr;
}

int32 UEInventory::GetItemTypeSlotsCount(const TSubclassOf<AEPickableItem>& InBaseItemType, const bool bInIncludeNotFilled) const
{
    int32 Result{0};
    FItemsProcessingCallbackSignature Delegate;
    Delegate.BindLambda([&Result](const FEInventorySlot*)
    {
        Result++;
    });
    ProcessSlotItemsByCondition(InBaseItemType, Delegate, bInIncludeNotFilled);
    return Result;
}

int32 UEInventory::GetItemSlotIndex(const AEPickableItem* InItem) const
{
    FSlotSearchConditionSignature Delegate;
    Delegate.BindLambda([InItem](const TSubclassOf<AEPickableItem>&, const FEInventorySlot& InSlot)
    {
        const bool bFound{InSlot.ContainsItem(InItem)};
        return (bFound) ? (EESearchConditionState::ESCS_Found) : (EESearchConditionState::ESCS_NotFound);
    });
    return FindSlotIndex(InItem->GetClass(), Delegate, FreeSlotIndex);
}

TArray<AEPickableItem*> UEInventory::FindItemsByType(const TSubclassOf<AEPickableItem>& InBaseItemType) const
{
    TArray<AEPickableItem*> Result;
    FItemsProcessingCallbackSignature Delegate;
    Delegate.BindLambda([&Result](const FEInventorySlot* InSlot)
    {
        Result.Append(InSlot->GetItems());
    });
    ProcessSlotItemsByCondition(InBaseItemType, Delegate, false);
    return Result;
}
