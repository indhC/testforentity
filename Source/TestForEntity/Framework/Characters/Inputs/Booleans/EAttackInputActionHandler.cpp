﻿// © TestForEntity 2023.07.22:20:49 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EAttackInputActionHandler.h"

#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "TestForEntity/Components/Combats/Interfaces/ECombatScreenConfigInterface.h"
#include "TestForEntity/Interfaces/EInputActionControlInterface.h"


bool UEAttackInputActionHandler::MayBeExecuted_Implementation() const
{
    return IEInputActionControlInterface::Execute_IsAttackPossible(GetCurrentCharacter());
}

void UEAttackInputActionHandler::Handle_Implementation(const FInputActionValue& InValue)
{
    ACharacter* Character{GetCurrentCharacter()};
    const APlayerController* PlayerController{Cast<APlayerController>(Character->GetController())};
    if (!PlayerController)
    {
        return;
    }

    if (FVector WorldPosition, WorldRotation;
        UGameplayStatics::DeprojectScreenToWorld(PlayerController, IECombatScreenConfigInterface::Execute_GetCrosshairScreenLocation(Character), WorldPosition, WorldRotation))
    {
        IEInputActionControlInterface::Execute_Attack(Character, WorldPosition, WorldRotation);
    }
}
