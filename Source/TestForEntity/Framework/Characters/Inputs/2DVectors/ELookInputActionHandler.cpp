﻿// © TestForEntity 2023.07.21:04:41 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ELookInputActionHandler.h"

#include "GameFramework/Character.h"


void UELookInputActionHandler::Handle_Implementation(const FInputActionValue& InValue)
{
    ACharacter* Character{GetCurrentCharacter()};
    const FVector2D AxisVector{InValue.Get<FVector2D>()};
    Character->AddControllerYawInput(AxisVector.X);
    Character->AddControllerPitchInput(AxisVector.Y);
}
