﻿// © TestForEntity 2023.07.23:02:20 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EInputActionHandlerChain.h"

#include "GameplayTagContainer.h"
#include "Kismet/GameplayStatics.h"
#include "TestForEntity/Utilities/ERegistriesFunctionLibrary.h"


namespace InputActionHandlerChain
{
    constexpr int32 GIndexNotSet{-1};
}


UEInputActionHandlerChain::UEInputActionHandlerChain(): bExecuteAllHandlers{false},
                                                        MandatoryHandlerIndex{InputActionHandlerChain::GIndexNotSet}
{
}

void UEInputActionHandlerChain::Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent)
{
    ensureAlways(!InputActionHandlersChainNameTags.IsEmpty());

    Super::Assign_Implementation(InEnhancedInputComponent);

    ACharacter* Character{GetCurrentCharacter()};
    UGameInstance* GameInstance{UGameplayStatics::GetGameInstance(this)};
    for (const FGameplayTag InputActionHandlerNameTag : InputActionHandlersChainNameTags)
    {
        InputActionHandlersChain.Add(UERegistriesFunctionLibrary::GetInputActionHandler(GameInstance, Character, InputActionHandlerNameTag));
    }
}

void UEInputActionHandlerChain::Handle_Implementation(const FInputActionValue& InValue)
{
    for (UEInputActionHandler* InputActionHandler : InputActionHandlersChain)
    {
        if (!InputActionHandler->MayBeExecuted())
        {
            continue;
        }

        InputActionHandler->Handle(InValue);
        if (!bExecuteAllHandlers)
        {
            return;
        }
    }

    if (InputActionHandlersChain.IsValidIndex(MandatoryHandlerIndex))
    {
        InputActionHandlersChain[MandatoryHandlerIndex]->Handle(InValue);
    }
}
