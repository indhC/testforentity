﻿// © TestForEntity 2023.07.23:12:24 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ETestForEntityGameInstance.h"

#include "GameFramework/Character.h"
#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"


FEAttributeDescriptor UETestForEntityGameInstance::TryCacheAttributeDescriptor(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeNameTag)
{
    FEAttributeDescriptor Result;
    if (!ensureAlways(AttributesDataTable) || !InAttributeNameTag.IsValid())
    {
        return Result;
    }

    if (const FEAttributeConfigurationDataTableRow* AttributeRow{AttributesDataTable->FindRow<FEAttributeConfigurationDataTableRow>(InAttributeNameTag.GetTagName(), FString{})})
    {
        Result.Initialize(InAttributesOwner, AttributeRow);
        AttributeDescriptors.Add(InAttributeNameTag, Result);
    }

    return Result;
}

UEInputActionHandler* UETestForEntityGameInstance::TryCacheInputActionHandler(ACharacter* InOwner, const FGameplayTag& InInputActionHandlerNameTag)
{
    UEInputActionHandler* Result{nullptr};
    if (!ensureAlways(InputActionHandlersDataTable) || !InInputActionHandlerNameTag.IsValid())
    {
        return Result;
    }

    const FEInputActionHandlerConfigRow* HandlerRow{InputActionHandlersDataTable->FindRow<FEInputActionHandlerConfigRow>(InInputActionHandlerNameTag.GetTagName(), FString{})};
    if (!HandlerRow || !ensureAlways(HandlerRow->HandlerType))
    {
        return Result;
    }

    Result = NewObject<UEInputActionHandler>(InOwner, HandlerRow->HandlerType);
    Result->Initialize(*HandlerRow, InOwner);
    InputActionHandlers.Add(InInputActionHandlerNameTag, Result);
    return Result;
}

FEAttributeDescriptor UETestForEntityGameInstance::GetCachedAttributeDescriptor_Implementation(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeNameTag)
{
    FEAttributeDescriptor* Result{AttributeDescriptors.Find(InAttributeNameTag)};
    return (Result) ? (*Result) : (TryCacheAttributeDescriptor(InAttributesOwner, InAttributeNameTag));
}

UEInputActionHandler* UETestForEntityGameInstance::GetCachedInputActionHandler_Implementation(ACharacter* InOwner, const FGameplayTag& InInputActionHandlerNameTag)
{
    const TObjectPtr<UEInputActionHandler>* Result{InputActionHandlers.Find(InInputActionHandlerNameTag)};
    return (Result) ? (Result->Get()) : (TryCacheInputActionHandler(InOwner, InInputActionHandlerNameTag));
}
