﻿// © TestForEntity 2023.07.04:23:34 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Engine/DataTable.h"
#include "TestForEntity/TestForEntity.h"
#include "EPickUpWidget.generated.h"


USTRUCT(BlueprintType)
struct FEPickUpWidgetTableRow : public FTableRowBase {
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pick Up Widget Row")
    FColor BackgroundColor{FColor::Black};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pick Up Widget Row")
    FColor RightTopPanelBackgroundColor{FColor::Black};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pick Up Widget Row")
    FColor RightBottomPanelBackgroundColor{FColor::Black};
};


/**
 * 
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class TESTFORENTITY_API UEPickUpWidget : public UUserWidget {
    GENERATED_BODY()


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UDataTable> PickUpWidgetTable;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    FColor BackgroundColor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    FColor RightTopPanelBackgroundColor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    FColor RightBottomPanelBackgroundColor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UTexture> ItemIconTexture;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    FText ItemNameText;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    FText ItemActionText;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    EEItemRarity ItemRarity;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pick Up Widget|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 ItemCount;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pick Up Widget", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class AEPickableItem> OwningItem;

protected:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pick Up Widget")
    void OnItemInitializedEvent(const AEPickableItem* InItem);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pick Up Widget")
    void OnItemCountChangedEvent(const AEPickableItem* InItem, const int32 InPreviousCount, const int32 InNewCount);

public:

    UFUNCTION(BlueprintCallable, Category = "Pick Up Widget")
    void SetOwningItem(AEPickableItem* InOwningItem);

};
