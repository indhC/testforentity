﻿// © TestForEntity 2023.07.23:02:15 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "EInputActionHandler.h"

#include "EInputActionHandlerChain.generated.h"


struct FGameplayTag;

/**
 * Implementation of Chain of Responsibility pattern to execute one Input Action Handler based on its Execution Condition
 */
UCLASS()
class TESTFORENTITY_API UEInputActionHandlerChain : public UEInputActionHandler
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input ActionHandler Chain|Configuration", meta = (AllowPrivateAccess = "true"))
    bool bExecuteAllHandlers;

    /** The Index of the Handler in the Chain of Handlers, which MUST BE executed regardless, if no other Handler has taken responsibility for execution */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input ActionHandler Chain|Configuration", meta = (AllowPrivateAccess = "true"))
    int32 MandatoryHandlerIndex;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input ActionHandler Chain|Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<FGameplayTag> InputActionHandlersChainNameTags;


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Input ActionHandler Chain", meta = (AllowPrivateAccess = "true"))
    TArray<TObjectPtr<UEInputActionHandler>> InputActionHandlersChain;

public:

    UEInputActionHandlerChain();


    virtual void Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent) override;

    virtual void Handle_Implementation(const FInputActionValue& InValue) override;

};
