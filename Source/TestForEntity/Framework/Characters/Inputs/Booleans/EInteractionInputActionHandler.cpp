﻿// © TestForEntity 2023.07.21:19:34 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EInteractionInputActionHandler.h"

#include "GameFramework/Character.h"
#include "TestForEntity/Components/Combats/Interfaces/ECombatScreenConfigInterface.h"
#include "TestForEntity/Interfaces/EInputActionControlInterface.h"
#include "TestForEntity/World/EPickableItem.h"


namespace InteractionInputActionHandler
{
    const FName GNameCapsuleComponentBeginOverlapHandler{TEXT("OnCapsuleComponentBeginOverlap")};
    const FName GNameCapsuleComponentEndOverlapHandler{TEXT("OnCapsuleComponentEndOverlap")};

    const FName GNameTraceHitHandlerHandler{TEXT("OnHitOccurredEvent")};
}


UEInteractionInputActionHandler::UEInteractionInputActionHandler(): OverlappedPickableItemsCount{0},
                                                                    bPickableItemsCheckingPaused{false}
{
}

// ReSharper disable once CppParameterNeverUsed
// ReSharper disable once CppParameterMayBeConstPtrOrRef
void UEInteractionInputActionHandler::OnCapsuleComponentBeginOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp,
                                                                     const int32 InOtherBodyIndex, const bool bInFromSweep, const FHitResult& InSweepResult)
{
    if (InOtherActor && InOtherActor->GetClass()->IsChildOf(AEPickableItem::StaticClass()))
    {
        OverlappedPickableItemsCount++;
    }
}

// ReSharper disable once CppParameterNeverUsed
// ReSharper disable once CppParameterMayBeConstPtrOrRef
void UEInteractionInputActionHandler::OnCapsuleComponentEndOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp,
                                                                   const int32 InOtherBodyIndex)
{
    if (InOtherActor && InOtherActor->GetClass()->IsChildOf(AEPickableItem::StaticClass()) && ((OverlappedPickableItemsCount - 1) >= 0))
    {
        OverlappedPickableItemsCount--;
    }

    if ((OverlappedPickableItemsCount > 0) || !TraceFoundItem)
    {
        return;
    }

    if (TraceFoundItem->Implements<UEInteractionInterface>())
    {
        IEInteractionInterface::Execute_HidePickUpWidget(TraceFoundItem, GetCurrentCharacter());
    }
}

void UEInteractionInputActionHandler::OnHitOccurredEvent(const FHitResult& InHitResult)
{
    bPickableItemsCheckingPaused = true;

    ACharacter* Character{GetCurrentCharacter()};
    if (TraceFoundItem && TraceFoundItem->Implements<UEInteractionInterface>())
    {
        IEInteractionInterface::Execute_HidePickUpWidget(TraceFoundItem, Character);
    }

    TraceFoundItem = InHitResult.GetActor();
    if (TraceFoundItem && TraceFoundItem->Implements<UEInteractionInterface>())
    {
        IEInteractionInterface::Execute_ShowPickUpWidget(TraceFoundItem, Character);
    }

    FTimerHandle TracePauseTimerHandle;
    FTimerDelegate TimerDelegate;
    TimerDelegate.BindLambda([this]()
    {
        bPickableItemsCheckingPaused = false;
    });
    Character->GetWorldTimerManager().SetTimer(TracePauseTimerHandle, TimerDelegate, IECombatScreenConfigInterface::Execute_GetTraceDelay(Character), false);
}

void UEInteractionInputActionHandler::Assign_Implementation(UEnhancedInputComponent* InEnhancedInputComponent)
{
    Super::Assign_Implementation(InEnhancedInputComponent);

    ACharacter* Character{GetCurrentCharacter()};
    IEInputActionControlInterface::Execute_SubscribeCapsuleOverlapEvents(Character, this,
                                                                         InteractionInputActionHandler::GNameCapsuleComponentBeginOverlapHandler,
                                                                         InteractionInputActionHandler::GNameCapsuleComponentEndOverlapHandler);

    IEInputActionControlInterface::Execute_SubscribeTraceHitEvent(Character, this, InteractionInputActionHandler::GNameTraceHitHandlerHandler);

    IEInputActionControlInterface::Execute_UpdateTraceOwner(Character, this);
}

void UEInteractionInputActionHandler::Handle_Implementation(const FInputActionValue& InValue)
{
    if (!TraceFoundItem || !TraceFoundItem->Implements<UEInteractionInterface>())
    {
        return;
    }

    if (ACharacter* Character{GetCurrentCharacter()};
        (EEInteractionType::EIT_PickUp != IEInteractionInterface::Execute_GetInteractionType(TraceFoundItem))
        || IEInputActionControlInterface::Execute_IsTraceItemInventoryAddable(Character, TraceFoundItem))
    {
        IEInteractionInterface::Execute_Interact(TraceFoundItem, Character);
    }
}


/////////////////////////////////////////////////////////////////
/// IEPeriodicTraceConditionInterface Implementation
/////////////////////////////////////////////////////////////////

bool UEInteractionInputActionHandler::IsCurrentlyTraceable_Implementation()
{
    return (OverlappedPickableItemsCount > 0) && !bPickableItemsCheckingPaused;
}
