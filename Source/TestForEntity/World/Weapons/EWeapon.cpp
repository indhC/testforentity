﻿// © TestForEntity 2023.07.03:22:23 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EWeapon.h"

#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "TestForEntity/Components/Attributes/EAttributesComponent.h"
#include "TestForEntity/Components/Combats/Interfaces/EBoneTransformTrackerInterface.h"
#include "TestForEntity/Components/Traces/ELineTraceComponent.h"
#include "TestForEntity/Utilities/ECombatFunctionLibrary.h"
#include "TestForEntity/World/Ammo/EAmmo.h"


namespace EWeapon
{
    constexpr float GDefaultDelayBetweenShots{0.3f};
    constexpr float GDefaultBulletHittingRange{50'000.0f};

    constexpr int32 GCountSingleBullet{1};
}


// Sets default values
AEWeapon::AEWeapon(): ClipBoneName{NAME_None},
                      WeaponStockSocketName{NAME_None},
                      bAutomatic{false},
                      DelayBetweenShots{EWeapon::GDefaultDelayBetweenShots},
                      BulletHittingRange{EWeapon::GDefaultBulletHittingRange},
                      ShotsCount{0},
                      bMovingClip{false}
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    TraceComponent = CreateDefaultSubobject<UELineTraceComponent>(TEXT("TraceComponent"));
    TraceComponent->SetTraceInTimer(false);
}

// Called when the game starts or when spawned
void AEWeapon::BeginPlay()
{
    Super::BeginPlay();
}

void AEWeapon::SpawnImpactParticles_Implementation(const FHitResult& InHitResult, const AActor* InOtherActor)
{
    if (InOtherActor)
    {
        const FVector UpVector{InOtherActor->GetActorUpVector()};
        FVector RotationDirection{FVector::CrossProduct(UpVector, InHitResult.ImpactNormal)};
        RotationDirection.Normalize();
        const double RotationAngle{FMath::Acos(FVector::DotProduct(UpVector, InHitResult.ImpactNormal))};
        const FQuat RotationQuat{FQuat{RotationDirection, RotationAngle} * InOtherActor->GetActorQuat()};
        UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, ImpactParticles, InHitResult.ImpactPoint, RotationQuat.Rotator(), FVector::OneVector, true, true);
    }
}

void AEWeapon::PlayImpactSound_Implementation(const AActor* InOtherActor, const FVector& InImpactPoint)
{
    if (const TObjectPtr<USoundBase>& Sound{(InOtherActor) ? (ImpactBodySound) : (ImpactSurfaceSound)})
    {
        UGameplayStatics::PlaySoundAtLocation(this, Sound.Get(), InImpactPoint, FRotator::ZeroRotator, 1.0f, 1.0f, 0.0f, SoundAttenuation.Get());
    }
}

void AEWeapon::ApplyWeaponEffects_Implementation(const AActor* InOtherActor)
{
    if (InOtherActor && InOtherActor->Implements<UEAttributesActorInterface>())
    {
        UEAttributesComponent* OtherAttributesComponent{Execute_GetAttributesComponent(InOtherActor)};
        (OtherAttributesComponent) ? (OtherAttributesComponent->ApplyEffect(CurrentAmmo.Get())) : (false);
    }
}

// Called every frame
void AEWeapon::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AEWeapon::Initialize(const FEPickableItemConfigTableRow* InRow)
{
    const FEWeaponConfigTableRow* WeaponConfig{static_cast<const FEWeaponConfigTableRow*>(InRow)};
    if (!WeaponConfig)
    {
        checkNoEntry();
    }

    Super::Initialize(InRow);

    SupportedAmmoTypes = WeaponConfig->SupportedAmmoTypes;
    ClipBoneName = WeaponConfig->ClipBoneName;
    WeaponStockSocketName = WeaponConfig->WeaponStockSocketName;
    bAutomatic = WeaponConfig->bAutomatic;
    DelayBetweenShots = WeaponConfig->DelayBetweenShots;
    BulletHittingRange = WeaponConfig->BulletHittingRange;
    SoundAttenuation = WeaponConfig->SoundAttenuation;
    FireEmptyAmmoSound = WeaponConfig->FireEmptyAmmoSound;
    ImpactParticles = WeaponConfig->ImpactParticles;
    ImpactBodySound = WeaponConfig->ImpactBodySound;
    ImpactSurfaceSound = WeaponConfig->ImpactSurfaceSound;

    if (!WeaponConfig->DefaultAmmoNameTag.IsValid() || !ensureAlways(WeaponConfig->AmmoConfigDataTable))
    {
        return;
    }

    if (const FEAmmoConfigTableRow* AmmoConfig{WeaponConfig->AmmoConfigDataTable->FindRow<FEAmmoConfigTableRow>(WeaponConfig->DefaultAmmoNameTag.GetTagName(), FString{}, false)};
        ensureAlways(AmmoConfig) && ensureAlways(SupportedAmmoTypes.Contains(AmmoConfig->ActualType)))
    {
        AEAmmo* DefaultAmmo{Cast<AEAmmo>(UECombatFunctionLibrary::SpawnItem(GetWorld(), AmmoConfig, AEAmmo::StaticClass(), GetActorLocation(), GetActorRotation()))};
        (ensureAlways(DefaultAmmo)) ? (Execute_Reload(this, DefaultAmmo)) : (false);
    }
}

void AEWeapon::Drop_Implementation()
{
    Execute_GetMeshComponent(this)->DetachFromComponent(FDetachmentTransformRules{EDetachmentRule::KeepWorld, true});

    Super::Drop_Implementation();
}

void AEWeapon::Fire(const FVector& InStartLocation, const FVector& InDirection)
{
    if (bMovingClip)
    {
        return;
    }

    if (IsEmpty())
    {
        (FireEmptyAmmoSound)
            ? (UGameplayStatics::PlaySoundAtLocation(this, FireEmptyAmmoSound.Get(), GetActorLocation(), GetActorRotation(), 1.0f, 1.0f, 0.0f, SoundAttenuation.Get()))
            : (false);
        return;
    }

    ShotsCount++;
    CurrentAmmo->Use(EWeapon::GCountSingleBullet);

    FHitResult HitResult;
    if (!TraceComponent->Trace(InStartLocation, InDirection, BulletHittingRange, HitResult))
    {
        return;
    }

    const AActor* OtherActor{HitResult.GetActor()};
    PlayImpactSound(OtherActor, HitResult.ImpactPoint);
    if (ImpactParticles)
    {
        SpawnImpactParticles(HitResult, OtherActor);
    }

    ApplyWeaponEffects(OtherActor);
}

void AEWeapon::OnShotOverNotificationHandler()
{
    if (ShotsCount > 0)
    {
        ShotsCount--;
    }
}

bool AEWeapon::IsEmpty_Implementation() const
{
    return !CurrentAmmo || (CurrentAmmo->GetCount() <= 0);
}


/////////////////////////////////////////////////////////////////
/// IEReloadableItemInterface Implementation
/////////////////////////////////////////////////////////////////

bool AEWeapon::MayBeReloaded_Implementation() const
{
    return !CurrentAmmo || (CurrentAmmo->GetCount() < CurrentAmmo->GetMaximumCount());
}

bool AEWeapon::Reload_Implementation(AEAmmo* InNewAmmo)
{
    const bool bResult{InNewAmmo && !InNewAmmo->IsActorBeingDestroyed()};
    if (bResult)
    {
        CurrentAmmo = InNewAmmo;
        CurrentAmmo->Equip(this);
    }
    return bResult;
}

void AEWeapon::GrabClip_Implementation()
{
    bMovingClip = true;
}

void AEWeapon::DropClip_Implementation()
{
    if (const AActor* Parent{GetParent()}; Parent && CurrentAmmo && Parent->Implements<UEBoneTransformTrackerInterface>())
    {
        const FTransform& LeftHandTransform{IEBoneTransformTrackerInterface::Execute_GetTransform(Parent)};
        CurrentAmmo->SetActorLocationAndRotation(LeftHandTransform.GetLocation(), LeftHandTransform.GetRotation(), false, nullptr, ETeleportType::TeleportPhysics);
    }
}

void AEWeapon::ReplaceClip_Implementation()
{
    bMovingClip = false;
}


//
// Getters And Setters
//

int32 AEWeapon::GetAmmoCount() const
{
    return (CurrentAmmo) ? (CurrentAmmo->GetCount()) : (0);
}

const USkeletalMeshSocket* AEWeapon::GetStockSocket() const
{
    return Execute_GetMeshComponent(this)->GetSocketByName(WeaponStockSocketName);
}

FTransform AEWeapon::GetStockSocketTransform() const
{
    const USkeletalMeshComponent* Mesh{Execute_GetMeshComponent(this)};
    return Mesh->GetSocketTransform(WeaponStockSocketName, RTS_World);
}
