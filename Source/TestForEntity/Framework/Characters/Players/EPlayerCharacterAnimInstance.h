﻿// © TestForEntity 2023.07.05:04:23 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h"
#include "EPlayerCharacterAnimInstance.generated.h"


USTRUCT(BlueprintType)
struct FEPlayerCharacterAnimInstanceProxy: public FAnimInstanceProxy
{
    GENERATED_BODY()


    FEPlayerCharacterAnimInstanceProxy() = default;

    explicit FEPlayerCharacterAnimInstanceProxy(UAnimInstance* InAnimInstance);


    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Configuration")
    TObjectPtr<class AEPlayerCharacter> PlayerCharacter;


    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Movement")
    double Speed{0.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Movement")
    bool bShouldStartJogging{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Movement")
    bool bShouldStopJogging{true};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Movement")
    bool bInAir{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Movement")
    bool bAccelerating{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Movement")
    double MovementOffsetYaw{0.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Combat")
    bool bAiming{false};
    
    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Combat")
    bool bReloading{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Combat")
    double bLeftHandTransformTrackingAlpha{1.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Combat")
    FTransform StockSocketTransform;

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    double CharacterForwardRotationYaw{0.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    double CharacterForwardRotationNegateYaw{0.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    double CharacterForwardRotationPitch{0.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    bool bShouldTurnRight{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    bool bShouldNotTurnRight{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    bool bShouldTurnLeft{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    bool bShouldNotTurnLeft{false};


    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    double PreviousPlayerRotationYaw{0.0};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Turn In Place")
    float PreviousRotationCurveValue{0.0f};

protected:

    void UpdateSpeed(const FVector& Velocity);

    void UpdateMovementOffsetYaw(const FVector& InVelocity, const FRotator& InBaseAimRotation);

    void ResetGroundRelatedStates();

    void UpdateCharacterForwardRotation();

    void UpdateCharacterForwardRotationPerTurningCurve(const class UEPlayerCharacterAnimInstance* InAnimInstance);

    void UpdateTurnRightLeftFlags();


    virtual void PreUpdate(UAnimInstance * InAnimInstance, const float InDeltaSeconds) override;

};


/**
 * 
 */
UCLASS(Transient, BlueprintType)
class TESTFORENTITY_API UEPlayerCharacterAnimInstance : public UAnimInstance
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Configuration", meta = (AllowPrivateAccess = "true"))
    FName RotationMetadataCurveName;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Configuration", meta = (AllowPrivateAccess = "true"))
    FName RotationCurveName;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Proxy", meta = (AllowPrivateAccess = "true"))
    FEPlayerCharacterAnimInstanceProxy Proxy;


    friend struct FEPlayerCharacterAnimInstanceProxy;

protected:

    virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override
    {
        return &Proxy;
    }

    virtual void DestroyAnimInstanceProxy(FAnimInstanceProxy* InProxy) override
    {
    }

public:

    UEPlayerCharacterAnimInstance();

    virtual void NativeInitializeAnimation() override;

};
