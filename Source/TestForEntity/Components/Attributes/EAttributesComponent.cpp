﻿// © TestForEntity 2023.07.23:12:28 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EAttributesComponent.h"

#include "Interfaces/EAttributesActorInterface.h"
#include "Interfaces/EAttributesStorageInterface.h"
#include "Kismet/GameplayStatics.h"
#include "TestForEntity/Utilities/ERegistriesFunctionLibrary.h"


// Sets default values for this component's properties
UEAttributesComponent::UEAttributesComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}

void UEAttributesComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (AffectingPeriodicAttributes.IsEmpty())
    {
        return;
    }

    TArray<FEAttributeDescriptor> AffectingDescriptors;
    TArray<FGameplayTag> Keys;
    AffectingPeriodicAttributes.GetKeys(Keys);
    for (const FGameplayTag& Key : Keys)
    {
        FPeriodAttributeDescriptor& Attribute{AffectingPeriodicAttributes[Key]};
        if (Attribute.ApplicationDuration >= Attribute.PeriodicAttribute.GetDuration())
        {
            AffectingPeriodicAttributes.Remove(Key);
            continue;
        }
        Attribute.ApplicationDuration += DeltaTime;

        Attribute.CurrentTime += DeltaTime;
        if (Attribute.CurrentTime >= Attribute.PeriodicAttribute.GetPeriod())
        {
            Attribute.CurrentTime = 0.0f;
            AffectingDescriptors.Add(Attribute.PeriodicAttribute);
        }
    }

    ApplyEffects(AffectingDescriptors);
}

// Called when the game starts
void UEAttributesComponent::BeginPlay()
{
    Super::BeginPlay();

    UGameInstance* GameInstance{UGameplayStatics::GetGameInstance(GetOwner())};
    for (const FGameplayTag& AttributeTag : RelatedAttributeTags)
    {
        if (AttributeDescriptors.Contains(AttributeTag))
        {
            continue;
        }

        FEAttributeDescriptor Descriptor;
        if (!UERegistriesFunctionLibrary::GetAttributeDescriptor(GameInstance, this, AttributeTag, Descriptor))
        {
            continue;
        }

        AttributeDescriptors.Add(AttributeTag, Descriptor);
        if (Descriptor.IsActive())
        {
            ActiveAttributeTags.AddTag(AttributeTag);
        }
    }
}

void UEAttributesComponent::ApplyAttributeEffect(const AActor* Instigator, const FEAttributeDescriptor& InOtherActiveAttribute)
{
    float OtherActiveAttributeValue{InOtherActiveAttribute.GetValue()};
    for (const FGameplayTag& AffectedAttributeTag : InOtherActiveAttribute.GetAffecting())
    {
        FEAttributeDescriptor* AffectedAttribute{AttributeDescriptors.Find(AffectedAttributeTag)};
        if (!AffectedAttribute || FMath::IsNearlyZero(OtherActiveAttributeValue))
        {
            continue;
        }

        const float PreviousOtherActiveAttributeValue{OtherActiveAttributeValue};
        const float PreviousAffectedAttributeValue{AffectedAttribute->GetValue()};
        OtherActiveAttributeValue = AffectedAttribute->Update(this, OtherActiveAttributeValue);
        if (const float CurrentAffectedValue{AffectedAttribute->GetValue()}; CurrentAffectedValue != PreviousAffectedAttributeValue)
        {
            OnAttributeChangeDelegate.Broadcast(Instigator, this, AffectedAttributeTag, CurrentAffectedValue, PreviousOtherActiveAttributeValue, OtherActiveAttributeValue);
        }
    }
}

void UEAttributesComponent::ApplyEffects(const TArray<FEAttributeDescriptor>& InAttributes)
{
    const AActor* Instigator{GetOwner()};
    for (const FEAttributeDescriptor& Attribute : InAttributes)
    {
        ApplyAttributeEffect(Instigator, Attribute);
    }
}

void UEAttributesComponent::GetAttribute(const FGameplayTag& InAttributeTag, FEAttributeDescriptor& OutAttribute) const
{
    OutAttribute = FEAttributeDescriptor{};
    const FEAttributeDescriptor* Attribute{AttributeDescriptors.Find(InAttributeTag)};
    if (!Attribute)
    {
        return;
    }

    if (!ParentAttributesComponent)
    {
        OutAttribute = *Attribute;
        return;
    }

    OutAttribute = FEAttributeDescriptor{*Attribute};
    for (const FGameplayTag& AffectedByTag : Attribute->GetAffectedBy())
    {
        FEAttributeDescriptor ParentAttribute;
        ParentAttributesComponent->GetAttribute(AffectedByTag, ParentAttribute);
        if (!ParentAttribute.IsInvalid())
        {
            OutAttribute.Update(this, ParentAttribute.GetValue());
        }
    }
}

bool UEAttributesComponent::ResetAttribute(const AActor* InInstigator, const FGameplayTag& InAttributeNameTag)
{
    if (!InInstigator || (InInstigator != GetOwner()))
    {
        return false;
    }

    FEAttributeDescriptor* AttributeDescriptor{AttributeDescriptors.Find(InAttributeNameTag)};
    if (!AttributeDescriptor)
    {
        return false;
    }

    AttributeDescriptor->Update(this, AttributeDescriptor->GetDefault(this));
    return true;
}

bool UEAttributesComponent::ApplyEffect(AActor* InEffectActor)
{
    if (!InEffectActor || !InEffectActor->Implements<UEAttributesActorInterface>())
    {
        return false;
    }

    const UEAttributesComponent* OtherAttributesComponent{IEAttributesActorInterface::Execute_GetAttributesComponent(InEffectActor)};
    if (!OtherAttributesComponent)
    {
        return false;
    }

    const AActor* Instigator{GetOwner()};
    for (const FGameplayTag& OtherActiveAttributeTag : OtherAttributesComponent->GetActiveAttributeTags())
    {
        FEAttributeDescriptor OtherActiveAttribute;
        OtherAttributesComponent->GetAttribute(OtherActiveAttributeTag, OtherActiveAttribute);
        if (OtherActiveAttribute.IsInvalid())
        {
            continue;
        }

        if (!OtherActiveAttribute.IsPeriodic())
        {
            ApplyAttributeEffect(Instigator, OtherActiveAttribute);
            continue;
        }

        if (FPeriodAttributeDescriptor* AttributeDescriptor{AffectingPeriodicAttributes.Find(OtherActiveAttributeTag)})
        {
            AttributeDescriptor->ApplicationDuration = 0.0f;
        }
        else
        {
            AffectingPeriodicAttributes.Add(OtherActiveAttributeTag, FPeriodAttributeDescriptor{OtherActiveAttribute});
        }
    }

    return true;
}
