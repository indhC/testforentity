﻿// © TestForEntity 2023.07.04:03:40 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/Interface.h"
#include "ECombatScreenConfigInterface.generated.h"


USTRUCT(BlueprintType)
struct FECombatScreenConfigTableRow : public FTableRowBase
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    FName LeftHandBoneName{NAME_None};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    FName RightHandBoneName{NAME_None};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    TObjectPtr<UTexture> CrosshairDefaultTexture;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    FVector2D CrosshairTextureSize{0.0, 0.0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    FVector2D CrosshairScreenOffset{0.0, 0.0};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    float TraceDistance{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Screen Configuration Table Row")
    float TraceDelay{0.0f};
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCombatScreenConfigChaned);


// This class does not need to be modified.
UINTERFACE()
class UECombatScreenConfigInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IECombatScreenConfigInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    UTexture* GetCrosshairDefaultTexture() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    FVector2D GetCrosshairTextureSize() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    FVector2D GetCrosshairTextureHalfSize() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    FVector2D GetCrosshairScreenOffset() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    FVector2D GetCrosshairScreenLocation() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    float GetTraceDistance() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    FVector GetTargetPickUpLocation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    FRotator GetCameraRotation() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Combat Screen Configuration Interface")
    float GetTraceDelay();

};
