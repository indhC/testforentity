﻿// © TestForEntity 2023.07.04:03:44 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "TestForEntity/Interfaces/ELoadableItemInterface.h"
#include "TestForEntity/World/EPickableItem.h"
#include "EAmmo.generated.h"


USTRUCT(BlueprintType)
struct FEAmmoConfigTableRow: public FEPickableItemConfigTableRow
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo Configuration")
    TArray<TSubclassOf<class AEWeapon>> SupportedWeaponTypes;
};


UCLASS(Blueprintable, BlueprintType)
class TESTFORENTITY_API AEAmmo : public AEPickableItem, public IELoadableItemInterface
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo|Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<TSubclassOf<AEWeapon>> SupportedWeaponTypes;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    virtual EEItemState GetEquippedState_Implementation() const override;

    virtual void UpdateInitialLocation_Implementation() override;

public:

    // Sets default values for this actor's properties
    AEAmmo();


    // Called every frame
    virtual void Tick(const float DeltaTime) override;


    virtual void Initialize(const FEPickableItemConfigTableRow* InRow) override;

    virtual void Use_Implementation(const int32 InCount) override;


    /////////////////////////////////////////////////////////////////
    /// IELoadableItemInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual void LoadMaximally_Implementation(AEPickableItem* InSourceItem) override;


    //
    // Getters And Setters
    //

    FORCEINLINE const TArray<TSubclassOf<AEWeapon>>& GetSupportedWeaponTypes() const
    {
        return SupportedWeaponTypes;
    }

};
