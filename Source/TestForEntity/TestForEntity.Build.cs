// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TestForEntity : ModuleRules
{
	public TestForEntity(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivateDependencyModuleNames.AddRange(new string[] { "ControlRig" });
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "GameplayTags", "Niagara", "UMG" });
	}
}
