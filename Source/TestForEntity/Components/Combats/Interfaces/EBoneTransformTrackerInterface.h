﻿// © TestForEntity 2023.07.23:13:13 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "EBoneTransformTrackerInterface.generated.h"


// This class does not need to be modified.
UINTERFACE()
class UEBoneTransformTrackerInterface : public UInterface
{
    GENERATED_BODY()
};


/**
 * 
 */
class TESTFORENTITY_API IEBoneTransformTrackerInterface
{
    GENERATED_BODY()

    // Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bone Transform Tracker")
    FTransform GetTransform() const;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bone Transform Tracker")
    void UpdateTransform(const FTransform& InUpdatedTransform);

};
