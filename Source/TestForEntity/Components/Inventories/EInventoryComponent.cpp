﻿// © TestForEntity 2023.07.07:18:39 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EInventoryComponent.h"

#include "EInventory.h"
#include "TestForEntity/World/Ammo/EAmmo.h"
#include "TestForEntity/World/Weapons/EWeapon.h"


namespace EInventoryComponent
{
    constexpr int32 KDefaultCurrentWeaponIndex{0};

    constexpr int32 KSlotsNumberZero{0};
    constexpr int32 KSlotsNumberAdditional{1};

    const TSubclassOf<AEPickableItem> KTypeWeapon{AEWeapon::StaticClass()};
    const TSubclassOf<AEPickableItem> KTypeAmmo{AEAmmo::StaticClass()};
}


// Sets default values for this component's properties
UEInventoryComponent::UEInventoryComponent(): CurrentWeaponIndex{EInventoryComponent::KDefaultCurrentWeaponIndex}
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UEInventoryComponent::BeginPlay()
{
    Super::BeginPlay();

    if (ensureAlways(InventoryType))
    {
        Inventory = NewObject<UEInventory>(this, InventoryType);
        Inventory->OnItemPickedUpDelegate.AddDynamic(this, &UEInventoryComponent::OnItemPickedUpByInventoryEvent);
        Inventory->OnItemDroppedDelegate.AddDynamic(this, &UEInventoryComponent::OnItemDroppedFromInventoryEvent);
    }
}

int32 UEInventoryComponent::FindNewExistingWeaponIndex_Implementation(AEWeapon* InWeapon)
{
    int32 SlotStartIndex;
    int32 SlotEndIndex;
    Inventory->GetItemTypesSlotIndexesRange(InWeapon->GetClass(), SlotStartIndex, SlotEndIndex);

    int32 Result{SlotStartIndex};
    while ((Result <= SlotEndIndex) && !Inventory->PeekBySlotNumber(Result))
    {
        Result++;
    }

    return Result;
}

void UEInventoryComponent::PickUpItemWithStatus_Implementation(AEPickableItem* InItem, const EEInventoryOperationStatus InPickUpStatus)
{
    if (!InItem)
    {
        return;
    }
    if (!Inventory->CanAddItem(InItem))
    {
        const EEInventoryOperationStatus Status{
            (InItem && InItem->GetClass()->IsChildOf(EInventoryComponent::KTypeWeapon))
                ? (EEInventoryOperationStatus::FWeaponSlotsAreOut)
                : (EEInventoryOperationStatus::FFreeInventorySlotsAreOut)
        };
        OnItemPickedUpDelegate.Broadcast(Status, InItem);
        return;
    }

    if (AEWeapon* Weapon{Cast<AEWeapon>(InItem)})
    {
        PickUpWeapon(Weapon, InPickUpStatus);
    }
    else if (AEAmmo* Ammo{Cast<AEAmmo>(InItem)})
    {
        PickUpAmmo(Ammo, InPickUpStatus);
    }
    // else if (AEConsumable* Consumable{Cast<AEConsumable>(InItem)})
    // {
    //     // TODO DOV:
    //     PickUpConsumable(Consumable);
    // }
    else
    {
        checkNoEntry();
    }
}

void UEInventoryComponent::PickUpAmmo_Implementation(AEAmmo* InAmmo, const EEInventoryOperationStatus InPickUpStatus)
{
    if (!InAmmo)
    {
        return;
    }
    Inventory->Add(InAmmo, true);
    OnItemPickedUpDelegate.Broadcast(InPickUpStatus, InAmmo);
}

void UEInventoryComponent::PickUpWeapon_Implementation(AEWeapon* InWeapon, const EEInventoryOperationStatus InPickUpStatus)
{
    if (!InWeapon)
    {
        return;
    }
    if (const int32 NewWeaponIndex{Inventory->Add(InWeapon, true)}; !Cast<AEWeapon>(Inventory->PeekBySlotNumber(CurrentWeaponIndex)))
    {
        SwapCurrentWeaponIndex(NewWeaponIndex, false);
    }

    OnItemPickedUpDelegate.Broadcast(InPickUpStatus, InWeapon);
}

void UEInventoryComponent::SwapCurrentWeaponIndex_Implementation(const int32 InNewIndex, const bool bInValidateIndex)
{
    if (bInValidateIndex && !Cast<AEWeapon>(Inventory->PeekBySlotNumber(InNewIndex)))
    {
        OnWeaponSwappedDelegate.Broadcast(EEInventoryOperationStatus::FInvalidWeaponIndex, GetCurrentWeapon(), CurrentWeaponIndex, InNewIndex);
        return;
    }

    const int32 PreviousWeaponIndex{CurrentWeaponIndex};
    CurrentWeaponIndex = InNewIndex;
    if (PreviousWeaponIndex != CurrentWeaponIndex)
    {
        OnWeaponSwappedDelegate.Broadcast(EEInventoryOperationStatus::SSuccess, GetCurrentWeapon(), PreviousWeaponIndex, CurrentWeaponIndex);
    }
}

TSubclassOf<AEAmmo> UEInventoryComponent::UpdateCurrentAmmoType_Implementation(const AEWeapon* InWeapon)
{
    if (!InWeapon)
    {
        return nullptr;
    }

    const TArray<TSubclassOf<AEPickableItem>>& AmmoTypes{InWeapon->GetSupportedAmmoTypes()};
    const TSubclassOf<AEAmmo>& PreviousAmmoType{CurrentAmmoType};
    CurrentAmmoType = Inventory->ContainsItemTypesSlot(AmmoTypes);
    if (const bool bAmmoTypeChanged{PreviousAmmoType != CurrentAmmoType}; bAmmoTypeChanged && !CurrentAmmoType)
    {
        OnAmmoTypeSelectedDelegate.Broadcast(EEInventoryOperationStatus::FSupportedAmmoTypeNotFound, PreviousAmmoType, CurrentAmmoType);
    }
    else if (bAmmoTypeChanged)
    {
        OnAmmoTypeSelectedDelegate.Broadcast(EEInventoryOperationStatus::SSuccess, PreviousAmmoType, CurrentAmmoType);
    }
    return CurrentAmmoType;
}

void UEInventoryComponent::DropRemovedItems_Implementation(const TArray<AEPickableItem*>& InRemovedItems, const bool bInCurrentWeaponExists)
{
    OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SItemDroppingStarted, InRemovedItems);
    for (AEPickableItem* Item : InRemovedItems)
    {
        if (AEWeapon* Weapon{Cast<AEWeapon>(Item)})
        {
            DropWeapon(Weapon, bInCurrentWeaponExists);
        }
        else
        {
            DropSingleItem(Item);
        }
    }
    OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SItemDroppingEnded, InRemovedItems);
}

void UEInventoryComponent::DropWeapon_Implementation(AEWeapon* InWeapon, const bool bInCurrentWeaponExists)
{
    if (!InWeapon)
    {
        return;
    }
    if (const bool bCurrentWeaponDropped{nullptr == Inventory->PeekBySlotNumber(CurrentWeaponIndex)};
        bCurrentWeaponDropped && (bInCurrentWeaponExists != bCurrentWeaponDropped))
    {
        SwapCurrentWeaponIndex(FindNewExistingWeaponIndex(InWeapon), false);
    }

    OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SSuccess, {InWeapon});
}

void UEInventoryComponent::DropSingleItem_Implementation(AEPickableItem* InItem)
{
    if (InItem)
    {
        OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SSuccess, {InItem});
    }
}

void UEInventoryComponent::OnItemPickedUpByInventoryEvent_Implementation(AEPickableItem* InItem)
{
    PickUpItemWithStatus(InItem, EEInventoryOperationStatus::SPickingUpUsedItem);
}

void UEInventoryComponent::OnItemDroppedFromInventoryEvent_Implementation(AEPickableItem* InItem)
{
    if (InItem)
    {
        OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SDroppingUsedItem, {InItem});
    }
}

// Called every frame
void UEInventoryComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UEInventoryComponent::CanAddItem_Implementation(AActor* InItem)
{
    return Inventory->CanAddItem(Cast<AEPickableItem>(InItem));
}

void UEInventoryComponent::SwapWeapon_Implementation(const int32 InNewIndex)
{
    SwapCurrentWeaponIndex(InNewIndex, true);
}

void UEInventoryComponent::ProcessUsedItem_Implementation(AEPickableItem* InOldItem)
{
    Inventory->ProcessUsedItem(InOldItem);
}

TSubclassOf<AEAmmo> UEInventoryComponent::GetCurrentWeaponAmmoType_Implementation() const
{
    return CurrentAmmoType;
}

AEAmmo* UEInventoryComponent::GetCurrentWeaponAmmo_Implementation()
{
    const AEWeapon* Weapon{GetCurrentWeapon()};
    // Do we have a Weapon in our Inventory and does it need Reloading at all?
    if (!Weapon || !IEReloadableItemInterface::Execute_MayBeReloaded(Weapon))
    {
        return nullptr;
    }

    // Do we have Ammo for the current Weapon type?
    if (!Inventory->ContainsItemTypeSlot(CurrentAmmoType) && !UpdateCurrentAmmoType(Weapon))
    {
        OnWeaponReloadedDelegate.Broadcast(EEInventoryOperationStatus::FOutOfAmmo, Weapon, CurrentAmmoType, 0);
        return nullptr;
    }

    const TArray<AEPickableItem*>& Result{Inventory->RemoveOfType(CurrentAmmoType)};
    OnWeaponReloadedDelegate.Broadcast(EEInventoryOperationStatus::SSuccess, Weapon, CurrentAmmoType, Result.Last()->GetCount());
    return Cast<AEAmmo>(Result.Last());
}

void UEInventoryComponent::SelectWeaponAmmoType_Implementation(const AEAmmo* InAmmo)
{
    if (const AEWeapon* Weapon{GetCurrentWeapon()};
        !InAmmo || !Weapon || !InAmmo->GetSupportedWeaponTypes().Contains(Weapon->GetClass()) || !Inventory->ContainsItemTypeSlot(InAmmo->GetClass()))
    {
        OnWeaponReloadedDelegate.Broadcast(EEInventoryOperationStatus::FInvalidAmmoType, Weapon, CurrentAmmoType, Weapon->GetAmmoCount());
        return;
    }

    const TSubclassOf<AEAmmo>& PreviousAmmoType{CurrentAmmoType};
    CurrentAmmoType = InAmmo->GetClass();
    OnAmmoTypeSelectedDelegate.Broadcast(EEInventoryOperationStatus::SSuccess, PreviousAmmoType, CurrentAmmoType);
}

void UEInventoryComponent::PickUpItem_Implementation(AEPickableItem* InItem)
{
    PickUpItemWithStatus(InItem, EEInventoryOperationStatus::SSuccess);
}

void UEInventoryComponent::DropItem_Implementation(AEPickableItem* InItem)
{
    const bool bCurrentWeaponExists{nullptr != Inventory->PeekBySlotNumber(CurrentWeaponIndex)};
    if (!InItem || !Inventory->Remove(InItem))
    {
        OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SNoDroppingItems, {InItem});
        return;
    }

    DropRemovedItems({InItem}, bCurrentWeaponExists);
}

void UEInventoryComponent::DropItemsFromSlot_Implementation(const int32 InSlotNumber, const int32 InCount)
{
    const bool bCurrentWeaponExists{nullptr != Inventory->PeekBySlotNumber(CurrentWeaponIndex)};
    if (const TArray<AEPickableItem*>& RemovedItems{Inventory->RemoveFromSlot(InSlotNumber, InCount)}; !RemovedItems.IsEmpty())
    {
        DropRemovedItems(RemovedItems, bCurrentWeaponExists);
    }
    else
    {
        OnItemDroppedDelegate.Broadcast(EEInventoryOperationStatus::SNoDroppingItems, RemovedItems);
    }
}


//
// Getters And Setters
//

AEWeapon* UEInventoryComponent::GetCurrentWeapon() const
{
    return Cast<AEWeapon>(Inventory->PeekBySlotNumber(CurrentWeaponIndex));
}
