﻿// © TestForEntity 2023.07.05:00:08 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EPickUpWidget.h"

#include "TestForEntity/World/EPickableItem.h"


namespace EPickUpWidget {
    const TCHAR* GItemRarityEnumPackage{TEXT("/Script/TestForEntity.EEItemRarity")};
}


void UEPickUpWidget::OnItemInitializedEvent_Implementation(const AEPickableItem* InItem) {
    ItemIconTexture = InItem->GetIconTexture();
    ItemNameText = InItem->GetNameText();
    ItemActionText = InItem->GetActionText();
    ItemRarity = InItem->GetRarity();
    ItemCount = InItem->GetCount();

    if (!ensureAlways(PickUpWidgetTable)) {
        return;
    }

    const UEnum* ItemRarityEnum{FindObject<UEnum>(FTopLevelAssetPath{EPickUpWidget::GItemRarityEnumPackage}, true)};
    const FName& RowName{ItemRarityEnum->GetDisplayNameTextByValue(static_cast<int64>(ItemRarity)).ToString()};
    if (const FEPickUpWidgetTableRow* Row{PickUpWidgetTable->FindRow<FEPickUpWidgetTableRow>(RowName, FString{})}; ensureAlways(Row)) {
        BackgroundColor = Row->BackgroundColor;
        RightTopPanelBackgroundColor = Row->RightTopPanelBackgroundColor;
        RightBottomPanelBackgroundColor = Row->RightBottomPanelBackgroundColor;
    }
}

void UEPickUpWidget::OnItemCountChangedEvent_Implementation(const AEPickableItem* InItem, const int32 InPreviousCount, const int32 InNewCount) {
    ItemCount = InNewCount;
}

void UEPickUpWidget::SetOwningItem(AEPickableItem* InOwningItem) {
    OwningItem = InOwningItem;

    OwningItem->OnItemInitializedDelegate.AddUniqueDynamic(this, &UEPickUpWidget::OnItemInitializedEvent);
    OwningItem->OnItemCountChangedDelegate.AddUniqueDynamic(this, &UEPickUpWidget::OnItemCountChangedEvent);
}
