// © TestForEntity 2023.07.23:12:21 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"


UENUM(BlueprintType)
enum class EEItemRarity: uint8
{
    EIR_Damaged UMETA(DisplayName = "Rarity.Damaged"),
    EIR_Common UMETA(DisplayName = "Rarity.Common"),
    EIR_Uncommon UMETA(DisplayName = "Rarity.Uncommon"),
    EIR_Rare UMETA(DisplayName = "Rarity.Rare"),
    EIR_Legendary UMETA(DisplayName = "Rarity.Legendary"),
};
