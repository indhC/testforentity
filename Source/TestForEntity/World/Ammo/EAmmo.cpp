﻿// © TestForEntity 2023.07.04:03:45 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EAmmo.h"


// Sets default values
AEAmmo::AEAmmo()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEAmmo::BeginPlay()
{
    Super::BeginPlay();
}

EEItemState AEAmmo::GetEquippedState_Implementation() const
{
    return EEItemState::EIS_PickedUp;
}

void AEAmmo::UpdateInitialLocation_Implementation()
{
}

// Called every frame
void AEAmmo::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AEAmmo::Initialize(const FEPickableItemConfigTableRow* InRow)
{
    const FEAmmoConfigTableRow* AmmoConfig{static_cast<const FEAmmoConfigTableRow*>(InRow)};
    if (!AmmoConfig)
    {
        checkNoEntry();
    }

    Super::Initialize(InRow);

    SupportedWeaponTypes = AmmoConfig->SupportedWeaponTypes;
}

void AEAmmo::Use_Implementation(const int32 InCount)
{
    if (const int32 CurrentCount{GetCount()}; CurrentCount > 0)
    {
        SetCount(FMath::Max(0, CurrentCount - InCount));
    }
}


/////////////////////////////////////////////////////////////////
/// IELoadableItemInterface Implementation
/////////////////////////////////////////////////////////////////

void AEAmmo::LoadMaximally_Implementation(AEPickableItem* InSourceItem)
{
    const int32 SourceCount{InSourceItem->GetCount()};
    const int32 Delta{FMath::Min(SourceCount, (GetMaximumCount() - SourceCount))};
    InSourceItem->SetCount(SourceCount - Delta);
    SetCount(GetCount() + Delta);
}
