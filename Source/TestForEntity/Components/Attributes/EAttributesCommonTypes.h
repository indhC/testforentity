﻿// © TestForEntity 2023.07.07:18:38 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameplayTagContainer.h"
#include "EAttributesCommonTypes.generated.h"


UENUM(BlueprintType)
enum class EEEffectOperation: uint8
{
    EEO_Addition UMETA(DisplayName = "Operation.Addition"),
    EEO_Multiplication UMETA(DisplayName = "Operation.Multiplication"),
};


USTRUCT(BlueprintType)
struct FEAttributeConfigurationDataTableRow : public FTableRowBase
{
    GENERATED_BODY()


    /**
     * Specifies the behavior of the Attribute. Active Attributes are used to apply the Effect of one Actor to the Attributes of another Actor. Passive Attributes are affected
     * by Active Attributes. Passive Attributes basically define the state of an Actor. Active Attributes basically define the state value of the applied Effect (operation
     * state value). Also, Active Attributes can define Periodic Effects (DOTs)
     */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    bool bActive{false};

    /**
     * This value determines whether the current Attribute describes a Periodic Effect which ticks every Period seconds. Currently, the Effect value is not adjusted by Parent
     * Attributes
     */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration", DisplayName = "PERIODIC: Periodic")
    bool bPeriodic{false};

    /** This value determines how rarely or how often the current Periodic Effect Attribute will affect the Actor's Attributes Component */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration", DisplayName = "PERIODIC: Period",
        meta = (EditCondition = "bPeriodic", EditConditionHides = "true"))
    float Period{0.0f};

    /** This value determines how long the current Periodic Effect Attribute will affect the Actor's Attribute Component */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration", DisplayName = "PERIODIC: Duration",
        meta = (EditCondition = "bPeriodic", EditConditionHides = "true"))
    float Duration{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    EEEffectOperation EffectOperation{EEEffectOperation::EEO_Addition};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    float MinimumValue{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    float MaximumValue{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    FGameplayTag MaximumValueAttributeTag{FGameplayTag::RequestGameplayTag(NAME_None, false)};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    float DefaultValue{0.0f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    FGameplayTag DefaultValueAttributeTag{FGameplayTag::RequestGameplayTag(NAME_None, false)};

    /** List of Attribute Name Tags affected by this attribute (e.g. Damage affects Health) */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    TArray<FGameplayTag> Affecting;

    /** List of Attribute Name Tags which affect the current Attribute (e.g. Damage is affected by a Damage Multiplier buff) */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Configuration")
    TArray<FGameplayTag> AffectedBy;

};


USTRUCT(BlueprintType)
struct FEAttributeDescriptor
{
    GENERATED_BODY()


    FEAttributeDescriptor(): bInvalid(true),
                             bActive(false),
                             bPeriodic(false),
                             Period(0.0f),
                             Duration(0.0f),
                             EffectOperation(EEEffectOperation::EEO_Addition),
                             MinimumValue(0.0f),
                             MaximumValue(0.0f),
                             MaximumValueAttributeTag(FGameplayTag::RequestGameplayTag(NAME_None, false)),
                             DefaultValue(0.0f),
                             DefaultValueAttributeTag(FGameplayTag::RequestGameplayTag(NAME_None, false)),
                             Value(0.0f)
    {
    }

    FEAttributeDescriptor(const FEAttributeDescriptor& InSourceAttribute): bInvalid(InSourceAttribute.IsInvalid()),
                                                                           bActive(InSourceAttribute.IsActive()),
                                                                           bPeriodic(InSourceAttribute.IsPeriodic()),
                                                                           Period(InSourceAttribute.GetPeriod()),
                                                                           Duration(InSourceAttribute.GetDuration()),
                                                                           EffectOperation(InSourceAttribute.EffectOperation),
                                                                           MinimumValue(InSourceAttribute.MinimumValue),
                                                                           MaximumValue(InSourceAttribute.MaximumValue),
                                                                           MaximumValueAttributeTag(InSourceAttribute.MaximumValueAttributeTag),
                                                                           DefaultValue(InSourceAttribute.DefaultValue),
                                                                           DefaultValueAttributeTag(InSourceAttribute.DefaultValueAttributeTag),
                                                                           Affecting(InSourceAttribute.GetAffecting()),
                                                                           AffectedBy(InSourceAttribute.GetAffectedBy()),
                                                                           Value(InSourceAttribute.GetValue())
    {
    }


    void Initialize(const class UEAttributesComponent* InAttributesOwner, const FEAttributeConfigurationDataTableRow* InAttributeConfigurationRow);

    float Update(const UEAttributesComponent* InAttributesOwner, const float InDelta);


    FORCEINLINE float GetValue() const
    {
        return Value;
    }

    FORCEINLINE TArray<FGameplayTag> GetAffecting() const
    {
        return Affecting;
    }

    FORCEINLINE TArray<FGameplayTag> GetAffectedBy() const
    {
        return AffectedBy;
    }


    FORCEINLINE bool IsInvalid() const
    {
        return bInvalid;
    }

    FORCEINLINE bool IsActive() const
    {
        return bActive;
    }

    FORCEINLINE bool IsPeriodic() const
    {
        return bPeriodic;
    }

    FORCEINLINE float GetPeriod() const
    {
        return Period;
    }

    FORCEINLINE float GetDuration() const
    {
        return Duration;
    }

    FORCEINLINE bool IsValueMinimum() const
    {
        return Value <= MinimumValue;
    }

    FORCEINLINE bool IsValueMaximum(const UEAttributesComponent* InAttributesOwner) const
    {
        return Value >= GetCorrectValue(InAttributesOwner, MaximumValueAttributeTag, MaximumValue);
    }

    FORCEINLINE float GetMaximum(const UEAttributesComponent* InAttributesOwner) const
    {
        return GetCorrectValue(InAttributesOwner, MaximumValueAttributeTag, MaximumValue);
    }

    FORCEINLINE bool IsValueDefault(const UEAttributesComponent* InAttributesOwner) const
    {
        return FMath::IsNearlyEqual(Value, GetCorrectValue(InAttributesOwner, DefaultValueAttributeTag, DefaultValue));
    }

    FORCEINLINE float GetDefault(const UEAttributesComponent* InAttributesOwner) const
    {
        return GetCorrectValue(InAttributesOwner, DefaultValueAttributeTag, DefaultValue);
    }

private:

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    bool bInvalid;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    bool bActive;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    bool bPeriodic;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    float Period;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    float Duration;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    EEEffectOperation EffectOperation;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    float MinimumValue;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    float MaximumValue;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    FGameplayTag MaximumValueAttributeTag;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    float DefaultValue;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    FGameplayTag DefaultValueAttributeTag;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<FGameplayTag> Affecting;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Configuration", meta = (AllowPrivateAccess = "true"))
    TArray<FGameplayTag> AffectedBy;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Attribute Description", meta = (AllowPrivateAccess = "true"))
    float Value;


    float GetAdditionOperationDelta(float InDelta, bool bInNegative, float InMaximum) const;


    //
    // Static Methods
    //

    static float GetCorrectValue(const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeTag, const float InDefaultValue);

};
