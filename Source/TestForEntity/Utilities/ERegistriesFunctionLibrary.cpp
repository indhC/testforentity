﻿// © TestForEntity 2023.07.24:01:18 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "ERegistriesFunctionLibrary.h"

#include "TestForEntity/Components/Attributes/EAttributesComponent.h"
#include "TestForEntity/Components/Attributes/Interfaces/EAttributesStorageInterface.h"
#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"
#include "TestForEntity/Framework/Characters/Inputs/Interfaces/EInputActionHandlersStorageInterface.h"


bool UERegistriesFunctionLibrary::GetAttributeDescriptor(UGameInstance* InGameInstance, const UEAttributesComponent* InAttributesOwner, const FGameplayTag& InAttributeNameTag,
                                                         FEAttributeDescriptor& OutDescriptor)
{
    bool bResult{false};
    if (InGameInstance && InGameInstance->Implements<UEAttributesStorageInterface>())
    {
        OutDescriptor = IEAttributesStorageInterface::Execute_GetCachedAttributeDescriptor(InGameInstance, InAttributesOwner, InAttributeNameTag);
        bResult = !OutDescriptor.IsInvalid();
    }
    return bResult;
}

UEInputActionHandler* UERegistriesFunctionLibrary::GetInputActionHandler(UGameInstance* InGameInstance, ACharacter* InOwner, const FGameplayTag& InInputActionHandlerNameTag)
{
    UEInputActionHandler* Result{nullptr};
    if (InGameInstance && InGameInstance->Implements<UEInputActionHandlersStorageInterface>())
    {
        Result = IEInputActionHandlersStorageInterface::Execute_GetCachedInputActionHandler(InGameInstance, InOwner, InInputActionHandlerNameTag);
    }
    return Result;
}
