﻿// © TestForEntity 2023.07.23:12:22 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GameFramework/Actor.h"
#include "TestForEntity/Components/Attributes/Interfaces/EAttributesActorInterface.h"
#include "EDummy.generated.h"


UCLASS(Abstract, Blueprintable, BlueprintType)
class TESTFORENTITY_API AEDummy : public AActor, public IEAttributesActorInterface
{
    GENERATED_BODY()


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dummy|Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USkeletalMeshComponent> SkeletalMeshComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dummy|Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UBoxComponent> CollisionBoxComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dummy|Components|Attributes", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UEAttributesComponent> AttributesComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dummy|Components|Generators", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<class UELootGeneratorComponent> LootGeneratorComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dummy|Configuration", meta = (AllowPrivateAccess = "true"))
    FGameplayTag HealthAttributeNameTag;

protected:

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Dummy|Attributes")
    void OnAttributeChangedEvent(const AActor* InInstigator, const UEAttributesComponent* InInstigatorAttributesComponent, const FGameplayTag& InAttributeTag,
                                 const float InNewValue, const float InValue, const float InValueDelta);

public:

    // Sets default values for this actor's properties
    AEDummy();


    /////////////////////////////////////////////////////////////////
    /// IEAttributesActorInterface Implementation
    /////////////////////////////////////////////////////////////////

    virtual UEAttributesComponent* GetAttributesComponent_Implementation() const override;

    virtual void UpdateParentAttributesComponent_Implementation(const AActor* InParent) override;

};
