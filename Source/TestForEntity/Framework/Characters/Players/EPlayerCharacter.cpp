﻿// © TestForEntity 2023.07.05:05:39 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#include "EPlayerCharacter.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TestForEntity/Components/Attributes/EAttributesComponent.h"
#include "TestForEntity/Components/Combats/ECombatComponent.h"
#include "TestForEntity/Components/Generators/ELootGeneratorComponent.h"
#include "TestForEntity/Components/Inventories/EInventoryComponent.h"
#include "TestForEntity/Components/Traces/ELineTraceComponent.h"
#include "TestForEntity/Framework/Characters/Inputs/EInputActionHandler.h"
#include "TestForEntity/Utilities/ECombatFunctionLibrary.h"
#include "TestForEntity/Utilities/ERegistriesFunctionLibrary.h"
#include "TestForEntity/World/EPickableItem.h"
#include "TestForEntity/World/Ammo/EAmmo.h"
#include "TestForEntity/World/Weapons/EWeapon.h"


namespace EPlayerCharacter
{
    constexpr double GMultiplierHalfValue{0.5};

    constexpr double GDefaultMaxAcceleration{1024};

    constexpr double GDefaultArmLength{400.0};
    const FVector GDefaultArmOffset{0.0, 50.0, 120.0};

    const FRotator GDefaultCameraRotation{-15.0, 0.0, 0.0};

    constexpr float GDefaultTraceDelay{0.8f};
    constexpr float GDefaultTraceDistance{5000.0f};

    constexpr double GDefaultPelvisZLocation{-1.0};

    constexpr double GDefaultCapsuleAdjusterZOffset{30.0};
    constexpr float GDefaultCapsuleAdjusterRadius{54.0f};
    constexpr float GDefaultCapsuleAdjusterHalfHeight{60.0f};
    constexpr float GDefaultCapsuleAdjusterRadiusWithoutCollision{6.0f};
}


// Sets default values
AEPlayerCharacter::AEPlayerCharacter(): CombatScreenConfigRowNameTag{FGameplayTag::RequestGameplayTag(NAME_None, false)},
                                        PelvisBoneName{NAME_None},
                                        LeftHandBoneName{NAME_None},
                                        RightHandBoneName{NAME_None},
                                        TraceDistance{EPlayerCharacter::GDefaultTraceDistance},
                                        TraceDelay{EPlayerCharacter::GDefaultTraceDelay},
                                        CapsuleAdjusterZOffset{EPlayerCharacter::GDefaultCapsuleAdjusterZOffset},
                                        CapsuleAdjusterRadius{EPlayerCharacter::GDefaultCapsuleAdjusterRadius},
                                        CapsuleAdjusterHalfHeight{EPlayerCharacter::GDefaultCapsuleAdjusterHalfHeight},
                                        CapsuleAdjusterRadiusWithoutCollision{EPlayerCharacter::GDefaultCapsuleAdjusterRadiusWithoutCollision}
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    bUseControllerRotationYaw = true;
    bUseControllerRotationPitch = false;
    bUseControllerRotationRoll = false;

    UCharacterMovementComponent* CurrentCharacterMovement{GetCharacterMovement()};
    CurrentCharacterMovement->bOrientRotationToMovement = false;
    CurrentCharacterMovement->MaxAcceleration = EPlayerCharacter::GDefaultMaxAcceleration;

    SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
    SpringArmComponent->bUsePawnControlRotation = true;
    SpringArmComponent->TargetArmLength = EPlayerCharacter::GDefaultArmLength;
    SpringArmComponent->SocketOffset = EPlayerCharacter::GDefaultArmOffset;
    SpringArmComponent->SetupAttachment(GetRootComponent());

    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
    CameraComponent->SetWorldRotation(EPlayerCharacter::GDefaultCameraRotation);
    CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);

    PickUpTargetComponent = CreateDefaultSubobject<USceneComponent>(TEXT("PickUpTargetComponent"));
    PickUpTargetComponent->SetupAttachment(GetRootComponent());

    LeftHandTransformTrackingComponent = CreateDefaultSubobject<USceneComponent>(TEXT("LeftHandTransformTrackingComponent"));
    LeftHandTransformTrackingComponent->SetupAttachment(GetRootComponent());

    LootGeneratorComponent = CreateDefaultSubobject<UELootGeneratorComponent>(TEXT("LootGeneratorComponent"));
    LootGeneratorComponent->SetupAttachment(GetRootComponent());

    AttributesComponent = CreateDefaultSubobject<UEAttributesComponent>(TEXT("AttributesComponent"));

    InventoryComponent = CreateDefaultSubobject<UEInventoryComponent>(TEXT("InventoryComponent"));

    CombatComponent = CreateDefaultSubobject<UECombatComponent>(TEXT("CombatComponent"));

    TraceComponent = CreateDefaultSubobject<UELineTraceComponent>(TEXT("TraceComponent"));
    TraceComponent->SetTraceInTimer(true);
}

void AEPlayerCharacter::PostInitProperties()
{
    Super::PostInitProperties();

    UpdateCombatScreenParameters(CombatScreenConfigRowNameTag);
}

void AEPlayerCharacter::Landed(const FHitResult& Hit)
{
    Super::Landed(Hit);

    StopJumping();
}

// Called when the game starts or when spawned
void AEPlayerCharacter::BeginPlay()
{
    Super::BeginPlay();

    //Add Input Mapping Context
    if (const APlayerController* PlayerController{Cast<APlayerController>(GetController())})
    {
        if (UEnhancedInputLocalPlayerSubsystem* Subsystem{ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer())})
        {
            Subsystem->AddMappingContext(InputMappingContext, 0);
        }
    }

    if (ensureAlways(LeftHandBoneName.IsValid()))
    {
        const FAttachmentTransformRules& AttachmentRules{EAttachmentRule::KeepRelative, true};
        LeftHandTransformTrackingComponent->AttachToComponent(GetMesh(), AttachmentRules, LeftHandBoneName);
    }

    InitializeCombatComponent();

    CombatComponent->OnOldClipRemovedDelegate.AddDynamic(this, &AEPlayerCharacter::OnOldClipRemovedEvent);

    InventoryComponent->OnItemDroppedDelegate.AddDynamic(this, &AEPlayerCharacter::OnItemsDroppedByInventoryEvent);
}

void AEPlayerCharacter::UpdateCombatScreenParameters_Implementation(const FGameplayTag& InConfigRowNameTag)
{
    if (!ensureAlwaysMsgf(CombatScreenConfigTable, TEXT("The Combat Screen Configuration Data Table is not specified!"))
        || !ensureAlwaysMsgf(InConfigRowNameTag.IsValid(), TEXT("The Combat Screen Configuration Row Name Tag is not specified")))
    {
        return;
    }

    const FECombatScreenConfigTableRow* Row{CombatScreenConfigTable->FindRow<FECombatScreenConfigTableRow>(InConfigRowNameTag.GetTagName(), FString{})};
    if (!ensureAlwaysMsgf(Row, TEXT("Combat Screen Configuration Row with with Name = '%s' is not found in specified Data Table!"), *InConfigRowNameTag.GetTagName().ToString()))
    {
        return;
    }

    LeftHandBoneName = Row->LeftHandBoneName;
    RightHandBoneName = Row->RightHandBoneName;

    CrosshairDefaultTexture = Row->CrosshairDefaultTexture;

    CrosshairTextureSize = Row->CrosshairTextureSize;
    CrosshairTextureHalfSize = FVector2D{EPlayerCharacter::GMultiplierHalfValue * CrosshairTextureSize};

    CrosshairScreenOffset = Row->CrosshairScreenOffset;

    if (GEngine && GEngine->GameViewport)
    {
        FVector2D ViewportSize;
        GEngine->GameViewport->GetViewportSize(ViewportSize);
        CrosshairScreenLocation = FVector2D{EPlayerCharacter::GMultiplierHalfValue * ViewportSize + CrosshairScreenOffset};
    }

    TraceDelay = Row->TraceDelay;
    TraceDistance = Row->TraceDistance;
}

void AEPlayerCharacter::InitializeCombatComponent_Implementation() const
{
    AEWeapon* DefaultWeapon{LootGeneratorComponent->SpawnWeapon(CombatComponent->GetDefaultWeaponNameTag())};
    InventoryComponent->PickUpItem(DefaultWeapon);
    CombatComponent->EquipWeapon(DefaultWeapon);
    if (DefaultWeapon && DefaultWeapon->IsEmpty())
    {
        AEAmmo* DefaultAmmo{LootGeneratorComponent->SpawnAmmo(CombatComponent->GetDefaultAmmoNameTag())};
        IEReloadableItemInterface::Execute_Reload(CombatComponent, DefaultAmmo);
    }
}

void AEPlayerCharacter::OnItemPickedUpEvent_Implementation(AEPickableItem* InItem)
{
    InventoryComponent->PickUpItem(InItem);
    if (const AEAmmo* Ammo{Cast<AEAmmo>(InItem)}; Ammo && !InventoryComponent->GetCurrentWeaponAmmoType())
    {
        InventoryComponent->SelectWeaponAmmoType(Ammo);
    }
}

void AEPlayerCharacter::OnItemDroppedEvent_Implementation(AEPickableItem* InItem)
{
    InItem->OnItemPickedUpDelegate.RemoveDynamic(this, &AEPlayerCharacter::OnItemPickedUpEvent);
    InItem->OnItemDroppedDelegate.RemoveDynamic(this, &AEPlayerCharacter::OnItemDroppedEvent);

    InventoryComponent->DropItem(InItem);
}

void AEPlayerCharacter::OnItemPickedUpByInventoryEvent_Implementation(const EEInventoryOperationStatus InStatus, AEPickableItem* InItem)
{
    if ((EEInventoryOperationStatus::SPickingUpUsedItem == InStatus) && InItem)
    {
        InItem->PickUp(this);
    }
}

void AEPlayerCharacter::OnItemsDroppedByInventoryEvent_Implementation(const EEInventoryOperationStatus InStatus, const TArray<AEPickableItem*>& InDroppingItems)
{
    if (EEInventoryOperationStatus::SDroppingUsedItem == InStatus)
    {
        for (AEPickableItem* DroppingItem : InDroppingItems)
        {
            DroppingItem->Drop();
        }
    }
}

void AEPlayerCharacter::OnOldClipRemovedEvent_Implementation(const AEWeapon* InCurrentWeapon, AEAmmo* InOldAmmo)
{
    InventoryComponent->ProcessUsedItem(InOldAmmo);
}

// Called every frame
void AEPlayerCharacter::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (const USkeletalMeshComponent* SkeletalMeshComponent{GetMesh()}; !SkeletalMeshComponent || !SkeletalMeshComponent->GetSkeletalMeshAsset())
    {
        return;
    }

    FVector Location{GetActorLocation()};
    Location.Z += CapsuleAdjusterZOffset;
    UCapsuleComponent* Capsule{GetCapsuleComponent()};
    if (FHitResult Hit;
        GetWorld()->SweepSingleByChannel(Hit, Location, Location, FQuat::Identity, ECC_Visibility, FCollisionShape::MakeCapsule(CapsuleAdjusterRadius, CapsuleAdjusterHalfHeight)))
    {
        Capsule->SetCapsuleRadius(CapsuleAdjusterRadius);
    }
    else
    {
        Capsule->SetCapsuleRadius(CapsuleAdjusterRadiusWithoutCollision);
    }
}

// Called to bind functionality to input
void AEPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    if (UEnhancedInputComponent* EnhancedInputComponent{CastChecked<UEnhancedInputComponent>(PlayerInputComponent)})
    {
        UGameInstance* GameInstance{UGameplayStatics::GetGameInstance(this)};
        for (const FGameplayTag& InputActionHandlerNameTag : InputActionHandlerNameTags)
        {
            if (InputActionHandlersMap.Contains(InputActionHandlerNameTag))
            {
                continue;
            }

            UEInputActionHandler* Handler{UERegistriesFunctionLibrary::GetInputActionHandler(GameInstance, this, InputActionHandlerNameTag)};
            Handler->Assign(EnhancedInputComponent);
            InputActionHandlersMap.Add(InputActionHandlerNameTag, Handler);
        }
    }
}


/////////////////////////////////////////////////////////////////
/// IEMeshActorInterface Implementation
/////////////////////////////////////////////////////////////////

USkeletalMeshComponent* AEPlayerCharacter::GetMeshComponent_Implementation() const
{
    return GetMesh();
}


/////////////////////////////////////////////////////////////////
/// IECombatScreenConfigInterface Implementation
/////////////////////////////////////////////////////////////////

UTexture* AEPlayerCharacter::GetCrosshairDefaultTexture_Implementation() const
{
    return CrosshairDefaultTexture;
}

FVector2D AEPlayerCharacter::GetCrosshairTextureSize_Implementation() const
{
    return CrosshairTextureSize;
}

FVector2D AEPlayerCharacter::GetCrosshairTextureHalfSize_Implementation() const
{
    return CrosshairTextureHalfSize;
}

FVector2D AEPlayerCharacter::GetCrosshairScreenOffset_Implementation() const
{
    return CrosshairScreenOffset;
}

FVector2D AEPlayerCharacter::GetCrosshairScreenLocation_Implementation() const
{
    return CrosshairScreenLocation;
}

float AEPlayerCharacter::GetTraceDistance_Implementation() const
{
    return TraceDistance;
}

FVector AEPlayerCharacter::GetTargetPickUpLocation_Implementation()
{
    return PickUpTargetComponent->GetComponentLocation();
}

FRotator AEPlayerCharacter::GetCameraRotation_Implementation() const
{
    return CameraComponent->GetComponentRotation();
}

float AEPlayerCharacter::GetTraceDelay_Implementation()
{
    return TraceDelay;
}


/////////////////////////////////////////////////////////////////
/// IEAttributesActorInterface Implementation
/////////////////////////////////////////////////////////////////

UEAttributesComponent* AEPlayerCharacter::GetAttributesComponent_Implementation() const
{
    return AttributesComponent;
}

void AEPlayerCharacter::UpdateParentAttributesComponent_Implementation(const AActor* InParent)
{
    UECombatFunctionLibrary::UpdateParentAttributesComponent(InParent, AttributesComponent);
}


/////////////////////////////////////////////////////////////////
/// IEPickableItemConsumerInterface Implementation
/////////////////////////////////////////////////////////////////

void AEPlayerCharacter::SubscribePickableItemDelegates_Implementation(AEPickableItem* InItem)
{
    InItem->OnItemPickedUpDelegate.AddUniqueDynamic(this, &AEPlayerCharacter::OnItemPickedUpEvent);
    InItem->OnItemDroppedDelegate.AddUniqueDynamic(this, &AEPlayerCharacter::OnItemDroppedEvent);
}


/////////////////////////////////////////////////////////////////
/// IEBoneTransformTrackerInterface Implementation
/////////////////////////////////////////////////////////////////

FTransform AEPlayerCharacter::GetTransform_Implementation() const
{
    return LeftHandTransformTrackingComponent->GetComponentTransform();
}

void AEPlayerCharacter::UpdateTransform_Implementation(const FTransform& InUpdatedTransform)
{
    LeftHandTransformTrackingComponent->SetWorldTransform(InUpdatedTransform);
}


/////////////////////////////////////////////////////////////////
/// IEPeriodicTraceConditionInterface Implementation
/////////////////////////////////////////////////////////////////

bool AEPlayerCharacter::IsCurrentlyTraceable_Implementation()
{
    return IEPeriodicTraceConditionInterface::IsCurrentlyTraceable_Implementation();
}


/////////////////////////////////////////////////////////////////
/// IEInputActionControlInterface Implementation
/////////////////////////////////////////////////////////////////

void AEPlayerCharacter::SubscribeCapsuleOverlapEvents_Implementation(UEInputActionHandler* InHandler, const FName& InBeginOverlapMethodName, const FName& InEndOverlapMethodName)
{
    UCapsuleComponent* Capsule{GetCapsuleComponent()};

    FScriptDelegate BeginOverlapDelegate;
    BeginOverlapDelegate.BindUFunction(InHandler, InBeginOverlapMethodName);
    Capsule->OnComponentBeginOverlap.AddUnique(BeginOverlapDelegate);

    FScriptDelegate EndOverlapDelegate;
    EndOverlapDelegate.BindUFunction(InHandler, InEndOverlapMethodName);
    Capsule->OnComponentEndOverlap.AddUnique(EndOverlapDelegate);
}

void AEPlayerCharacter::SubscribeTraceHitEvent_Implementation(UEInputActionHandler* InHandler, const FName& InTraceHitHandlerMethodName)
{
    FScriptDelegate Delegate;
    Delegate.BindUFunction(InHandler, InTraceHitHandlerMethodName);
    TraceComponent->OnHitOccurredDelegate.AddUnique(Delegate);
}

void AEPlayerCharacter::UpdateTraceOwner_Implementation(UEInputActionHandler* InTraceOwner)
{
    TraceComponent->SetComponentOwner(InTraceOwner);
}

bool AEPlayerCharacter::IsTraceItemInventoryAddable_Implementation(AActor* InFoundItem) const
{
    return InventoryComponent->CanAddItem(InFoundItem);
}

void AEPlayerCharacter::SubscribeWeaponAmmoOutEvent_Implementation(UEInputActionHandler* InHandler, const FName& InWeaponAmmoOutHandlerMethodName)
{
    FScriptDelegate Delegate;
    Delegate.BindUFunction(InHandler, InWeaponAmmoOutHandlerMethodName);
    CombatComponent->OnAmmoRanOutDelegate.AddUnique(Delegate);
}

bool AEPlayerCharacter::IsReloadPossible_Implementation() const
{
    return InventoryComponent->GetCurrentWeaponAmmoType() && IEReloadableItemInterface::Execute_MayBeReloaded(CombatComponent);
}

AEAmmo* AEPlayerCharacter::GetCurrentWeaponAmmo_Implementation() const
{
    return InventoryComponent->GetCurrentWeaponAmmo();
}

void AEPlayerCharacter::ReloadCurrentWeapon_Implementation(AEAmmo* InNewAmmo)
{
    IEReloadableItemInterface::Execute_Reload(CombatComponent, InNewAmmo);
}

bool AEPlayerCharacter::IsAttackPossible_Implementation() const
{
    return CombatComponent->CanAttack();
}

void AEPlayerCharacter::Attack_Implementation(const FVector& InWorldPosition, const FVector& InWorldRotation)
{
    CombatComponent->Attack(InWorldPosition, InWorldRotation);
}


//
// Getters And Setters
//

bool AEPlayerCharacter::IsAiming() const
{
    return CombatComponent->IsAiming();
}

bool AEPlayerCharacter::IsReloading() const
{
    return CombatComponent->IsReloading();
}

bool AEPlayerCharacter::IsLeftHandTransformTrackingEnabled() const
{
    return nullptr != CombatComponent->GetStockSocket();
}

FTransform AEPlayerCharacter::GetStockSocketTransform() const
{
    return CombatComponent->GetStockSocketTransform();
}
