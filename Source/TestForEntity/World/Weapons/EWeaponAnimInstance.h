﻿// © TestForEntity 2023.07.03:21:43 Dmitry V. (ind.hC). All rights reserved.
// This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. To view a copy of this license, visit
// https://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h"
#include "EWeaponAnimInstance.generated.h"


USTRUCT(BlueprintType)
struct FEWeaponAnimInstanceProxy: public FAnimInstanceProxy
{
    GENERATED_BODY()


    FEWeaponAnimInstanceProxy() = default;

    explicit FEWeaponAnimInstanceProxy(UAnimInstance* InInstance);


    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Configuration")
    TObjectPtr<class AEWeapon> OwningWeapon;


    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Transformation")
    bool bFiring{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Transformation")
    bool bMovingClip{false};

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Transformation")
    FVector CurrentLeftHandLocation;

    UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = "Proxy|Transformation")
    FRotator CurrentLeftHandRotation;

protected:

    virtual void PreUpdate(UAnimInstance* InAnimInstance, const float InDeltaSeconds) override;

};


/**
 * 
 */
UCLASS(Transient, BlueprintType)
class TESTFORENTITY_API UEWeaponAnimInstance : public UAnimInstance
{
    GENERATED_BODY()


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Proxy", meta = (AllowPrivateAccess = "true"))
    FEWeaponAnimInstanceProxy Proxy;

protected:

    virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override
    {
        return &Proxy;
    }

    virtual void DestroyAnimInstanceProxy(FAnimInstanceProxy* InProxy) override
    {
    }

public:

    virtual void NativeBeginPlay() override;

};
